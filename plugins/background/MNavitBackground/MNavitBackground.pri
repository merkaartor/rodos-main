HEADERS += IMapAdapter.h \
    IImageManager.h \
    NavitAdapter.h \
    NavitBin.h \
    NavitZip.h \
    NavitFeature.h
SOURCES += \
    Utils/TagSelector.cpp \
    Utils/SvgCache.cpp \
    NavitAdapter.cpp \
    NavitBin.cpp \
    NavitZip.cpp \
    NavitFeature.cpp \

RESOURCES += MNavitBackground.qrc
