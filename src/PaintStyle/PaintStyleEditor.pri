INCLUDEPATH += PaintStyle
DEPENDPATH += PaintStyle

# Header files
HEADERS +=  PaintStyleEditor.h

# Source files
SOURCES += PaintStyleEditor.cpp

# Forms
FORMS +=  PaintStyleEditor.ui
