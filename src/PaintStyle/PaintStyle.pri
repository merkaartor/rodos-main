INCLUDEPATH += PaintStyle
DEPENDPATH += PaintStyle

# Header files
HEADERS +=  \
    MasPaintStyle.h \
    MapCSSPaintstyle.h \
    PrimitivePainter.h \
    Painter.h \
    PaintStyle/IPaintStyle.h

# Source files
SOURCES +=  \
    MasPaintStyle.cpp \
    MapCSSPaintstyle.cpp \
    PrimitivePainter.cpp \
    Painter.cpp
