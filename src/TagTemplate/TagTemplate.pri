INCLUDEPATH += TagTemplate
DEPENDPATH += TagTemplate

#Header files
HEADERS += \
	TagTemplate.h 

#Source files
SOURCES += \
	TagTemplate.cpp 

#Forms
FORMS += 
	
#Resource file(s)
RESOURCES +=../Templates/Templates.qrc 
