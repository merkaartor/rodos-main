INCLUDEPATH += Features 
DEPENDPATH += Features

HEADERS += \
	Feature.h \
	Relation.h \
	Way.h \
	Node.h \
	TrackSegment.h \
    Features/IFeature.h

SOURCES += \ 
	Feature.cpp \
	Relation.cpp \
	Way.cpp \
	Node.cpp \
	TrackSegment.cpp \
