INCLUDEPATH += Tools
DEPENDPATH += Tools

#Header files
HEADERS += \
	RegionMapWidget.h \
	WorldOsbManager.h \
	ActionsDialog.h

#Source files
SOURCES += \
	RegionMapWidget.cpp \
	WorldOsbManager.cpp \
	ActionsDialog.cpp

#Forms
FORMS += \
	WorldOsbManager.ui


