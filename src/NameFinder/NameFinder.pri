SOURCES += xmlstreamreader.cpp \
    httpquery.cpp \
    namefindertablemodel.cpp \
    namefinderwidget.cpp
HEADERS += NameFinderResult.h \
    xmlstreamreader.h \
    httpquery.h \
    namefindertablemodel.h \
    namefinderwidget.h
FORMS += namefinderwidget.ui
