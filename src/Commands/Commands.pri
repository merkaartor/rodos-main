INCLUDEPATH += Commands
DEPENDPATH += Commands

HEADERS += \
	Command.h \
	DocumentCommands.h \
	FeatureCommands.h \
	RelationCommands.h \
	WayCommands.h \
	TrackSegmentCommands.h \
	NodeCommands.h \

SOURCES += \
	Command.cpp \
	DocumentCommands.cpp \
	FeatureCommands.cpp \
	NodeCommands.cpp \
	RelationCommands.cpp \
	WayCommands.cpp \
	TrackSegmentCommands.cpp \
