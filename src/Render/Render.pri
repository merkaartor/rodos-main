# Header files
HEADERS += \
    FeaturePainter.h \
    NativeRenderDialog.h \
    MapRenderer.h

# Source files
SOURCES += \
    FeaturePainter.cpp \
    NativeRenderDialog.cpp \
    MapRenderer.cpp

# Forms
FORMS += NativeRenderDialog.ui
QT += svg
