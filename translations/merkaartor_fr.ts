<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr">
<context>
    <name></name>
    <message>
        <location filename="../src/MainWindow.cpp" line="921"/>
        <location filename="../src/MainWindow.cpp" line="935"/>
        <location filename="../src/MainWindow.cpp" line="950"/>
        <source>Supported formats</source>
        <translation>Formats pris en charge</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="922"/>
        <location filename="../src/MainWindow.cpp" line="936"/>
        <source>Merkaartor document (*.mdc)
</source>
        <translation>Fichier Merkaartor (*.mdc)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="923"/>
        <location filename="../src/MainWindow.cpp" line="937"/>
        <location filename="../src/MainWindow.cpp" line="951"/>
        <source>GPS Exchange format (*.gpx)
</source>
        <translation>Fichier GPS Exchange (*.gpx)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="924"/>
        <location filename="../src/MainWindow.cpp" line="938"/>
        <location filename="../src/MainWindow.cpp" line="952"/>
        <source>OpenStreetMap format (*.osm)
</source>
        <translation>Fichier OpenStreetMap (*.osm)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="925"/>
        <location filename="../src/MainWindow.cpp" line="939"/>
        <location filename="../src/MainWindow.cpp" line="953"/>
        <source>OpenStreetMap binary format (*.osb)
</source>
        <translation>Fichier binaire OpenStreetMap (*.osb)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="927"/>
        <location filename="../src/MainWindow.cpp" line="941"/>
        <location filename="../src/MainWindow.cpp" line="955"/>
        <source>Noni GPSPlot format (*.ngt)
</source>
        <translation>Fichier Noni GPSPlot (*.ngt)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="928"/>
        <location filename="../src/MainWindow.cpp" line="942"/>
        <location filename="../src/MainWindow.cpp" line="956"/>
        <source>NMEA GPS log format (*.nmea *.nma)
</source>
        <translation>Fichier journal NMEA GPS (*.nmea *.nma)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="929"/>
        <location filename="../src/MainWindow.cpp" line="943"/>
        <location filename="../src/MainWindow.cpp" line="957"/>
        <source>KML file (*.kml)
</source>
        <translation>Fichier KML (*.kml)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="930"/>
        <location filename="../src/MainWindow.cpp" line="944"/>
        <location filename="../src/MainWindow.cpp" line="958"/>
        <source>ESRI Shapefile (*.shp)
</source>
        <translation>fichier d&apos;enveloppe ESRI (*.shp)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="932"/>
        <location filename="../src/MainWindow.cpp" line="947"/>
        <location filename="../src/MainWindow.cpp" line="960"/>
        <source>All Files (*)</source>
        <translation>Tous les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="946"/>
        <source>Geotagged images (*.jpg)
</source>
        <translation>Images géolocalisées (*.jpg)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="926"/>
        <location filename="../src/MainWindow.cpp" line="940"/>
        <location filename="../src/MainWindow.cpp" line="954"/>
        <source>OpenStreetMap change format (*.osc)
</source>
        <translation>Format de changement OpenStreetMap (*.osc)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="931"/>
        <location filename="../src/MainWindow.cpp" line="945"/>
        <location filename="../src/MainWindow.cpp" line="959"/>
        <source>Comma delimited format (*.csv)
</source>
        <translation>Format CSV (*.csv)
</translation>
    </message>
</context>
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/AboutDialog.ui" line="14"/>
        <source>About Merkaartor</source>
        <translation>A propos de Merkaartor</translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="30"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-style:italic;&quot;&gt;Merkaartor version&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;%1%2(%3)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-style:italic;&quot;&gt;Merkaartor version&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;%1%2(%3)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="54"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Boost version&lt;/span&gt;&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Version de Boost&lt;/span&gt;&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="101"/>
        <source>Copyright Bart Vanhauwaert, Chris Browet and others, 2006-2010</source>
        <translation>Copyright Bart Vanhauwaert, Chris Browet et autres, 2006-2010</translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="108"/>
        <source>This program is licensed under the GNU Public License v2</source>
        <translation>Ce programme est publié sous Licence Publique GNU v2</translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="178"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="92"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="135"/>
        <source>Changelog</source>
        <translation>Historique des changements</translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="42"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;QT version&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;%1&lt;/span&gt; (built with &lt;span style=&quot; font-weight:600;&quot;&gt;%2&lt;/span&gt;)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;QT version&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;%1&lt;/span&gt; (built with &lt;span style=&quot; font-weight:600;&quot;&gt;%2&lt;/span&gt;)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="115"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://www.merkaartor.org&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.merkaartor.org&lt;/span&gt;&lt;/a&gt; or &lt;a href=&quot;http://merkaartor.yuio.de&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://merkaartor.yuio.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://www.merkaartor.org&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.merkaartor.org&lt;/span&gt;&lt;/a&gt; ou &lt;a href=&quot;http://merkaartor.yuio.de&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://merkaartor.yuio.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>ActionsDialog</name>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="20"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="20"/>
        <source>Shortcut</source>
        <translation>Raccourci clavier</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="41"/>
        <source>&amp;Import</source>
        <translation>&amp;Importer</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="42"/>
        <source>&amp;Export</source>
        <translation>&amp;Exporter</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="43"/>
        <source>&amp;Default</source>
        <translation>Par d&amp;éfaut</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="44"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="45"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="72"/>
        <source>Shortcut Editor</source>
        <translation>Editeur de raccourci</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="116"/>
        <source>Load Shortcut scheme</source>
        <translation>Charger schéma de raccourcis</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="116"/>
        <location filename="../src/Tools/ActionsDialog.cpp" line="143"/>
        <source>Merkaartor shortcut scheme (*.mss)</source>
        <translation>Schéma de raccourci Merkaartor (*.mss)</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="120"/>
        <source>Unable to open file</source>
        <translation>Impossible d&apos;ouvrir le fichier</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="120"/>
        <source>%1 could not be opened.</source>
        <translation>%1 n&apos;a pas pu être ouvert.</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="143"/>
        <source>Save Shortcut scheme</source>
        <translation>Sauvegarder schéma de raccourci</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="143"/>
        <source>untitled</source>
        <translation>sans titre</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="148"/>
        <source>Unable to open save file</source>
        <translation>Impossible d&apos;ouvrir le fichier de sauvegarde</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="148"/>
        <source>%1 could not be opened for writing.</source>
        <translation>%1 n&apos;a pas pu être ouvert en écriture.</translation>
    </message>
</context>
<context>
    <name>Command</name>
    <message>
        <location filename="../src/Commands/Command.cpp" line="25"/>
        <source>No description</source>
        <translation>Pas de description</translation>
    </message>
</context>
<context>
    <name>CreateDoubleWayDock</name>
    <message>
        <location filename="../src/Interactions/CreateDoubleWayDock.ui" line="13"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateDoubleWayDock.ui" line="25"/>
        <source>Driving at the right side of the road</source>
        <translation>Conduite à droite</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateDoubleWayDock.ui" line="40"/>
        <source>Distance between two roads</source>
        <translation>Distance entre deux routes</translation>
    </message>
</context>
<context>
    <name>CreateRoundaboutDock</name>
    <message>
        <location filename="../src/Interactions/CreateRoundaboutDock.ui" line="13"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateRoundaboutDock.ui" line="25"/>
        <source>Driving at the right side of the road</source>
        <translation>Conduite à droite</translation>
    </message>
</context>
<context>
    <name>DirtyDock</name>
    <message>
        <location filename="../src/Docks/DirtyDock.cpp" line="197"/>
        <source>Center map</source>
        <translation>Centrer la carte</translation>
    </message>
    <message>
        <location filename="../src/Docks/DirtyDock.cpp" line="198"/>
        <source>Center &amp;&amp; Zoom map</source>
        <translation>Centrer &amp;&amp; Zoomer la carte</translation>
    </message>
    <message>
        <location filename="../src/Docks/DirtyDock.cpp" line="69"/>
        <source>There is &lt;b&gt;no&lt;/b&gt; object in the dirty layer</source>
        <translation>Il n&apos;y a &lt;b&gt;pas&lt;/b&gt; d&apos;objet dans la couche &quot;Modification&quot;</translation>
    </message>
    <message>
        <location filename="../src/Docks/DirtyDock.cpp" line="196"/>
        <source>Undo</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/Docks/DirtyDock.cpp" line="72"/>
        <source>There is &lt;b&gt;one&lt;/b&gt; object in the dirty layer</source>
        <translation>Il y a &lt;b&gt;un&lt;/b&gt; object dans la couche &quot;Modification&quot;</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/Docks/DirtyDock.cpp" line="76"/>
        <source>There are &lt;b&gt;%n&lt;/b&gt; objects in the dirty layer</source>
        <translation type="unfinished">
            <numerusform>Il y a &lt;b&gt;%n&lt;/b&gt; objet dans la couche &quot;Modification&quot;</numerusform>
            <numerusform>Il y a &lt;b&gt;%n&lt;/b&gt; objets dans la couche &quot;Modification&quot;</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DirtyDockWidget</name>
    <message>
        <location filename="../src/Docks/DirtyDock.ui" line="14"/>
        <source>Undo</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/Docks/DirtyDock.ui" line="37"/>
        <source>There is &lt;b&gt;no&lt;/b&gt; object in the dirty layer</source>
        <translation>Il n&apos;y a &lt;b&gt;pas&lt;/b&gt; d&apos;objet dans la couche &quot;Modification&quot;</translation>
    </message>
    <message>
        <location filename="../src/Docks/DirtyDock.ui" line="56"/>
        <source>Cleanup</source>
        <translation>Nettoyer</translation>
    </message>
</context>
<context>
    <name>DirtyLayerWidget</name>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="503"/>
        <source>Zoom</source>
        <translation>Faire un zoom</translation>
    </message>
</context>
<context>
    <name>DirtyListExecutor</name>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="430"/>
        <location filename="../src/Sync/DirtyList.cpp" line="437"/>
        <location filename="../src/Sync/DirtyList.cpp" line="453"/>
        <source>Error uploading request</source>
        <translation>Erreur durant le téléversage de la requête</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="434"/>
        <location filename="../src/Sync/DirtyList.cpp" line="454"/>
        <source>There was an error uploading this request (%1)
Server message is &apos;%2&apos;</source>
        <translation>Une erreur s&apos;est produite durant le téléversage de cette requete (%1)
Le message du serveur est &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="334"/>
        <location filename="../src/Sync/DirtyList.cpp" line="575"/>
        <source>ADD road %1</source>
        <translation>AJOUTER route %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="342"/>
        <location filename="../src/Sync/DirtyList.cpp" line="605"/>
        <source>ADD trackpoint %1</source>
        <translation>AJOUTER point %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="350"/>
        <location filename="../src/Sync/DirtyList.cpp" line="546"/>
        <source>ADD relation %1</source>
        <translation>AJOUTER relation %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="358"/>
        <location filename="../src/Sync/DirtyList.cpp" line="695"/>
        <source>UPDATE trackpoint %1</source>
        <translation>MODIFIER point %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="366"/>
        <location filename="../src/Sync/DirtyList.cpp" line="636"/>
        <source>UPDATE relation %1</source>
        <translation>RAFRAICHIR la relation %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="374"/>
        <location filename="../src/Sync/DirtyList.cpp" line="666"/>
        <source>UPDATE road %1</source>
        <translation>RAFRAICHIR la route %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="382"/>
        <location filename="../src/Sync/DirtyList.cpp" line="726"/>
        <source>REMOVE trackpoint %1</source>
        <translation>SUPPRIMER point %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="390"/>
        <location filename="../src/Sync/DirtyList.cpp" line="751"/>
        <source>REMOVE road %1</source>
        <translation>SUPPRIMER route %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="398"/>
        <location filename="../src/Sync/DirtyList.cpp" line="776"/>
        <source>REMOVE relation %1</source>
        <translation>SUPPRIMER relation %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="431"/>
        <source>Please check your username and password in the Preferences menu</source>
        <translation>Veuillez vérifier l&apos;exactitude de vos Identifiant et mot-de-passe dans le menu Préférences</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="498"/>
        <source>OPEN changeset</source>
        <translation>OUVRIR changeset</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="526"/>
        <source>CLOSE changeset</source>
        <translation>FERMER changeset</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="436"/>
        <source>
API message is &apos;%1&apos;</source>
        <translation>
Le message de l&apos;API est le suivant &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="471"/>
        <source>Uploading changes...</source>
        <translation>Envoi des changements...</translation>
    </message>
</context>
<context>
    <name>DirtyListExecutorOSC</name>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="61"/>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="68"/>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="82"/>
        <source>Error uploading request</source>
        <translation>Erreur durant le téléversage de la requête</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="62"/>
        <source>Please check your username and password in the Preferences menu</source>
        <translation>Veuillez vérifier l&apos;exactitude de vos Identifiant et mot-de-passe dans le menu Préférences</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="65"/>
        <source>There was an error uploading this request (%1)
Server message is &apos;%2&apos;</source>
        <translation>Une erreur s&apos;est produite durant le téléversage de cette requete (%1)
Le message du serveur est &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="67"/>
        <source>
API message is &apos;%1&apos;</source>
        <translation>
Le message de l&apos;API est le suivant &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="83"/>
        <source>There was an error uploading this request (%1)
&quot;%2&quot;
Please redownload the problematic feature to handle the conflict.</source>
        <translation>Une erreur s&apos;est produite lors du téléversement de cette requête (%1)
&quot;%2&quot;
Veuillez re-télécharger l&apos;objet problématique pour gérer le conflit.</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="99"/>
        <source>Checking changes...</source>
        <translation>Analyse des changements...</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="170"/>
        <source>OPEN changeset</source>
        <translation>OUVRIR changeset</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="288"/>
        <source>CLOSE changeset</source>
        <translation>FERMER changeset</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="342"/>
        <source>ADD relation %1</source>
        <translation>AJOUTER relation %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="356"/>
        <source>ADD road %1</source>
        <translation>AJOUTER route %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="371"/>
        <source>ADD trackpoint %1</source>
        <translation>AJOUTER point %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="387"/>
        <source>UPDATE relation %1</source>
        <translation>RAFRAICHIR la relation %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="402"/>
        <source>UPDATE road %1</source>
        <translation>RAFRAICHIR la route %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="416"/>
        <source>UPDATE trackpoint %1</source>
        <translation>MODIFIER point %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="430"/>
        <source>REMOVE trackpoint %1</source>
        <translation>SUPPRIMER point %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="444"/>
        <source>REMOVE road %1</source>
        <translation>SUPPRIMER route %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="458"/>
        <source>REMOVE relation %1</source>
        <translation>SUPPRIMER relation %1</translation>
    </message>
</context>
<context>
    <name>Document</name>
    <message>
        <location filename="../src/Document.cpp" line="108"/>
        <source>Dirty layer</source>
        <translation>Calque des brouillons</translation>
    </message>
    <message>
        <location filename="../src/Document.cpp" line="111"/>
        <source>Uploaded layer</source>
        <translation>Couche Téléversé</translation>
    </message>
    <message>
        <location filename="../src/Document.cpp" line="274"/>
        <source>Background imagery</source>
        <translation>Imagerie d&apos;arrière-plan</translation>
    </message>
    <message>
        <location filename="../src/Document.cpp" line="508"/>
        <location filename="../src/Document.cpp" line="565"/>
        <source>OSM Export</source>
        <translation>Export OSM</translation>
    </message>
    <message>
        <location filename="../src/Document.cpp" line="515"/>
        <location filename="../src/Document.cpp" line="575"/>
        <source>Exporting OSM...</source>
        <translation>Exportation OSM...</translation>
    </message>
</context>
<context>
    <name>DownloadMapDialog</name>
    <message>
        <location filename="../src/DownloadMapDialog.ui" line="14"/>
        <source>Download</source>
        <translation>Télécharger</translation>
    </message>
    <message>
        <location filename="../src/DownloadMapDialog.ui" line="32"/>
        <source>Location</source>
        <translation>Place</translation>
    </message>
    <message>
        <location filename="../src/DownloadMapDialog.ui" line="40"/>
        <source>Bookmark</source>
        <translation>Marque-page</translation>
    </message>
    <message>
        <location filename="../src/DownloadMapDialog.ui" line="62"/>
        <source>Current view</source>
        <translation>Vue actuelle</translation>
    </message>
    <message>
        <location filename="../src/DownloadMapDialog.ui" line="74"/>
        <source>WWW link (OSM/google maps)</source>
        <translation>Lien WWW (OSM/google maps)</translation>
    </message>
    <message>
        <location filename="../src/DownloadMapDialog.ui" line="86"/>
        <source>From the map below (map provided by the OpenStreetMap project)</source>
        <translation>D&apos;après la carte ci-dessous (carte fournie par le projet OpenStreetMap)</translation>
    </message>
    <message>
        <location filename="../src/DownloadMapDialog.ui" line="98"/>
        <source>Also download raw GPS tracks</source>
        <translation>Télécharger aussi les traces GPS brutes</translation>
    </message>
    <message>
        <location filename="../src/DownloadMapDialog.ui" line="105"/>
        <source>Resolve all relations</source>
        <translation>Résoudre toutes les relations</translation>
    </message>
</context>
<context>
    <name>Downloader</name>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="207"/>
        <source>error</source>
        <translation>erreur</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="211"/>
        <source>didn&apos;t download enough</source>
        <translation>n&apos;a pas téléchargé suffisamment</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="509"/>
        <source>Unresolved conflicts</source>
        <translation>Conflits non-résolus</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="509"/>
        <source>Please resolve existing conflicts first</source>
        <translation>Veuillez d&apos;abord résoudre les conflits existants</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="688"/>
        <source>OpenStreetBugs</source>
        <translation>OpenStreetBugs</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="713"/>
        <source>Downloading points</source>
        <translation>Téléchargement des points</translation>
    </message>
    <message>
        <location filename="../src/Docks/InfoDock.cpp" line="81"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="486"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="491"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="497"/>
        <source>Download failed</source>
        <translation>Échec du téléchargement</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="491"/>
        <source>Username/password invalid</source>
        <translation>Nom d&apos;utilisateur/mot de passe non valable</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="452"/>
        <source>Downloading from OSM (connecting)</source>
        <translation>Téléchargement depuis OSM (connection)</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="570"/>
        <source>Downloading trackpoints %1-%2</source>
        <translation>Téléchargement des noeuds %1-%2</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="439"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="562"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="707"/>
        <source>Parsing XML</source>
        <translation>Analyse XML</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="447"/>
        <source>Conflicts from %1</source>
        <translation>Conflits de %1</translation>
    </message>
    <message>
        <location filename="../src/Docks/InfoDock.cpp" line="81"/>
        <source>Unexpected http status code (%1)</source>
        <translation>Code d&apos;état http inattendu (%1)</translation>
    </message>
    <message>
        <location filename="../src/Utils/SlippyMapWidget.cpp" line="69"/>
        <source>Downloading %1,%2 (zoom %3)...</source>
        <translation>Téléchargement de %1,%2 (zoom %3)...</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="588"/>
        <source>Downloaded track - nodes %1-%2</source>
        <translation>Trace - Nœud téléchargés %1-%2</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportExportOSC.cpp" line="73"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="628"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="742"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="846"/>
        <source>%1 download</source>
        <translation>%1 téléchargement</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="320"/>
        <source>Downloading unresolved %1 of %2</source>
        <translation>Téléchargement de la non résolution %1 de %2</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="329"/>
        <source>Parsing unresolved %1 of %2</source>
        <translation>Analyse syntaxique de la non résolution %1 de %2</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="538"/>
        <source>Conflicts have been detected</source>
        <translation>Des conflits ont été détéctés</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="539"/>
        <source>This means that some of the feature you modified since your last download have since been modified by someone else on the server.
The features have been duplicated as &quot;conflict_...&quot; on the &quot;Conflicts...&quot; layer.
Before being able to upload your changes, you will have to manually merge the two versions and remove the one from the &quot;Conflicts...&quot; layer.</source>
        <translation>Ceci signifie que certains des objets que vous avez modifié depuis votre dernier téléchargement on été modifiés par quelqu&apos;un d&apos;autre sur le serveur.
Les objets ont été duplicié avec un identifiant &quot;conflict_...&quot; sur la couche &quot;Conflits&quot;.
Avant de pouvoir téléverser vos changements, vous devrez manuellement fusionner les deux versions et supprimer celle de la couche &quot;Conflits&quot;.</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="517"/>
        <source>Empty roads/relations detected</source>
        <translation>Routes/Relations vides détéctées</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="518"/>
        <source>Empty roads/relations are probably errors.
Do you want to mark them for deletion?</source>
        <translation>Des routes/relations vides sont probablement des erreurs.
Voulez-vous les marquer pour suppression ?</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="526"/>
        <source>Remove empty feature %1</source>
        <translation>Supprimer la caractéristique vide %1</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/Sync/DownloadOSM.cpp" line="320"/>
        <source>Downloading from OSM (%n bytes)</source>
        <translation type="unfinished">
            <numerusform>Téléchargement en provenance d&apos;OSM (%n octets)</numerusform>
            <numerusform>Téléchargement en provenance d&apos;OSM (%n octets)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/Sync/DownloadOSM.cpp" line="322"/>
        <source>Downloading from OSM (%n kBytes)</source>
        <translation type="unfinished">
            <numerusform>Téléchargement en provenance d&apos;OSM (%n kOctets)</numerusform>
            <numerusform>Téléchargement en provenance d&apos;OSM (%n kOctets)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="483"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="494"/>
        <source>Unexpected http status code (%1)
Server message is &apos;%2&apos;</source>
        <translation>Code état inattendu du protocole http (%1)
Le serveur a répondu &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="485"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="496"/>
        <source>
API message is &apos;%1&apos;</source>
        <translation>
Le message de l&apos;API est le suivant &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="442"/>
        <source>Downloading...</source>
        <translation>Téléchargement...</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="433"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="555"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="700"/>
        <source>Parsing...</source>
        <translation>Analyse...</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="313"/>
        <source>Downloading unresolved...</source>
        <translation>Chargement des non-résolus</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="131"/>
        <source>Uploading...</source>
        <translation>Envoi en cours...</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="153"/>
        <source>Preparing changes</source>
        <translation>Préparation des changements</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="155"/>
        <source>Waiting for server response</source>
        <translation>En attente de la réponse du serveur</translation>
    </message>
</context>
<context>
    <name>DrawingLayerWidget</name>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="272"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../src/ImportExport/ExportDialog.ui" line="13"/>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ExportDialog.ui" line="25"/>
        <source>What do you want to export?</source>
        <translation>Que voulez-vous exporter ?</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ExportDialog.ui" line="38"/>
        <source>All visible (i.e. not hidden)</source>
        <translation>Tous les éléments visibles (c.à.d. non-cachés)</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ExportDialog.ui" line="45"/>
        <source>Viewport</source>
        <translation>Vue</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ExportDialog.ui" line="52"/>
        <source>Selected</source>
        <translation>Sélectionné</translation>
    </message>
</context>
<context>
    <name>FeaturesDock</name>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="414"/>
        <source>Features</source>
        <translation>Objets</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="415"/>
        <source>Center map</source>
        <translation>Centrer la carte</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="416"/>
        <source>Center &amp;&amp; Zoom map</source>
        <translation>Centrer &amp;&amp; Zoomer la carte</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="417"/>
        <source>Download missing children</source>
        <translation>Charger les sous-éléments manquants</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="419"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="426"/>
        <source>Relations</source>
        <translation>Relations</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="427"/>
        <source>Roads</source>
        <translation>Routes</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="428"/>
        <source>POI&apos;s</source>
        <translation>Points d&apos;intérêt</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="429"/>
        <source>All</source>
        <translation>Tous</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="418"/>
        <source>Add to selection</source>
        <translation>Ajouter à la sélection</translation>
    </message>
</context>
<context>
    <name>FeaturesDockWidget</name>
    <message>
        <location filename="../src/Docks/FeaturesDock.ui" line="14"/>
        <source>Features</source>
        <translation>Objets</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.ui" line="72"/>
        <source>Only features fully within the viewport</source>
        <translation>Uniquement les objets pleinement dans la vue</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.ui" line="101"/>
        <source>Find...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.ui" line="108"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FilterPreferencesDialog</name>
    <message>
        <location filename="../src/Preferences/FilterPreferencesDialog.ui" line="14"/>
        <source>Filters setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/FilterPreferencesDialog.ui" line="20"/>
        <source>Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/FilterPreferencesDialog.ui" line="47"/>
        <source>Filter list:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/FilterPreferencesDialog.ui" line="76"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/FilterPreferencesDialog.ui" line="93"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/FilterPreferencesDialog.ui" line="116"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../src/Preferences/FilterPreferencesDialog.ui" line="123"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../src/Preferences/FilterPreferencesDialog.ui" line="130"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>GeoImageDock</name>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="85"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="903"/>
        <source>Geo Images</source>
        <translation>Images Géo.</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="93"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="905"/>
        <source>Remove Images</source>
        <translation>Enlever les images</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="94"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="906"/>
        <source>Copy filename to clipboard</source>
        <translation>Copie du nom de fichier dans le presse-papier</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="342"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="447"/>
        <source>Load geotagged Images</source>
        <translation>Charger les images georeférencées</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="343"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="448"/>
        <source>Select the layer to which the images belong:</source>
        <translation>Séléctionnez le calque auquel l&apos;image appartient:</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="469"/>
        <source>Loading Images ...</source>
        <translation>Chargement des images...</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="469"/>
        <source>Abort loading</source>
        <translation>Interrompre le chargement</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="642"/>
        <source>Specify offset</source>
        <translation>Spécifiez le décallage</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="644"/>
        <source>Position images more to the:</source>
        <translation>Positionner les images plus vers:</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="645"/>
        <source>end of the track</source>
        <translation>la fin de la trace</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="646"/>
        <source>beginning of the track</source>
        <translation>début de la trace</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="650"/>
        <source>hh:mm:ss</source>
        <translation>hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="715"/>
        <source>ss &apos;seconds&apos;</source>
        <translation>ss &apos;secondes&apos;</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="717"/>
        <source>mm &apos;minutes and&apos; ss &apos;seconds&apos;</source>
        <translation>mm &apos;minutes et&apos; ss &apos;secondes&apos;</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="719"/>
        <source>hh &apos;hours,&apos; mm &apos;minutes and&apos; ss &apos;seconds&apos;</source>
        <translation>hh &apos;heures,&apos; mm &apos;minutes et&apos; ss &apos;secondes&apos;</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="720"/>
        <source>Wrong image?</source>
        <translation>Pas la bonne image ?</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="721"/>
        <source>Image &quot;%1&quot; was taken %2 before the next trackpoint was recorded.
Do you still want to use it?</source>
        <translation>L&apos;image &quot;%1&quot; a été prise %2 avant que le point de traçe suivant ne soit enregistré.
Voulez vous quand même l&apos;utiliser ?</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="722"/>
        <source>Image &quot;%1&quot; was taken %2 after the last trackpoint was recorded.
Do you still want to use it?</source>
        <translation>L&apos;image &quot;%1&quot; a été prise %2 après que le point de traçe suivant ne soit enregistré.
Voulez vous quand même l&apos;utiliser ?</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="481"/>
        <source>No such file</source>
        <translation>Fichier inconnu</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="50"/>
        <source>Network timeout</source>
        <translation>Dépassement de temps réseau</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="50"/>
        <source>Cannot read the photo&apos;s details from the Walking Papers server.</source>
        <translation>Impossible d&apos;obtenir les détails de la photo depuis le serveur &quot;Walking Papers&quot;.</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="70"/>
        <source>Please specify Walking Papers URL</source>
        <translation>Veuillez spécifier l&apos;URL Walking Papers</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="71"/>
        <source>URL:</source>
        <translation>URL :</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="92"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="904"/>
        <source>Center map</source>
        <translation>Centrer la carte</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="99"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="909"/>
        <source>Save geotagged image...</source>
        <translation>Sauvergarder l&apos;image géomarquée</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="298"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="403"/>
        <source>Photo layer</source>
        <translation>Couche Photo</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="481"/>
        <source>Can&apos;t find image &quot;%1&quot;.</source>
        <translation>Impossible de trouver l&apos;image &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="489"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="491"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="847"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="851"/>
        <source>Exiv2</source>
        <translation>Exiv2</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="489"/>
        <source>Error while opening &quot;%2&quot;:
%1</source>
        <translation>Erreur lors du chargement de &quot;%2&quot;.
%1</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="708"/>
        <source>No TrackPoints</source>
        <translation>Pas de points de trace</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="708"/>
        <source>No TrackPoints found for image &quot;%1&quot;</source>
        <translation>Pas de points de trace trouvé pour l&apos;image &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="774"/>
        <source>JPEG Images (*.jpg)</source>
        <translation>Images JPEG (*.jpg)</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="491"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="851"/>
        <source>Error while loading EXIF-data from &quot;%1&quot;.</source>
        <translation>Erreur lors du chargement des données EXIF de &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="847"/>
        <source>Error while opening &quot;%1&quot;:
%2</source>
        <translation>Erreur à l&apos;ouverture de &quot;%1&quot;.
%2</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="95"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="907"/>
        <source>Select next image</source>
        <translation>Sélectionner l&apos;image suivante</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="96"/>
        <source>PgDown</source>
        <translation>PgDown</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="97"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="908"/>
        <source>Select previous image</source>
        <translation>Sélectionner l&apos;image précédente</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="98"/>
        <source>PgUp</source>
        <translation>PgUp</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="356"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="461"/>
        <source>Layer is readonly</source>
        <translation>La couche est en lecture seule</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="357"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="462"/>
        <source>The used layer is not writeable. Should it be made writeable?
If not, you can&apos;t load images that belongs to it.</source>
        <translation>La couche utilisée est en lecture seule. La rendre inscriptible?
Sinon, vous ne pourrez pas charger les images lui appartenant.</translation>
    </message>
</context>
<context>
    <name>GotoDialog</name>
    <message>
        <location filename="../src/GotoDialog.cpp" line="109"/>
        <source>Invalid OSM url</source>
        <translation>url OSM invalide</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.cpp" line="110"/>
        <source>The specified url is invalid!</source>
        <translation>L&apos;url spécifiée est invalide!</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.cpp" line="118"/>
        <location filename="../src/GotoDialog.cpp" line="127"/>
        <source>Invalid Coordinates format</source>
        <translation>Format de coordonnées non valable</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.cpp" line="119"/>
        <source>Coordinates must be: &apos;&lt;left lon&gt;, &lt;bottom lat&gt;, &lt;right lon&gt;, &lt;top lat&gt;&apos;</source>
        <translation>Les coordonnées doivent être: &apos;&lt;lon gauche&gt;, &lt;lat bas&gt;, &lt;lon droite&gt;, &lt;lat haut&gt;&apos;</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.cpp" line="128"/>
        <source>Coordinates must be: &apos;&lt;center lat&gt;, &lt;center lon&gt;, &lt;span lat&gt;, &lt;span lon&gt;&apos;</source>
        <translation>Les coordonnées doivent être: &apos;&lt;lat centre&gt;, &lt;lon centre&gt;, &lt;lat intervalle&gt;, &lt;lon intervalle&gt;&apos;</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="14"/>
        <source>Go To</source>
        <translation>Aller à</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="28"/>
        <source>Bookmark</source>
        <translation>Marque-page</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="61"/>
        <source>WWW link (OSM/google maps)</source>
        <translation>Lien WWW (OSM/google maps)</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="88"/>
        <source>lonMin, latMin, lonMax, latMax</source>
        <translation>lonMin, latMin, lonMax, latMax</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="115"/>
        <source>latCent, lonCent, latSpan, lonSpan</source>
        <translation>lat-Centre, lon-Centre, Intervalle-lat, Intervalle-lon</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="134"/>
        <source>Info</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="148"/>
        <source>OSM API Url</source>
        <translation>OSM API Url</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="175"/>
        <source>OSM XAPI url</source>
        <translation>OSM XAPI url</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="197"/>
        <location filename="../src/GotoDialog.ui" line="210"/>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
</context>
<context>
    <name>ImageLayerWidget</name>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="287"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="293"/>
        <source>Shape adapter</source>
        <translation>Adaptateur d&apos;enveloppe</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="371"/>
        <source>WMS adapter</source>
        <translation>Adapteur WMS</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="387"/>
        <source>TMS adapter</source>
        <translation>Adapteur TMS</translation>
    </message>
</context>
<context>
    <name>ImageMapLayer</name>
    <message>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="66"/>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="156"/>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="193"/>
        <source>Map - None</source>
        <translation>Carte - Aucune</translation>
    </message>
    <message>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="167"/>
        <source>Map - WMS - %1</source>
        <translation>Carte - WMS - %1</translation>
    </message>
    <message>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="172"/>
        <source>Map - WMS-C - %1</source>
        <translation>Carte - WMS-C - %1</translation>
    </message>
    <message>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="177"/>
        <source>Map - WMS-Tiled - %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="188"/>
        <source>Map - TMS - %1</source>
        <translation>Carte - TMS - %1</translation>
    </message>
    <message>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="209"/>
        <source>Map - OSB Background</source>
        <translation>Carte - Arrière-plan OSB</translation>
    </message>
    <message>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="215"/>
        <source>Map - %1</source>
        <translation>Carte - %1</translation>
    </message>
</context>
<context>
    <name>ImportCSVDialog</name>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="31"/>
        <source>String</source>
        <translation>Chaîne de caractères</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="32"/>
        <source>Integer</source>
        <translation>Entier</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="33"/>
        <source>Float</source>
        <translation>Flottant</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="34"/>
        <source>Longitude</source>
        <translation>Longitude</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="35"/>
        <source>Latitude</source>
        <translation>Latitude</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="302"/>
        <source>Invalid projection</source>
        <translation>Projection invalide</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="302"/>
        <source>Unable to set projection.</source>
        <translation>Impossible de spécifier la projection.</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="321"/>
        <source>No coordinates</source>
        <translation>Pas de coordonnées</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="322"/>
        <source>Latitude or Longitude field missing. It will be impossible to import the file.
Do you really want to exit?</source>
        <translation>Le champ Latitude ou Longitude est manquant. Il sera impossible d&apos;importer le fichier.
Désirez-vous réellement quitter?</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="359"/>
        <source>Load CSV import settings</source>
        <translation>Charger une configuration d&apos;import CSV</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="359"/>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="469"/>
        <source>Merkaartor import settings (*.mis)</source>
        <translation>Configuration d&apos;import Merkaartor (*.mis)</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="365"/>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="378"/>
        <source>Invalid file</source>
        <translation>Fichier invalide</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="365"/>
        <source>%1 could not be opened.</source>
        <translation>%1 n&apos;a pas pu être ouvert.</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="378"/>
        <source>%1 is not a CSV import settings file</source>
        <translation>%1 n&apos;est pas un fichier de configuration d&apos;import CSV</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="469"/>
        <source>Save CSV import settings</source>
        <translation>Sauvegarder la configuration d&apos;import CSV</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="478"/>
        <source>Unable to open save import settings</source>
        <translation>Impossible d&apos;ouvrir la configuration d&apos;import</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="478"/>
        <source>%1 could not be opened for writing.</source>
        <translation>%1 n&apos;a pas pu être ouvert en écriture.</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Boîte de dialogue</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="20"/>
        <source>Input</source>
        <translation>Entrée</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="49"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Courier New&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;1&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;3&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;4&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Courier New&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;1&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;3&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;4&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="66"/>
        <source>OSM Preview</source>
        <translation>Prévisualisation OSM</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="84"/>
        <source>Specifications</source>
        <translation>Spécifications</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="96"/>
        <source>First line contains headers</source>
        <translation>La première ligne contient les entêtes</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="103"/>
        <source>Delimiter</source>
        <translation>Séparateur</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="115"/>
        <source>Colon (,)</source>
        <translation>Virgule (,)</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="122"/>
        <source>Semicolon (;)</source>
        <translation>Point-virgule (;)</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="129"/>
        <source>Tab</source>
        <translation>Tabulation</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="136"/>
        <source>Other:</source>
        <translation>Autre :</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="162"/>
        <source>Projection (in PROJ4 format; leave blank for latitude/longitude)</source>
        <translation>Projection (en format PROJ4; laisser à blanc pour latitude/longitude)</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="175"/>
        <source>Fields</source>
        <translation>Champs</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="202"/>
        <source>Field list:</source>
        <translation>Liste des champs :</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="231"/>
        <source>Field Name (= tag key) :</source>
        <translation>Nom du champ (= clé d&apos;étiquette) :</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="248"/>
        <source>Field Type:</source>
        <translation>Type de champ :</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="258"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="284"/>
        <source>Import range</source>
        <translation>Importer une plage</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="290"/>
        <source>From (0 from start):</source>
        <translation>A partir de (0 pour le départ) :</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="300"/>
        <source>To (0 to End):</source>
        <translation>À (0 pour la fin) :</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="315"/>
        <source>Load settings...</source>
        <translation>Charger une configuration...</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="322"/>
        <source>Save settings...</source>
        <translation>Sauvergarder la configuration...</translation>
    </message>
</context>
<context>
    <name>InfoDock</name>
    <message>
        <location filename="../src/Docks/InfoDock.cpp" line="24"/>
        <location filename="../src/Docks/InfoDock.cpp" line="94"/>
        <source>Info</source>
        <translation>Information</translation>
    </message>
</context>
<context>
    <name>Layer</name>
    <message numerus="yes">
        <location filename="../src/Layers/Layer.cpp" line="566"/>
        <source>%n features</source>
        <translation type="unfinished">
            <numerusform>%n objets</numerusform>
            <numerusform>%n fonctionnalités</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="566"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
</context>
<context>
    <name>LayerDock</name>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="364"/>
        <source>Layers</source>
        <translation>Couches</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="370"/>
        <source>All</source>
        <translation>Tous</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="371"/>
        <source>Default</source>
        <translation>Par défaut</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="372"/>
        <source>OSM</source>
        <translation>OSM</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="373"/>
        <source>Tracks</source>
        <translation>Pistes</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="222"/>
        <location filename="../src/Docks/LayerDock.cpp" line="321"/>
        <source>Show All</source>
        <translation>Afficher tout</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="227"/>
        <location filename="../src/Docks/LayerDock.cpp" line="325"/>
        <source>Hide All</source>
        <translation>Cacher tout</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="234"/>
        <source>Readonly All</source>
        <translation>Tout en lecture seule</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="239"/>
        <source>Readonly None</source>
        <translation>Aucun en lecture seule</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="246"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>LayerWidget</name>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="192"/>
        <source>Zoom</source>
        <translation>Faire un zoom</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="174"/>
        <source>Low</source>
        <translation>Bas</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="174"/>
        <source>High</source>
        <translation>Haut</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="174"/>
        <source>Opaque</source>
        <translation>Opaque</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="177"/>
        <source>Opacity</source>
        <translation>Opacité</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="160"/>
        <source>Visible</source>
        <translation>Visible</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="166"/>
        <source>Readonly</source>
        <translation>Lecture seule</translation>
    </message>
</context>
<context>
    <name>MDiscardableDialog</name>
    <message>
        <location filename="../src/Utils/MDiscardableDialog.cpp" line="24"/>
        <source>Don&apos;t ask me this again</source>
        <translation>Ne plus me demander ceci</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../src/Main.cpp" line="178"/>
        <source>Merkaartor v%1%2(%3)
Loading plugins...</source>
        <translation>Merkaartor v%1%2(%3)
Chargement des greffons...</translation>
    </message>
    <message>
        <location filename="../src/Main.cpp" line="210"/>
        <source>Merkaartor v%1%2(%3)
Initializing...</source>
        <translation>Merkaartor v%1%2(%3)
Initialisation...</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/ImportExport/ImportExportOSC.cpp" line="69"/>
        <source>Import osmChange</source>
        <translation>Importer osmChange</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="966"/>
        <source>Open track file</source>
        <translation>Ouvrir un fichier de traces</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="992"/>
        <location filename="../src/Sync/SyncOSM.cpp" line="43"/>
        <source>Unsaved changes</source>
        <translation>Changements non sauvegardés</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="993"/>
        <source>The current map contains unsaved changes that will be lost when starting a new one.
Do you want to cancel starting a new map or continue and discard the old changes?</source>
        <translation>Le document contient des changements non-sauvegardés qui seront perdus.
Voulez-vous annuler le chargement d&apos;un nouveau document ou continuer et perdre les changements ?</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1147"/>
        <source>No valid file</source>
        <translation>Fichier invalide</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1147"/>
        <location filename="../src/MainWindow.cpp" line="2294"/>
        <source>%1 could not be opened.</source>
        <translation>%1 n&apos;a pas pu être ouvert.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1311"/>
        <source>Upload OSM</source>
        <translation>Téléverser OSM</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1332"/>
        <location filename="../src/MainWindow.cpp" line="1346"/>
        <location filename="../src/MainWindow.cpp" line="1370"/>
        <source>Error downloading</source>
        <translation>Erreur de téléchargement</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1332"/>
        <location filename="../src/MainWindow.cpp" line="1346"/>
        <location filename="../src/MainWindow.cpp" line="1370"/>
        <source>The map could not be downloaded</source>
        <translation>La carte n&apos;a pas pu être téléchargée</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1359"/>
        <source>Error downloading OpenStreetBugs</source>
        <translation>Erreur lors du téléchargement deuis OpenStreetBugs</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1359"/>
        <source>The OpenStreetBugs could not be downloaded</source>
        <translation>Le téléchargement OpenStreetBugs a échoué</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1713"/>
        <source>Simplify Roads</source>
        <translation>Simplifier les Routes</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1769"/>
        <source>Network timeout</source>
        <translation>Dépassement de temps réseau</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1769"/>
        <source>Cannot contact OpenStreetBugs.</source>
        <translation>Impossible de contacter OpenStreetBugs.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1779"/>
        <source>Error closing bug</source>
        <translation>Erreur lors de la cloture du bogue</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1779"/>
        <source>Cannot delete bug. Server message is:
%1</source>
        <translation>Impossible de supprimer le bug. Le message du serveur est:
%1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1808"/>
        <source>Add Street Numbers</source>
        <translation>Ajout de Numéros de rue</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1826"/>
        <source>Number of segments to divide into</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1829"/>
        <source>Subdivide road into %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1858"/>
        <source>Spread Nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1925"/>
        <source>Split area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1940"/>
        <source>Terrace area into %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2020"/>
        <source>Save map style</source>
        <translation>Sauvergarder le style de carte</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2020"/>
        <location filename="../src/MainWindow.cpp" line="2031"/>
        <source>Merkaartor map style (*.mas)</source>
        <translation>Style de carte Merkaartor (*.mas)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2031"/>
        <source>Load map style</source>
        <translation>Charger un Style de carte</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2375"/>
        <source>Export OSM</source>
        <translation>Exporter OSM</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2375"/>
        <source>OSM Files (*.osm)</source>
        <translation>Fichiers OSM (*.osm)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2419"/>
        <source>Export osmChange</source>
        <translation>Exporter osmChange</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2419"/>
        <source>osmChange Files (*.osc)</source>
        <translation>Fichiers osmChange (*.osc)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2771"/>
        <source>Add Bookmark</source>
        <translation>Ajouter un marque-page</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2772"/>
        <source>Specify the name of the bookmark.</source>
        <translation>Spécifiez le nom du signet.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2775"/>
        <source>Invalid bookmark name</source>
        <translation>Nom de signet invalide</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2776"/>
        <source>Bookmark cannot be blank.</source>
        <translation>Un marque-page ne peut être vide.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2780"/>
        <source>Warning: Bookmark name already exists</source>
        <translation>Attention: le nom du marque-page existe déjà</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2781"/>
        <source>Enter a new one, keep the same to overwrite or cancel.</source>
        <translation>Entrez un nouveau nom, gardez le même pour remplacer ou annulez.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2819"/>
        <source>Remove Bookmark</source>
        <translation>Supprimer le signet</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2820"/>
        <source>Select the bookmark to remove.</source>
        <translation>Sélectionnez le signet à supprimer.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="14"/>
        <source>Merkaartor</source>
        <translation>Merkaartor</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="32"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="38"/>
        <source>&amp;Create</source>
        <translation>&amp;Créer</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="51"/>
        <location filename="../src/MainWindow.ui" line="608"/>
        <source>&amp;Road</source>
        <translation>&amp;Route</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="66"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editer</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="86"/>
        <source>&amp;View</source>
        <translation>&amp;Vue</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="90"/>
        <source>&amp;Bookmarks</source>
        <translation>&amp;Signets</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="98"/>
        <source>Set &amp;projection</source>
        <translation>Spécifier la &amp;projection</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="127"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="175"/>
        <source>&amp;Style</source>
        <translation>&amp;Style</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="209"/>
        <location filename="../src/MainWindow.ui" line="626"/>
        <source>&amp;Node</source>
        <translation>&amp;Nœud</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="378"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="383"/>
        <source>&amp;About</source>
        <translation>&amp;A propos</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="392"/>
        <source>&amp;Open</source>
        <translation>&amp;Ouvrir</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="395"/>
        <source>Create a new document and import a file</source>
        <translation>Crée un nouveau document et importe un fichier</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="407"/>
        <source>Zoom &amp;all</source>
        <translation>Faire un zoom sur &amp;tout</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="410"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="415"/>
        <source>Zoom &amp;window</source>
        <translation>Faire un zoom sur la &amp;fenêtre</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="418"/>
        <location filename="../src/MainWindow.ui" line="421"/>
        <source>Zoom window</source>
        <translation>Zoomer la fenêtre</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="424"/>
        <source>F3</source>
        <translation>F3</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="433"/>
        <source>Zoom &amp;out</source>
        <translation>Zoom a&amp;rrière</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="445"/>
        <source>Zoom &amp;in</source>
        <translation>Zoom &amp;avant</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="448"/>
        <location filename="../src/MainWindow.ui" line="451"/>
        <source>Zoom in</source>
        <translation>Zoom avant</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="459"/>
        <location filename="../src/MainWindow.ui" line="462"/>
        <location filename="../src/MainWindow.ui" line="465"/>
        <source>Curved link</source>
        <translation>Liaison courbe</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="477"/>
        <location filename="../src/MainWindow.ui" line="996"/>
        <source>&amp;Undo</source>
        <translation>&amp;Défaire</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="480"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="489"/>
        <source>&amp;Redo</source>
        <translation>&amp;Refaire</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="492"/>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="516"/>
        <source>&amp;Import</source>
        <translation>&amp;Importer</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="519"/>
        <source>Import a file into the current document</source>
        <translation>Importe un fichier dans le document courant</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="534"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="539"/>
        <source>Link</source>
        <translation>Lien</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="542"/>
        <location filename="../src/MainWindow.ui" line="545"/>
        <source>Create link</source>
        <translation>Créer une liaison</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="548"/>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="563"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="578"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="590"/>
        <source>R&amp;emove</source>
        <translation>&amp;Supprimer</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="596"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="644"/>
        <source>&amp;Reverse</source>
        <translation>&amp;Inverser le sens</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="664"/>
        <source>&amp;Double carriage way</source>
        <translation>Route à &amp;double sens</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="672"/>
        <source>&amp;Roundabout</source>
        <translation>&amp;Rond-point</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="684"/>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="687"/>
        <source>Create a new document</source>
        <translation>Crée un nouveau document</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="696"/>
        <source>&amp;Split</source>
        <translation>&amp;Séparer</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="711"/>
        <source>&amp;Join</source>
        <translation>&amp;Joindre</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="726"/>
        <source>&amp;Break apart</source>
        <translation>&amp;Séparer à part</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="740"/>
        <source>Re&amp;lation</source>
        <translation>Re&amp;lation</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="755"/>
        <source>&amp;Area</source>
        <translation>&amp;Surface</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="872"/>
        <source>&amp;Save</source>
        <translation>&amp;Sauvegarder</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="778"/>
        <source>&amp;Curved road</source>
        <translation>Route &amp;sinueuse</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="787"/>
        <source>&amp;Preferences...</source>
        <translation>&amp;Préférences...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="792"/>
        <location filename="../src/MainWindow.ui" line="800"/>
        <source>&amp;All...</source>
        <translation>&amp;Tous...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="795"/>
        <location filename="../src/MainWindow.ui" line="803"/>
        <source>Export all visible layers to a file</source>
        <translation>Exporte toutes les couches visibles vers un fichier</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="823"/>
        <location filename="../src/MainWindow.ui" line="831"/>
        <source>&amp;Viewport...</source>
        <translation>Fenêtre de &amp;Vue...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="826"/>
        <location filename="../src/MainWindow.ui" line="834"/>
        <source>Export the features in the viewport to a file</source>
        <translation>Exporte les éléments de la vue vers un fichier</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="839"/>
        <source>&amp;Add...</source>
        <translation>&amp;Ajouter...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="844"/>
        <source>&amp;Remove...</source>
        <translation>&amp;Supprimer...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="849"/>
        <source>&amp;Merge</source>
        <translation>&amp;Fusionner</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="855"/>
        <source>Merge the selected nodes (first selected will remain)</source>
        <translation>Fusionne les nœuds sélectionnés (le premier sélectionné restera)</translation>
    </message>
    <message>
        <location filename="../src/Sync/SyncOSM.cpp" line="17"/>
        <source>Unresolved conflicts</source>
        <translation>Conflits non-résolus</translation>
    </message>
    <message>
        <location filename="../src/Sync/SyncOSM.cpp" line="17"/>
        <source>Please resolve existing conflicts first</source>
        <translation>Veuillez d&apos;abord résoudre les conflits existants</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1311"/>
        <source>You don&apos;t seem to have specified your
OpenStreetMap username and password.
Do you want to do this now?</source>
        <translation>Il semble que vous n&apos;avez pas spécifié
vos identifiant et mot-de-passe OpenStreetMap.
Voulez-vous le faire maintenant ?</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2230"/>
        <source>Save Merkaartor document</source>
        <translation>Enregistrer un document Merkaartor</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2230"/>
        <source>Merkaartor documents Files (*.mdc)</source>
        <translation>Fichiers document Merkaartor (*.mdc)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="398"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="632"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="863"/>
        <source>Save &amp;As...</source>
        <translation>Sauvegarder &amp;sous...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="878"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="887"/>
        <source>Download more</source>
        <translation>Télécharger plus</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="893"/>
        <location filename="../src/MainWindow.ui" line="896"/>
        <source>Download the current view to the previous download layer</source>
        <translation>Télécharge la vue courante dans la derniére couche de téléchargement</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="899"/>
        <source>Ctrl+Shift+D</source>
        <translation>Ctrl+Shift+D</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1302"/>
        <source>Old Qt version detected</source>
        <translation>Version ancienne de Qt détectée</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1303"/>
        <source>Your setup uses Qt %1, which contains various known errors in uploading data to OpenStreetMap leading to 401 server response codes. Are you sure you want to continue (which is not recommended).
For more information see http://wiki.openstreetmap.org/index.php/Problem_uploading_with_Merkaartor</source>
        <translation>Votre configuration utilise Qt %1, qui contient un certain nombre d&apos;erreurs connues lors du chargement vers OpenStreetMap, menant à des codes de réponse 401 du serveur. Etes-vous certain de vouloir continuer (ce qui n&apos;est pas recommandé) ?
Pour plus d&apos;information, voir http://wiki.openstreetmap.org/index.php/Problem_uploading_with_Merkaartor (en anglais)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2230"/>
        <source>untitled</source>
        <translation>sans titre</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="218"/>
        <source>&amp;Window</source>
        <translation>&amp;Fenêtre</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="222"/>
        <location filename="../src/MainWindow.ui" line="904"/>
        <source>&amp;Docks</source>
        <translation>&amp;Encarts</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="915"/>
        <location filename="../src/MainWindow.ui" line="918"/>
        <source>Hide/Show the Properties dock</source>
        <translation>Cacher/afficher l&apos;encart Propriétés</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="921"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="932"/>
        <location filename="../src/MainWindow.ui" line="935"/>
        <source>Hide/Show the Layers dock</source>
        <translation>Cacher/afficher l&apos;encart Couches</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="938"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="949"/>
        <location filename="../src/MainWindow.ui" line="952"/>
        <source>Hide/Show the Info dock</source>
        <translation>Cacher/afficher l&apos;encart Informations</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="955"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="436"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="454"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="507"/>
        <source>Ctrl+M</source>
        <translation>Ctrl+M</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="614"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="702"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="717"/>
        <source>Alt+J</source>
        <translation>Alt+J</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="735"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="812"/>
        <source>&amp;Find...</source>
        <translation>&amp;Rechercher...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="818"/>
        <source>Find and select items</source>
        <translation>Recherche et sélectionne des éléments</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="858"/>
        <source>Alt+M</source>
        <translation>Alt+M</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="964"/>
        <source>&amp;Align</source>
        <translation>&amp;Aligner</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="970"/>
        <source>Align selected nodes. First two selected give the line.</source>
        <translation>Aligne les nœuds sélectionnés. Les deux premiers sélectionnés indiquent la ligne.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="973"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1290"/>
        <source>Open track files</source>
        <translation>Ouvrir un fichier de traces</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2294"/>
        <location filename="../src/MainWindow.cpp" line="2300"/>
        <location filename="../src/MainWindow.cpp" line="2310"/>
        <source>Invalid file</source>
        <translation>Fichier invalide</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2300"/>
        <source>%1 is not a valid XML file.</source>
        <translation>%1 n&apos;est pas un fichier XML valide.</translation>
    </message>
    <message>
        <location filename="../src/Sync/SyncOSM.cpp" line="44"/>
        <source>It is strongly recommended to save the changes to your document after an upload.
Do you want to do this now?</source>
        <translation>Il est fortement recommandé de sauvegarder les changements de votre document après un télévesage.
Voulez-vous le faire maintenant ?</translation>
    </message>
    <message>
        <location filename="../src/Commands/DocumentCommands.cpp" line="124"/>
        <source>Cascaded cleanup</source>
        <translation>Nettoyage en cascade</translation>
    </message>
    <message>
        <location filename="../src/Commands/FeatureCommands.cpp" line="66"/>
        <location filename="../src/Commands/FeatureCommands.cpp" line="74"/>
        <location filename="../src/Commands/FeatureCommands.cpp" line="188"/>
        <source>Set Tag &apos;%1=%2&apos; on %3</source>
        <translation>Mettre l&apos;étiquette &apos;%1&apos; à &apos;%2&apos; sur %3</translation>
    </message>
    <message>
        <location filename="../src/Commands/FeatureCommands.cpp" line="307"/>
        <location filename="../src/Commands/FeatureCommands.cpp" line="396"/>
        <location filename="../src/Docks/PropertiesDock.cpp" line="703"/>
        <source>Clear Tag &apos;%1&apos; on %2</source>
        <translation>Supprimer l&apos;étiquette &apos;%1&apos; sur %2</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportNMEA.cpp" line="44"/>
        <source>Import NMEA</source>
        <translation>Importer NMEA</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateAreaInteraction.cpp" line="103"/>
        <source>Create Area %1</source>
        <translation>Créer Surface %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateAreaInteraction.cpp" line="138"/>
        <source>Area: Create Road %1</source>
        <translation>Surface: Créer Route %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateAreaInteraction.cpp" line="169"/>
        <source>Area: Finish Road %1</source>
        <translation>Surface: Terminer Route %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateAreaInteraction.cpp" line="186"/>
        <location filename="../src/Interactions/CreateAreaInteraction.cpp" line="199"/>
        <source>Area: Add node %1 to Road %2</source>
        <translation>Zone: Ajouter le nœud %1 à la route %2</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateDoubleWayInteraction.cpp" line="165"/>
        <source>Add nodes to double-way Road %1</source>
        <translation>Ajouter des nœuds à la route à double-sens %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateDoubleWayInteraction.cpp" line="220"/>
        <source>Create double-way Road %1</source>
        <translation>Créer la Route à double-sens %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateNodeInteraction.cpp" line="82"/>
        <location filename="../src/Interactions/MoveNodeInteraction.cpp" line="240"/>
        <source>Create node in Road: %1</source>
        <translation>Créer un nœud sur la route: %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateNodeInteraction.cpp" line="98"/>
        <source>Create point %1</source>
        <translation>Créer le point %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateRoundaboutInteraction.cpp" line="87"/>
        <source>Create Roundabout %1</source>
        <translation>Créer le Rond-point %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateSingleWayInteraction.cpp" line="130"/>
        <location filename="../src/Interactions/CreateSingleWayInteraction.cpp" line="181"/>
        <location filename="../src/Interactions/CreateSingleWayInteraction.cpp" line="194"/>
        <source>Create Node %1 in Road %2</source>
        <translation>Créer le Nœud %1 sur la route %2</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateSingleWayInteraction.cpp" line="163"/>
        <source>Create Road: %1</source>
        <translation>Créer la Route: %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/EditInteraction.cpp" line="248"/>
        <source>Remove feature %1</source>
        <translation>Supprimer l&apos;objet %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/EditInteraction.cpp" line="278"/>
        <source>Reverse Road %1</source>
        <translation>Inverser le sens de la Route %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="694"/>
        <source>Clipboard invalid</source>
        <translation>Presse-papiers non valable</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1671"/>
        <source>Join Roads</source>
        <translation>Joindre des Routes</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1685"/>
        <source>Split Roads</source>
        <translation>Séparer des Routes</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1699"/>
        <source>Break Roads</source>
        <translation>Casser des Routes</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1970"/>
        <source>Create Relation %1</source>
        <translation>Créer la Relation %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1844"/>
        <source>Align Nodes</source>
        <translation>Aligner les nœuds</translation>
    </message>
    <message>
        <location filename="../src/Interactions/MoveNodeInteraction.cpp" line="203"/>
        <location filename="../src/MainWindow.cpp" line="1872"/>
        <source>Merge Nodes into %1</source>
        <translation>Fusionner des Nœuds dans %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="999"/>
        <location filename="../src/MainWindow.ui" line="1002"/>
        <source>Hide/Show the Undo dock</source>
        <translation>Cacher/afficher l&apos;encart Défaire</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1016"/>
        <source>Ctrl+Alt+A</source>
        <translation>Ctrl+Alt+A</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1031"/>
        <source>Copy the selected feature&apos;s tags to the clipboard; if the feature is a trackpoint, copy the coordinates, too.</source>
        <translation>Copie les étiquettes de l&apos;objet sélectionné vers le clipboard; Si l&apos;objet est un noeud, copie également les coordonnées.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1034"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1042"/>
        <source>Paste (and overwrite) the tags in the clipboard to the selected feature.</source>
        <translation>Colle (et écrase) les étiquettes de l&apos;objet sélectionné à partir du clipboard.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1045"/>
        <source>Ctrl+V, O</source>
        <translation>Ctrl+V, O</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1063"/>
        <source>Merge the tags in the clipboard with the ones of the selected feature.</source>
        <translation>Fusionne les étiquettes du clipboard avec celles de l&apos;objet sélectionné.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1066"/>
        <source>Ctrl+V, M</source>
        <translation>Ctrl+V, M</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1071"/>
        <location filename="../src/MainWindow.ui" line="1076"/>
        <source>Selected...</source>
        <translation>Sélectionné...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1085"/>
        <source>Paste Features</source>
        <translation>Coller Objets</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1094"/>
        <source>Paste the features in the clipboard; If the features&apos;id are already in the document, overwrite them.</source>
        <translation>Colle les objets du clipboard; Si les identificateurs des objets sont déjà dans le document, ces derniers seront écrasés.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1097"/>
        <location filename="../src/MainWindow.ui" line="1540"/>
        <source>Ctrl+V, F</source>
        <translation>Ctrl+V, F</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportGPX.cpp" line="290"/>
        <source>Import GPX</source>
        <translation>Importer GPX</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportNGT.cpp" line="19"/>
        <source>Import NGT</source>
        <translation>Importer NGT</translation>
    </message>
    <message>
        <location filename="../src/Features/Relation.cpp" line="753"/>
        <source>Relation Modified %1</source>
        <translation>Relation Modifiée %1</translation>
    </message>
    <message>
        <location filename="../src/Features/Way.cpp" line="767"/>
        <source>Do you want to delete the children nodes also?</source>
        <translation>Voulez-vous également supprimer les nœuds enfants?</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2310"/>
        <source>%1 is not a valid Merkaartor document.</source>
        <translation>%1 n&apos;est pas un document Merkaartor valide.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2398"/>
        <source>Export Binary OSM</source>
        <translation>Exporter OSM binaire</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2398"/>
        <source>OSM Binary Files (*.osb)</source>
        <translation>Fichiers OSM binaires (*.osb)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2445"/>
        <source>Export GPX</source>
        <translation>Exporter GPX</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2445"/>
        <source>GPX Files (*.gpx)</source>
        <translation>Fichiers GPX (*.gpx)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2470"/>
        <source>Export KML</source>
        <translation>Exporter KML</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2470"/>
        <source>KML Files (*.kml)</source>
        <translation>Fichiers KML (*.kml)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1005"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1105"/>
        <source>OSM (XML)</source>
        <translation>OSM (XML)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1110"/>
        <source>OSM (Binary)</source>
        <translation>OSM (Binaire)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1121"/>
        <location filename="../src/MainWindow.ui" line="1124"/>
        <source>Commit the selected feature from a non-uploadable layer (e.g.Track or Extract) to the dirty layer, ready for upload</source>
        <translation>Déplace les objets sélectionné d&apos;une couche non-téléversable vers la couche modification, prêts pour téléversage</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1129"/>
        <source>GPX</source>
        <translation>GPX</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1134"/>
        <source>KML</source>
        <translation>KML</translation>
    </message>
    <message>
        <location filename="../src/TagModel.cpp" line="133"/>
        <location filename="../src/TagModel.cpp" line="164"/>
        <source>Set Tags on %1</source>
        <translation>Mettre Étiquettes sur %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateSingleWayInteraction.cpp" line="171"/>
        <source>Create Node: %1</source>
        <translation>Créer le nœud: %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateSingleWayInteraction.cpp" line="197"/>
        <source>Add Node %1 to Road %2</source>
        <translation>Ajouter le nœud %1 à la route %2</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="180"/>
        <source>tile %v / %m</source>
        <translation>tuile %v / %m</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="315"/>
        <source>Properties...</source>
        <translation>Propriétés...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1021"/>
        <source>Waypoints</source>
        <translation>Noeud intermédiaire</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1041"/>
        <source>Track %1</source>
        <translation>Trace %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="3028"/>
        <source>GPS error</source>
        <translation>Erreur du GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="3029"/>
        <source>Unable to open GPS port.</source>
        <translation>Impossible d&apos;ouvrir le port de l&apos;instrument GPS.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="3038"/>
        <source>Open NMEA log file</source>
        <translation>Ouvrir un fichier de journalisation NMEA</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="256"/>
        <location filename="../src/MainWindow.ui" line="929"/>
        <source>&amp;Layers</source>
        <translation>&amp;Calques</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1013"/>
        <source>Show &amp;downloaded areas</source>
        <translation>Afficher les aires &amp;téléchargées</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1139"/>
        <source>Toggle Toolbar</source>
        <translation>Basculer la barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1142"/>
        <location filename="../src/MainWindow.ui" line="1145"/>
        <source>Hide/Show the Toolbar</source>
        <translation>Cacher/afficher la barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1150"/>
        <source>Hide All</source>
        <translation>Cacher tout</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1153"/>
        <location filename="../src/MainWindow.ui" line="1161"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1158"/>
        <source>Show All</source>
        <translation>Afficher tout</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1169"/>
        <source>&amp;Image layer</source>
        <translation>Calque &amp;image</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1185"/>
        <source>Ctrl+Alt+P</source>
        <translation>Ctrl+Alt+P</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1196"/>
        <source>Ctrl+Alt+N</source>
        <translation>Ctrl+Alt+N</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1201"/>
        <source>&amp;Start</source>
        <translation>Démarrer</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1209"/>
        <source>&amp;Replay...</source>
        <translation>&amp;Rejouer...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1223"/>
        <location filename="../src/MainWindow.ui" line="1226"/>
        <source>Hide/Show the GPS dock</source>
        <translation>Cacher/afficher l&apos;encart GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="659"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="108"/>
        <source>&amp;Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="185"/>
        <source>Ta&amp;g templates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1237"/>
        <source>S&amp;top</source>
        <translation>&amp;Arrêter</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1248"/>
        <source>&amp;Center on GPS</source>
        <translation>&amp;Centrer sur le point GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1256"/>
        <source>Show track &amp;segments</source>
        <translation>Afficher les &amp;segments de trace</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1259"/>
        <source>Ctrl+Alt+T</source>
        <translation>Ctrl+Alt+T</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1267"/>
        <source>Show &amp;scale</source>
        <translation>Afficher l&apos;&amp;échelle</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1270"/>
        <source>Ctrl+Alt+S</source>
        <translation>Ctrl+Alt+S</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1278"/>
        <source>Show &amp;relations</source>
        <translation>Afficher les &amp;relations</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1281"/>
        <source>Ctrl+Alt+R</source>
        <translation>Ctrl+Alt+R</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1289"/>
        <source>Show roads background</source>
        <translation>Afficher l&apos;arrière-plan des routes</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1297"/>
        <source>Show roads boundary</source>
        <translation>Afficher les bordures des routes</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1305"/>
        <source>Show touchup</source>
        <translation>Afficher le touchup</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1316"/>
        <location filename="../src/MainWindow.ui" line="1319"/>
        <source>Record</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1333"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1347"/>
        <location filename="../src/MainWindow.ui" line="1350"/>
        <source>Hide/Show the GeoImage dock</source>
        <translation>Cacher/afficher l&apos;encart GeoImage</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1353"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../src/Commands/NodeCommands.cpp" line="17"/>
        <location filename="../src/Commands/NodeCommands.cpp" line="25"/>
        <location filename="../src/Commands/NodeCommands.cpp" line="110"/>
        <source>Move node %1</source>
        <translation>Déplacer le nœud %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/MoveNodeInteraction.cpp" line="193"/>
        <source>Nodes at the same position found.</source>
        <translation>Nœuds trouvés avec la même position</translation>
    </message>
    <message>
        <location filename="../src/Interactions/MoveNodeInteraction.cpp" line="194"/>
        <source>Do you want to merge all nodes at the drop position?</source>
        <translation>Voulez-vous fusionner tous les nœuds à la destination ?</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="345"/>
        <source>Low</source>
        <translation>Bas</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="345"/>
        <source>High</source>
        <translation>Haut</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="345"/>
        <source>Opaque</source>
        <translation>Opaque</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="457"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="461"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="465"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="469"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="473"/>
        <source>Directional Arrows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="477"/>
        <source>GPS</source>
        <translation>GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="481"/>
        <source>Layers</source>
        <translation>Couches</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="485"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="489"/>
        <source>Feature</source>
        <translation>Objet</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="497"/>
        <source>Node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="501"/>
        <source>Way</source>
        <translation>Route</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="505"/>
        <source>Relation</source>
        <translation>Relation</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="509"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="513"/>
        <source>Windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="517"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1084"/>
        <source>Big Fat Copyright Warning</source>
        <translation>Gros lourd avertissement de droits d&apos;auteur</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1085"/>
        <source>You are trying to import a KML file. Please be aware that:

 - You cannot import to OSM a KML file created from Google Earth. While you might
   think that nodes you created from GE are yours, they are not!
   They are still a derivative work from GE, and, as such, cannot be used in OSM.

 - If you downloaded it from the Internet, chances are that there is a copyright on it.
   Please be absolutely sure that using those data in OSM is permitted by the author, or
   that the data is public domain.

If unsure, please seek advice on the &quot;legal&quot; or &quot;talk&quot; openstreetmap mailing lists.

Are you absolutely sure this KML can legally be imported in OSM?</source>
        <translation>Vous tentez d&apos;importer un fichier KML. Veuillez noter que:

 - Vous ne pouvez pas importer dans OSM un fichier KML créé par vous dans Google Earth. Vous pourriez penser que les nœuds que vous avez créés sont votre propriété, il n&apos;en est rien!
Ils sont un travail dérivé de GE et ne peuvent pour cela pas être importés dans OSM.

 - Si vous l&apos;avez téléchargé de l&apos;internet, il est probable qu&apos;il soit sous copyright.
Veuillez vous assurer que l&apos;utilisation de ces données dans OSM est permise par l&apos;auteur ou que ces données soient dans le domaine public.

En cas de doute, demandez un avis sur les listes de diffusion OSM &quot;legal&quot; et/ou &quot;talk&quot;.

Êtes-vous absolument certain que ce fichier KML peut être importé légalement dans OSM?</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1887"/>
        <source>Detach Node %1</source>
        <translation>Détacher le nœud %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1901"/>
        <source>Add member to relation</source>
        <translation>Ajouter le membre à la relation</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1913"/>
        <source>Remove member from relation</source>
        <translation>Enlever le membre de la relation</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2273"/>
        <source>Unable to open save file</source>
        <translation>Impossible d&apos;ouvrir le fichier de sauvegarde</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2273"/>
        <source>%1 could not be opened for writing.</source>
        <translation>%1 n&apos;a pas pu être ouvert en écriture.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2893"/>
        <source>Invalid Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2893"/>
        <source>Unable to set filter &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="3154"/>
        <source>Save Tag Templates</source>
        <translation>Sauvergarder Template d&apos;étiquette</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="3154"/>
        <source>Merkaartor tag templates (*.mat)</source>
        <translation>Template d&apos;étiquette Merkaartor (*.mat)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="3166"/>
        <location filename="../src/MainWindow.cpp" line="3180"/>
        <source>Open Tag Templates</source>
        <translation>Ouvrir Template d&apos;étiquette</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="287"/>
        <source>Show directional &amp;Arrows</source>
        <translation>Afficher les flêches directionelles</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="103"/>
        <source>Set Areas &amp;opacity</source>
        <translation>Définir l&apos;&amp;opacité des aires</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="131"/>
        <source>&amp;Export</source>
        <translation>&amp;Exporter</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="142"/>
        <source>Re&amp;cent open</source>
        <translation>Ouvert ré&amp;cemment</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="147"/>
        <source>Recen&amp;t import</source>
        <translation>Impor&amp;ter récemment</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="171"/>
        <source>T&amp;ools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="241"/>
        <source>Fea&amp;ture</source>
        <translation>&amp;Caractéristique</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="493"/>
        <location filename="../src/MainWindow.ui" line="245"/>
        <source>OpenStreetBugs</source>
        <translation>OpenStreetBugs</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="263"/>
        <source>&amp;Gps</source>
        <translation>&amp;Gps</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="276"/>
        <source>Rel&amp;ation</source>
        <translation>Rel&amp;ation</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="283"/>
        <source>S&amp;how</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="331"/>
        <source>Main toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="504"/>
        <source>&amp;Move</source>
        <translation>&amp;Déplacer</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="528"/>
        <source>&amp;Download</source>
        <translation>&amp;Télécharger</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="531"/>
        <source>Download map data for a new area</source>
        <translation>Télécharger des données cartographique pour une nouvelle aire</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="560"/>
        <source>&amp;Select</source>
        <translation>&amp;Sélectionner</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="572"/>
        <source>&amp;Upload</source>
        <translation>&amp;Envoyer</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="575"/>
        <source>Upload changes to the server</source>
        <translation>Téléverser les changements sur le serveur</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="593"/>
        <source>Remove selected features</source>
        <translation>Enlever les caratéristiques sélectionnées</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="611"/>
        <source>Create new road</source>
        <translation>Créer une nouvelle route</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="629"/>
        <source>Create new node</source>
        <translation>Créer un nouveau nœud</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="647"/>
        <source>Reverse road direction</source>
        <translation>Inverser la direction de la route</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="656"/>
        <source>&amp;Go To...</source>
        <translation>&amp;Aller à...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="667"/>
        <source>Create Double carriage way</source>
        <translation>Crée une voie à deux bandes</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="675"/>
        <source>Create Roundabout</source>
        <translation>Créer un rond-point</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="699"/>
        <source>Split road into separate (connected) roads</source>
        <translation>Séparer la route en deux portions (collées)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="714"/>
        <source>Join connected roads to a single road</source>
        <translation>Joindre les routes connectées en une seule</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="729"/>
        <source>Break</source>
        <translation>Séparer</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="732"/>
        <source>Break apart connected roads</source>
        <translation>Séparer des routes connectées</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="743"/>
        <source>Create Relation</source>
        <translation>Crée une relation</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="758"/>
        <source>Create new area</source>
        <translation>Créer une nouvelle zone</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="763"/>
        <source>&amp;Edit...</source>
        <translation>&amp;Modifier...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="768"/>
        <location filename="../src/MainWindow.ui" line="1381"/>
        <source>&amp;Save...</source>
        <translation>&amp;Sauvegarder...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="773"/>
        <location filename="../src/MainWindow.ui" line="1371"/>
        <source>&amp;Load...</source>
        <translation>&amp;Charger...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="815"/>
        <source>Find</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="852"/>
        <source>Node Merge</source>
        <translation>Fusionner des Nœuds</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="875"/>
        <source>Save to file</source>
        <translation>Enregistrer dans un fichier</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="890"/>
        <source>Download more map data for the current area</source>
        <translation>Télécharger des données cartographiques supplémentaires pour la zone actuelle</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="912"/>
        <source>&amp;Properties</source>
        <translation>&amp;Propriétés</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="946"/>
        <source>&amp;Info</source>
        <translation>&amp;Info</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="967"/>
        <source>Align nodes</source>
        <translation>Aligner les nœuds</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="982"/>
        <source>&amp;Spread</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="985"/>
        <source>Spread nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="988"/>
        <source>Align and spread selected nodes equally.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1025"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copier</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1028"/>
        <source>Copy selected features and tags to the clipboard</source>
        <translation>Copie les éléments sélectionnés vers le presse-papier</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1039"/>
        <source>Paste Tags (&amp;Overwrite)</source>
        <translation>Coller Étiquettes (&amp;Écraser)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1054"/>
        <source>Paste Tags (&amp;Merge)</source>
        <translation>Coller Étiquettes (&amp;Fusionner)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1057"/>
        <source>Paste tags</source>
        <translation>Coller Étiquettes</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1060"/>
        <source>Paste tags from the clipboard (Merge with existing tags)</source>
        <translation>Coller les tags du presse-papiers (Fusion avec les tags existants)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1088"/>
        <source>Paste</source>
        <translation>Coller</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1091"/>
        <source>Paste features from the clipboard</source>
        <translation>Coller les caractéristique depuis le presse-papiers</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1118"/>
        <source>Commit feature to the dirty layer</source>
        <translation>Appliquer l&apos;élément à la couche &quot;Modifications&quot;</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1174"/>
        <source>&amp;Raster/SVG</source>
        <translation>Images mat&amp;ricielles/SVG</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1182"/>
        <source>Show &amp;nodes</source>
        <translation>Afficher les &amp;noeuds</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1193"/>
        <source>Show na&amp;mes</source>
        <translation>Afficher les no&amp;ms</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1204"/>
        <source>Start GPS</source>
        <translation>Démarrer le GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1212"/>
        <source>Replay GPS</source>
        <translation>Rejouer GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1220"/>
        <source>&amp;GPS</source>
        <translation>&amp;GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1229"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1240"/>
        <source>Stop GPS</source>
        <translation>Arreter le GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1322"/>
        <source>Record GPS</source>
        <translation>Enregistrer le GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1336"/>
        <source>Pause GPS</source>
        <translation>Mettre le GPS en pause</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1344"/>
        <source>G&amp;eoImage</source>
        <translation>Image géoréférencée</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1358"/>
        <source>World OSB manager...</source>
        <translation>Gestionnaire &quot;World OSB&quot;...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1366"/>
        <source>&amp;Shortcut Editor...</source>
        <translation>Editeur de &amp;raccourci</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1376"/>
        <source>&amp;Merge...</source>
        <translation>&amp;Fusionner...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1386"/>
        <source>&amp;Add member</source>
        <translation>&amp;Ajouter un membre</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1391"/>
        <source>&amp;Remove member</source>
        <translation>&amp;Retirer le membre</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1399"/>
        <source>&amp;Never</source>
        <translation>&amp;Jamais</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1407"/>
        <source>for &amp;Oneway roads</source>
        <translation>pour &amp;Routes à sens unique</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1415"/>
        <source>&amp;Always</source>
        <translation>&amp;Toujours</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1424"/>
        <source>&amp;Detach</source>
        <translation>&amp;Détacher</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1427"/>
        <source>Detach node from a road</source>
        <translation>Détacher un nœud d&apos;une route</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1430"/>
        <source>Detach a node from a Road</source>
        <translation>Détacher un nœud d&apos;une route</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1442"/>
        <source>&amp;Work Offline</source>
        <translation>&amp;Travailler déconnecté</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1447"/>
        <source>SVG</source>
        <translation>SVG</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1455"/>
        <source>&amp;Styles</source>
        <translation>&amp;Styles</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1458"/>
        <location filename="../src/MainWindow.ui" line="1461"/>
        <source>Hide/Show the Styles dock</source>
        <translation>Cacher/Afficher l&apos;encart Styles</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1469"/>
        <source>&amp;WMS Servers Editor...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1474"/>
        <source>&amp;TMS Servers Editor...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1553"/>
        <source>Add street &amp;numbers (Karlsruhe scheme)</source>
        <translation>Ajouter des Numéro de Rue (Système &quot;Karlsruhe&quot;)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1562"/>
        <source>&amp;Subdivide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1565"/>
        <source>Subdivide segment equally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1568"/>
        <source>Subdivide a selected way segment (the way and two adjacent nodes) into segments of equal length.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1584"/>
        <source>Show lat/lon &amp;grid</source>
        <translation>Afficher la &amp;grille lat/lon</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1592"/>
        <source>&amp;Lock zoom to tiled background</source>
        <translation>B&amp;loquer le zoom en fonction de l&apos;arrière-plan</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1597"/>
        <source>&amp;Projections Editor...</source>
        <translation>&amp;Editeur de &amp;projection...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1605"/>
        <source>Show &amp;Photos on map</source>
        <translation>Afficher les &amp;Photos sur la carte</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1610"/>
        <source>OsmChange (OSC)</source>
        <translation>OsmChange (OSC)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1615"/>
        <source>Force Delete</source>
        <translation>Suppression forcée</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1620"/>
        <source>Download OpenStreet&amp;Bugs</source>
        <translation>Charger OpenStreet&amp;Bugs</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1625"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1630"/>
        <source>S&amp;implify</source>
        <translation>S&amp;implifier</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1633"/>
        <source>Simplify road(s)</source>
        <translation>Simplifier la(les) route(s)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1636"/>
        <source>Simplify way by removing unnecessary child nodes</source>
        <translation>Simplifier la route en retirant les noeuds fils superflus</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1641"/>
        <source>&amp;Filters Editor...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1649"/>
        <source>&amp;None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1658"/>
        <source>&amp;Split Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1661"/>
        <source>Split area between two nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1664"/>
        <source>Split a selected area between two selected nodes into two separates areas.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1673"/>
        <source>&amp;Terrace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1676"/>
        <source>Terrace area into residences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1679"/>
        <source>Split a selected area into terraced residences.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1684"/>
        <source>Toolbar Editor...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="751"/>
        <source>Remove member &apos;%1&apos; on %2</source>
        <translation>Retire le membre &apos;%1&apos; de %2</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2719"/>
        <source>%1 (int)</source>
        <translation>%1 (int)</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateAreaInteraction.cpp" line="164"/>
        <source>Add a hole.</source>
        <translation>Ajouter un trou.</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateAreaInteraction.cpp" line="165"/>
        <source>Do you want to add a(nother) hole to this area?</source>
        <translation>Voulez-vous ajouter un (nouveau) trou à cette surface ?</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="338"/>
        <source>Cannot load Projections file</source>
        <translation>Impossible de charger le fichier de projections</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="338"/>
        <source>&quot;Projections.xml&quot; could not be opened anywhere. Aborting.</source>
        <translation>&quot;Projection.xml&quot; ne peut être ouvert nulle part. Abandon.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1786"/>
        <source>Create Junction</source>
        <translation>Créer Carrefour</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2882"/>
        <source>Invalid projection</source>
        <translation>Projection invalide</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2882"/>
        <source>Unable to set projection &quot;%1&quot;.</source>
        <translation>Impossible d&apos;appliquer la projection &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1464"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1479"/>
        <source>&amp;Reset Discardable dialogs status</source>
        <translation>&amp;Réinitialiser le status des boîtes de dialogue cachées</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1488"/>
        <source>GPS Menu</source>
        <translation>Menu GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1497"/>
        <source>Camera</source>
        <translation>Appareil photo</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1502"/>
        <source>Create &amp;Junction</source>
        <translation>Créer &amp;Carrefour</translation>
    </message>
    <message>
        <location filename="../src/Features/Way.cpp" line="766"/>
        <source>Delete Children.</source>
        <translation>Supprimer l&apos;enfant.</translation>
    </message>
    <message>
        <location filename="../src/TagModel.cpp" line="131"/>
        <location filename="../src/TagModel.cpp" line="162"/>
        <source>Set Tags on multiple features</source>
        <translation>Appliquer étiquettes sur objets multiples</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateAreaInteraction.cpp" line="36"/>
        <source>Create Area Interaction</source>
        <translation>Création d&apos;une Surface</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateDoubleWayInteraction.cpp" line="44"/>
        <source>Create double way Interaction</source>
        <translation>Création d&apos;une voie double</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateNodeInteraction.cpp" line="31"/>
        <source>Create node Interaction</source>
        <translation>Création d&apos;un Noeud</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreatePolygonInteraction.cpp" line="30"/>
        <source>LEFT-CLICK to start;DRAG to scale;SHIFT-DRAG to rotate;LEFT-CLICK to end</source>
        <translation>CLIC-GAUCHE pour démarrer; GLISSER pour échelonner; SHIFT-GLISSER pour pivoter; CLIC-GAUCHE pour terminer</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreatePolygonInteraction.cpp" line="35"/>
        <source>Create Polygon Interaction</source>
        <translation>Création d&apos;un Polygone</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreatePolygonInteraction.cpp" line="86"/>
        <source>Create Polygon %1</source>
        <translation>Créer polygone %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateRoundaboutInteraction.cpp" line="43"/>
        <source>Create roundabout Interaction</source>
        <translation>Création d&apos;un Rond-Point</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateSingleWayInteraction.cpp" line="44"/>
        <source>Create way Interaction</source>
        <translation>Création d&apos;une Voie</translation>
    </message>
    <message>
        <location filename="../src/Interactions/EditInteraction.cpp" line="63"/>
        <source>Edit Interaction</source>
        <translation>Édition</translation>
    </message>
    <message>
        <location filename="../src/Interactions/MoveNodeInteraction.cpp" line="139"/>
        <source>Move Nodes</source>
        <translation>Déplacer les nœuds</translation>
    </message>
    <message>
        <location filename="../src/Interactions/MoveNodeInteraction.cpp" line="143"/>
        <source>Move Node %1</source>
        <translation>Déplacer nœud %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/MoveNodeInteraction.cpp" line="39"/>
        <source>LEFT-CLICK to select;LEFT-DRAG to move</source>
        <translation>CLIC-GAUCHE pour sélectionner; GLISSER-GAUCHE pour déplacer</translation>
    </message>
    <message>
        <location filename="../src/Interactions/MoveNodeInteraction.cpp" line="44"/>
        <source>Move node Interaction</source>
        <translation>Déplacement de Noeud</translation>
    </message>
    <message>
        <location filename="../src/Interactions/RotateInteraction.cpp" line="38"/>
        <source>HOVER to select;LEFT-DRAG to rotate/scale</source>
        <translation>SURVOLER pour sélectionner; GLISSER-GAUCHE pour pivoter/échelonner</translation>
    </message>
    <message>
        <location filename="../src/Interactions/RotateInteraction.cpp" line="43"/>
        <source>Rotate Interaction</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <location filename="../src/Interactions/RotateInteraction.cpp" line="127"/>
        <source>Scale/Rotate Nodes</source>
        <translation>Échelonner/Pivoter les nœuds</translation>
    </message>
    <message>
        <location filename="../src/Interactions/ZoomInteraction.cpp" line="23"/>
        <source>LEFT-CLICK to first corner -&gt; LEFT-DRAG to specify area -&gt; LEFT-CLICK to zoom</source>
        <translation>CLIC-GAUCHE pour le premier coin -&gt; GLISSER-GAUCHE pour spécifier la surface -&gt; CLIC-GAUCHE pour zoomer</translation>
    </message>
    <message>
        <location filename="../src/Interactions/ZoomInteraction.cpp" line="26"/>
        <source>Zoom Interaction</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1626"/>
        <source>Create Polygon</source>
        <translation>Créer polygone</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1626"/>
        <source>Specify the number of sides</source>
        <translation>Précisez le nombre de côtés</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1744"/>
        <source>Force Feature upload</source>
        <translation>Forcer l&apos;envoi de l&apos;objet</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1115"/>
        <source>&amp;Force Upload</source>
        <translation>&amp;Forcer l&apos;envoi</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1514"/>
        <source>Rotate</source>
        <translation>Pivoter</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1517"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1522"/>
        <source>Polygon</source>
        <translation>Polygone</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1527"/>
        <source>Rectangle</source>
        <translation>Rectangle</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1532"/>
        <source>Add new Image layer</source>
        <translation>Ajouter un nouveau calque d&apos;image</translation>
    </message>
    <message>
        <location filename="../src/Interactions/EditInteraction.cpp" line="56"/>
        <source>LEFT-CLICK to select;RIGHT-CLICK to pan;CTRL-LEFT-CLICK to toggle selection;SHIFT-LEFT-CLICK to add to selection;LEFT-DRAG for area selection;CTRL-RIGHT-DRAG for zoom;</source>
        <translation>CLIC-GAUCHE pour sélectionner; CLIC-DROIT pour dérouler; CTRL-CLIC-GAUCHE pour basculer la sélection; SHIFT-CLIC-GAUCHE pour ajouter à la sélection; GLISSER-GAUCHE pour sélectionner par surface; CTRL-GLISSER-DROIT pour zoomer;</translation>
    </message>
    <message>
        <location filename="../src/Interactions/EditInteraction.cpp" line="58"/>
        <source>CLICK to select/move;CTRL-CLICK to toggle selection;SHIFT-CLICK to add to selection;SHIFT-DRAG for area selection;CTRL-DRAG for zoom;</source>
        <translation>CLIC pour sélectionner/déplacer; CTRL-CLIC pour basculer la sélection; SHIFT-CLIC pour ajouter à la sélection; SHIFT-GLISSER pour sélectionner par surface, CTRL-GLISSER pour zoomer</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="694"/>
        <source>Clipboard do not contain valid data.</source>
        <translation>Le presse-papier ne contient pas de données valide.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1790"/>
        <source>Multiple intersection.</source>
        <translation>Intersections Multiples</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1791"/>
        <source>Those roads have multiple intersections.
Do you still want to create a junction for each one (Unwanted junctions can still be deleted afterhand)?</source>
        <translation>Ces routes ont de multiples intersections.
Voulez-vous créer un carrefour pour chacune d&apos;elles? (Les carrefours non désirées peuvent toujours être supprimées par après)?</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1537"/>
        <source>Paste Feature(s)</source>
        <translation>Coller l(es) objet(s)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1548"/>
        <source>Features</source>
        <translation>Objets</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="3254"/>
        <source>Warning! Could not load the Merkaartor translations for the &quot;%1&quot; language. Switching to default english.</source>
        <translation>Warning! Could not load the Merkaartor translations for the &quot;%1&quot; language. Switching to default english.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1576"/>
        <source>Show &amp;virtual nodes</source>
        <translation>Afficher les nœuds &amp;virtuels</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="711"/>
        <source>Clear %1 tags on %2</source>
        <translation>Supprimer les étiquettes %1 de %2</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="828"/>
        <location filename="../src/Docks/PropertiesDock.cpp" line="873"/>
        <source>Reorder members in relation %1</source>
        <translation>Réordonner les membres dans la relation %1</translation>
    </message>
</context>
<context>
    <name>MapFeature</name>
    <message>
        <location filename="../src/Features/Feature.cpp" line="913"/>
        <source>&lt;i&gt;V: &lt;/i&gt;&lt;b&gt;%1&lt;/b&gt; </source>
        <translation>&lt;i&gt;V: &lt;/i&gt;&lt;b&gt;%1&lt;/b&amp;gt; </translation>
    </message>
    <message>
        <location filename="../src/Features/Feature.cpp" line="915"/>
        <source>&lt;i&gt;last: &lt;/i&gt;&lt;b&gt;%1&lt;/b&gt; by &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>&lt;i&gt;dernier: &lt;/i&gt;&lt;b&gt;%1&lt;/b&gt; par &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/Features/Feature.cpp" line="917"/>
        <source>&lt;i&gt;last: &lt;/i&gt;&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>&lt;i&gt;dernier: &lt;/i&gt;&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/Features/Feature.cpp" line="920"/>
        <source>&lt;br/&gt;&lt;i&gt;layer: &lt;/i&gt;&lt;b&gt;%1&lt;/b&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Features/Feature.cpp" line="927"/>
        <source>History</source>
        <translation>Historique</translation>
    </message>
    <message>
        <location filename="../src/Features/Feature.cpp" line="930"/>
        <source>Referenced by ways</source>
        <translation>Référencé par les routes</translation>
    </message>
    <message>
        <location filename="../src/Features/Feature.cpp" line="933"/>
        <source>Referenced by relation</source>
        <translation>Référencé par la relation</translation>
    </message>
    <message>
        <location filename="../src/Features/Relation.cpp" line="570"/>
        <source>size</source>
        <translation>taille</translation>
    </message>
    <message>
        <location filename="../src/Features/Relation.cpp" line="570"/>
        <source>members</source>
        <translation>membres</translation>
    </message>
    <message>
        <location filename="../src/Features/Relation.cpp" line="573"/>
        <location filename="../src/Features/Way.cpp" line="1060"/>
        <source>Topleft</source>
        <translation>En haut à gauche</translation>
    </message>
    <message>
        <location filename="../src/Features/Relation.cpp" line="575"/>
        <location filename="../src/Features/Way.cpp" line="1062"/>
        <source>Botright</source>
        <translation>BasDroit</translation>
    </message>
    <message>
        <location filename="../src/Features/Relation.cpp" line="577"/>
        <source>Relation</source>
        <translation>Relation</translation>
    </message>
    <message>
        <location filename="../src/Features/Relation.cpp" line="732"/>
        <source>Role</source>
        <translation>Role</translation>
    </message>
    <message>
        <location filename="../src/Features/Relation.cpp" line="734"/>
        <source>Member</source>
        <translation>Membre</translation>
    </message>
    <message>
        <location filename="../src/Features/Way.cpp" line="1064"/>
        <source>Way</source>
        <translation>Route</translation>
    </message>
    <message>
        <location filename="../src/Features/Node.cpp" line="608"/>
        <source>coord</source>
        <translation>coordonnées</translation>
    </message>
    <message>
        <location filename="../src/Features/Node.cpp" line="629"/>
        <source>Node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="../src/Features/Way.cpp" line="1057"/>
        <source>%1 nodes</source>
        <translation>%1 nœuds</translation>
    </message>
    <message>
        <location filename="../src/Features/Way.cpp" line="1055"/>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <location filename="../src/Features/Way.cpp" line="1057"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../src/Features/Way.cpp" line="1064"/>
        <source>Area</source>
        <translation>Aire</translation>
    </message>
    <message>
        <location filename="../src/Features/Node.cpp" line="611"/>
        <source>elevation</source>
        <translation>élévation</translation>
    </message>
    <message>
        <location filename="../src/Features/Node.cpp" line="613"/>
        <source>speed</source>
        <translation>vitesse</translation>
    </message>
    <message>
        <location filename="../src/Features/Node.cpp" line="620"/>
        <source>Waypoint</source>
        <translation>Noeud intermédiaire</translation>
    </message>
    <message>
        <location filename="../src/Features/Node.cpp" line="615"/>
        <location filename="../src/Features/Node.cpp" line="623"/>
        <source>description</source>
        <translation>description</translation>
    </message>
    <message>
        <location filename="../src/Features/Node.cpp" line="617"/>
        <location filename="../src/Features/Node.cpp" line="626"/>
        <source>comment</source>
        <translation>commentaire</translation>
    </message>
</context>
<context>
    <name>MapView</name>
    <message>
        <location filename="../src/MapView.cpp" line="279"/>
        <source>%1 m</source>
        <translation>%1 m</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="281"/>
        <source>%1 km</source>
        <translation>%1 km</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="678"/>
        <source>Feature</source>
        <translation>Objet</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="695"/>
        <source>Road</source>
        <translation>Route</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="812"/>
        <source>Load image</source>
        <translation>Charger une image</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="814"/>
        <source>Add node position to image</source>
        <translation>Ajouter la postion du noeud à l&apos;image</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="816"/>
        <source>Geotag image with this position</source>
        <translation>Géo-éttiquette l&apos;image avec cette position</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="687"/>
        <source>Node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="703"/>
        <source>Relation</source>
        <translation>Relation</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="818"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="250"/>
        <source>%1ms;ppm:%2</source>
        <translation>%1ms;ppm:%2</translation>
    </message>
</context>
<context>
    <name>MerkaartorPreferences</name>
    <message>
        <location filename="../src/Preferences/TmsServersList.cpp" line="20"/>
        <location filename="../src/Preferences/TmsServersList.cpp" line="36"/>
        <location filename="../src/Preferences/WmsServersList.cpp" line="20"/>
        <location filename="../src/Preferences/WmsServersList.cpp" line="43"/>
        <source>New Server</source>
        <translation>Nouveau serveur</translation>
    </message>
    <message>
        <location filename="../src/Preferences/MerkaartorPreferences.cpp" line="403"/>
        <location filename="../src/Preferences/MerkaartorPreferences.cpp" line="406"/>
        <source>Preferences upload failed</source>
        <translation>Echec du chargement des préférences</translation>
    </message>
    <message>
        <location filename="../src/Preferences/MerkaartorPreferences.cpp" line="403"/>
        <source>Duplicate key</source>
        <translation>Dupliquer la clé</translation>
    </message>
    <message>
        <location filename="../src/Preferences/MerkaartorPreferences.cpp" line="406"/>
        <source>More than 150 preferences</source>
        <translation>Plus de 150 préférences</translation>
    </message>
    <message>
        <location filename="../src/Preferences/BookmarksList.cpp" line="20"/>
        <location filename="../src/Preferences/BookmarksList.cpp" line="27"/>
        <source>New Bookmark</source>
        <translation>Nouveau signet</translation>
    </message>
</context>
<context>
    <name>MultiProperties</name>
    <message>
        <location filename="../src/MultiProperties.ui" line="13"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../src/MultiProperties.ui" line="33"/>
        <source>Tags</source>
        <translation>Etiquettes</translation>
    </message>
    <message>
        <location filename="../src/MultiProperties.ui" line="53"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/MultiProperties.ui" line="73"/>
        <source>Selected items</source>
        <translation>Eléments sélectionnés</translation>
    </message>
</context>
<context>
    <name>NameFinder::NameFinderTableModel</name>
    <message>
        <location filename="../src/NameFinder/namefindertablemodel.cpp" line="68"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/NameFinder/namefindertablemodel.cpp" line="71"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../src/NameFinder/namefindertablemodel.cpp" line="74"/>
        <source>Near</source>
        <translation>A proximité de</translation>
    </message>
    <message>
        <location filename="../src/NameFinder/namefindertablemodel.cpp" line="77"/>
        <source>Unknown field</source>
        <translation>Champ inconnu</translation>
    </message>
</context>
<context>
    <name>NameFinder::NameFinderWidget</name>
    <message>
        <location filename="../src/NameFinder/namefinderwidget.cpp" line="89"/>
        <source>Error!</source>
        <translation>Erreur!</translation>
    </message>
    <message>
        <location filename="../src/NameFinder/namefinderwidget.cpp" line="93"/>
        <source>Name finder service host not found.</source>
        <translation>Le service de recherche de nom est injoignable.</translation>
    </message>
    <message>
        <location filename="../src/NameFinder/namefinderwidget.cpp" line="96"/>
        <source>Name finder service host refused connection.</source>
        <translation>Le service de recherche de nom à refusé la connexion.</translation>
    </message>
    <message>
        <location filename="../src/NameFinder/namefinderwidget.cpp" line="99"/>
        <source>Name finder service requires authentication.</source>
        <translation>Le service de recherche de nom demande une authentification</translation>
    </message>
    <message>
        <location filename="../src/NameFinder/namefinderwidget.cpp" line="101"/>
        <source>Unknown error.</source>
        <translation>Erreur inconnue.</translation>
    </message>
</context>
<context>
    <name>NameFinderWidgetUi</name>
    <message>
        <location filename="../src/NameFinder/namefinderwidget.ui" line="13"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
</context>
<context>
    <name>NativeRenderDialog</name>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="14"/>
        <source>Raster/SVG export</source>
        <translation>Export Images matricielles/SVG</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="20"/>
        <source>Export type</source>
        <translation>Type d&apos;export</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="33"/>
        <source>Raster</source>
        <translation>Images matricielles</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="43"/>
        <source>Export options</source>
        <translation>Options d’export</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="67"/>
        <source>min lat/Lon</source>
        <translation>Lat/Lon min.</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="134"/>
        <source>max lat/Lon</source>
        <translation>Lat/Lon max.</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="201"/>
        <source>Show Scale</source>
        <translation>Afficher l&apos;échelle</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="214"/>
        <source>Show Grid</source>
        <translation>Afficher la grille</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="227"/>
        <source>Show Borders</source>
        <translation>Afficher les bordures</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="240"/>
        <source>Show License</source>
        <translation>Afficher la licence</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="256"/>
        <source>Width x Height (px)</source>
        <translation>Largeur x Hauteur (px)</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="279"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.cpp" line="36"/>
        <source>Proceed...</source>
        <translation>Poursuivre...</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.cpp" line="133"/>
        <source>Working. Please Wait...</source>
        <translation>Travail en cours. Patientez s&apos;il vous plaît....</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.cpp" line="133"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.cpp" line="159"/>
        <source>SVG rendering</source>
        <translation>Rendu SVG</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.cpp" line="162"/>
        <source>Raster rendering</source>
        <translation>Rendu en images matricielles</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="26"/>
        <source>SVG</source>
        <translation>SVG</translation>
    </message>
</context>
<context>
    <name>OsbLayer</name>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="1496"/>
        <source># of loaded Regions</source>
        <translation>Nombre de régions chargées</translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="1496"/>
        <location filename="../src/Layers/Layer.cpp" line="1497"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="1497"/>
        <source># of loaded Tiles</source>
        <translation>Nombre de Tuiles chargées</translation>
    </message>
</context>
<context>
    <name>OsbLayerWidget</name>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="552"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>PaintStyleEditor</name>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="45"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="46"/>
        <source>Always</source>
        <translation>Toujours</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="235"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="262"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="306"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="374"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="442"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="532"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="573"/>
        <source>Select Color</source>
        <translation>Choisir une couleur</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="13"/>
        <source>Map style editor</source>
        <translation>Editeur de Style de carte</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="86"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="93"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="177"/>
        <source>Tag selection</source>
        <translation>Sélection d&apos;étiquette</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="197"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="253"/>
        <source>meter/pixel</source>
        <translation>mètre/pixel</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="223"/>
        <source>Visible up to</source>
        <translation>Visible jusqu&apos;à</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="230"/>
        <source>Visible from scale</source>
        <translation>Visible à partie de l&apos;échelle</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="49"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="129"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="145"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="326"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="521"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="595"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="748"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1074"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1319"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="372"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="641"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="794"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="967"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1168"/>
        <source>Proportional thickness</source>
        <translation>Epaisseur proportionnelle</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="392"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="661"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="814"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="987"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1188"/>
        <source>Fixed thickness</source>
        <translation>Epaisseur fixe</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="454"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="876"/>
        <source>Dashed</source>
        <translation>Pointillé</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="468"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="890"/>
        <source>off</source>
        <translation>désactivé</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="505"/>
        <source>Fill area</source>
        <translation>Remplir la surface</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="927"/>
        <source>Draw icon</source>
        <translation>Dessiner l&apos;icône</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="100"/>
        <source>Duplicate</source>
        <translation>Dupliquer</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="33"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="296"/>
        <source>Background</source>
        <translation>Arrière-plan</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="310"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="579"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1058"/>
        <source>Draw with color</source>
        <translation>Dessiner avec la couleur</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="565"/>
        <source>Boundary</source>
        <translation>Bordure</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="718"/>
        <source>Touchup</source>
        <translation>Retoucher</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="732"/>
        <source>Draw steps</source>
        <translation>Dessiner les marches</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1044"/>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1081"/>
        <source>Font</source>
        <translation>Police de caractère</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1128"/>
        <source>Label tag</source>
        <translation>Marquage de l&apos;étiquette</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1303"/>
        <source>Draw with background color</source>
        <translation>Dessiner avec la couleur d&apos;arrière-plan</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1359"/>
        <source>Label with background tag</source>
        <translation>Label avec l&apos;étiquette de fond</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="19"/>
        <source>Global</source>
        <translation>Global</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1244"/>
        <source>Halo</source>
        <translation>Auréole</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1254"/>
        <source>Area</source>
        <translation>Aire</translation>
    </message>
</context>
<context>
    <name>PhotoLoadErrorDialog</name>
    <message>
        <location filename="../src/Docks/PhotoLoadErrorDialog.ui" line="14"/>
        <source>No Valid geotag</source>
        <translation>Pas de géomarque valide</translation>
    </message>
    <message>
        <location filename="../src/Docks/PhotoLoadErrorDialog.ui" line="20"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../src/Docks/PhotoLoadErrorDialog.ui" line="27"/>
        <source>This photo do not contain geo-tagging EXIF data.
What do you want to do next?</source>
        <translation>Cette photo ne contient pas de données de géomarquage EXIF.
Que désirez-vous faire ensuite?</translation>
    </message>
    <message>
        <location filename="../src/Docks/PhotoLoadErrorDialog.ui" line="44"/>
        <source>Try to match with a track node</source>
        <translation>Essayer de rapprocher d&apos;un point de trace</translation>
    </message>
    <message>
        <location filename="../src/Docks/PhotoLoadErrorDialog.ui" line="63"/>
        <source>Load without associated node</source>
        <translation>Charger sans noeud associé</translation>
    </message>
    <message>
        <location filename="../src/Docks/PhotoLoadErrorDialog.ui" line="73"/>
        <source>Extract info from barcode (Walking Papers)</source>
        <translation>Extraction des infos depuis le code barre (Walking Papers)</translation>
    </message>
    <message>
        <location filename="../src/Docks/PhotoLoadErrorDialog.ui" line="89"/>
        <source>Ignore it</source>
        <translation>L&apos;ignorer</translation>
    </message>
    <message>
        <location filename="../src/Docks/PhotoLoadErrorDialog.ui" line="99"/>
        <source>Do this for all current photos.</source>
        <translation>Effectuer cela pour toutes les photos courantes</translation>
    </message>
</context>
<context>
    <name>PictureViewerDialog</name>
    <message>
        <location filename="../src/Utils/PictureViewerDialog.cpp" line="39"/>
        <location filename="../src/Utils/PictureViewerDialog.cpp" line="45"/>
        <source>Output filename</source>
        <translation>Nom du fichier de sortie</translation>
    </message>
    <message>
        <location filename="../src/Utils/PictureViewerDialog.cpp" line="45"/>
        <source>Image files (*.png *.jpg)</source>
        <translation>Fichiers images (*.png *.jpg)</translation>
    </message>
    <message>
        <location filename="../src/Utils/PictureViewerDialog.ui" line="13"/>
        <source>Dialog</source>
        <translation>Boîte de dialogue</translation>
    </message>
    <message>
        <location filename="../src/Utils/PictureViewerDialog.cpp" line="39"/>
        <source>SVG files (*.svg)</source>
        <translation>Fichiers SVG (*.svg)</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="15"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="28"/>
        <source>Visual</source>
        <translation>Visuel</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="34"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="42"/>
        <source>Zoom Out/in (%)</source>
        <translation>Zoom arrière/avant (%)</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="66"/>
        <source>Opacity low/high</source>
        <translation>Opacité bas/haut</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="582"/>
        <source>Map style</source>
        <translation>Style de carte</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="708"/>
        <source>Custom</source>
        <translation>Personnalisé</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="769"/>
        <source>OSM</source>
        <translation>OSM</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="777"/>
        <source>Website:</source>
        <translation>Site internet:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="787"/>
        <source>Username:</source>
        <translation>Nom d&apos;utilisateur:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="797"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1079"/>
        <source>Password:</source>
        <translation>Mot-de-passe:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1060"/>
        <source>Network</source>
        <translation>Réseau</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1066"/>
        <source>Proxy settings</source>
        <translation>Configuration du Proxy</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1072"/>
        <source>Use Proxy</source>
        <translation>Utiliser un serveur mandataire</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1116"/>
        <source>Host:</source>
        <translation>Nom d&apos;hôte:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1109"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1186"/>
        <source>Background Image</source>
        <translation>Image d&apos;arrière-plan</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1198"/>
        <source>Cache directory</source>
        <translation>Répertoire du cache</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1208"/>
        <source>Cache size (in Mb; 0 to disable)</source>
        <translation>Taille du cache (en Mb; 0 pour désactiver)</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1227"/>
        <source>Map Adapter</source>
        <translation>Adapteur de carte</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="757"/>
        <source>Data</source>
        <translation>Données</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="816"/>
        <source>Documents</source>
        <translation>Documents</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="822"/>
        <source>Autosave documents after upload</source>
        <translation>Sauvegarder automatiquement les documents après téléversage</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="684"/>
        <source>Built-in</source>
        <translation>Inclus</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="646"/>
        <source>Disable styles for track layers</source>
        <translation>Désactiver les styles pour les couches de piste</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="832"/>
        <source>Tracks</source>
        <translation>Pistes</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="840"/>
        <source>Automatically extract tracks on open</source>
        <translation>Extraire automatiquement les pistes à l&apos;ouverture</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="396"/>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="409"/>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="422"/>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="435"/>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="448"/>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="460"/>
        <source>Select Color</source>
        <translation>Choisir une couleur</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="475"/>
        <source>Tool already exists</source>
        <translation>L&apos;outil existe déjà</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="476"/>
        <source>A tool of this name already exists.
Please select another name or click the &lt;Apply&gt; button if you want to modify the existing one</source>
        <translation>Un outil portant ce nom existe déjà.
Veuillez choisir un autre nom ou cliquer sur le boutton &lt;Appliquer&gt; si vous désirer modifier celui existant</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="495"/>
        <source>Cannot delete preset tool</source>
        <translation>Impossible de supprimer l&apos;outil prédéfini</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="496"/>
        <source>Cannot delete preset tool &quot;%1&quot;</source>
        <translation>Impossible de supprimer l&apos;outil prédéfini &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="514"/>
        <source>Cannot modify preset tool name</source>
        <translation>Impossible de modifier le nom d&apos;un outil prédéfini</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="515"/>
        <source>Cannot modify preset tool &quot;%1&quot;&apos;s name</source>
        <translation>Impossible de modifier le nom de l&apos;outil prédéfini &quot;%1&quot;&apos;</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="540"/>
        <source>Select tool executable</source>
        <translation>Séléctionnez l&apos;exécutable de l&apos;outil</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="548"/>
        <source>Select Log directory</source>
        <translation>Séléctionnez le dossier des fichiers de journalisation</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="505"/>
        <source>Locale</source>
        <translation>Locale</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="511"/>
        <source>You may need to restart the program for these changes to take effect</source>
        <translation>Il sera peut être nécéssaire de redémarrer le programme pour que ces changements prennent effet</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="520"/>
        <source>Use language</source>
        <translation>Utiliser la langue</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="555"/>
        <source>Translate standard tags</source>
        <translation>Traduire les marquages standards</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="158"/>
        <source>Colors</source>
        <translation>Couleurs</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="437"/>
        <source>Background</source>
        <translation>Arrière-plan</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="360"/>
        <source>Hover</source>
        <translation>Survoler</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="420"/>
        <source>Focus</source>
        <translation>Focalisation</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="413"/>
        <source>Relations</source>
        <translation>Relations</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="175"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="230"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="276"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="322"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="375"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="459"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="613"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="731"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1021"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1317"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="576"/>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="891"/>
        <source>GPS</source>
        <translation>GPS</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="998"/>
        <source>Save NMEA log</source>
        <translation>Sauver les fichiers de journalisation NMEA</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1039"/>
        <source>Set system time to GPS</source>
        <translation>Mettre l&apos;horloge système à celle du GPS</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1165"/>
        <source>Enable JOSM-compatible local server on port 8111</source>
        <translation>Activer le serveur local compatible JOSM sur le port 8111</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1262"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1280"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1290"/>
        <source>Path:</source>
        <translation>Chemin:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1339"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1346"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1353"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="378"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="590"/>
        <source>Custom styles directory</source>
        <translation>Répertoire des styles personnalisés</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="387"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="676"/>
        <source>Tag Template</source>
        <translation>Modèkle de tag</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="387"/>
        <source>Merkaartor tag template (*.mat)</source>
        <translation>Modèle de tag Merkaartor (*.mat)</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="97"/>
        <source>Separate Move mode</source>
        <translation>Mode de déplacement séparé</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="104"/>
        <source>Single mouse button interaction</source>
        <translation>Interaction avec un seul bouton de souris</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="111"/>
        <source>Use custom Qt style</source>
        <translation>Utiliser un style Qt personnalisé</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="188"/>
        <source>Overwrite style</source>
        <translation>Ecraser le style</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="246"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="292"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="338"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="391"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="475"/>
        <source>Pixels</source>
        <translation>Pixels</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="624"/>
        <source>Current style</source>
        <translation>Style actuel</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="670"/>
        <source>Template</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="847"/>
        <source>Track layers readonly by default</source>
        <translation>Couches de traces en lecture seule par défaut</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="858"/>
        <source>Don&apos;t connect GPX nodes separated by more than (in km; 0 to disable)</source>
        <translation>Ne pas connecter les nœuds du GPX séparés par plus de (en km; 0 pour désactiver)</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1192"/>
        <source>Tiles Caching (not active for Yahoo! due to legal restrictions)</source>
        <translation>Mise en cache des tuiles (Inactif pour Yahoo! pour des raisons légales)</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="108"/>
        <source>English</source>
        <translation>Anglais</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="109"/>
        <source>Arabic</source>
        <translation>Arabe</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="110"/>
        <source>Czech</source>
        <translation>Tchèque</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="112"/>
        <source>German</source>
        <translation>Allemand</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="113"/>
        <source>French</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="114"/>
        <source>Italian</source>
        <translation>Italien</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="116"/>
        <source>Polish</source>
        <translation>Polonais</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="118"/>
        <source>Brazilian Portuguese</source>
        <translation>Portugais Brésilien</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="119"/>
        <source>Russian</source>
        <translation>Russe</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1102"/>
        <source>User:</source>
        <translation>Utilisateur:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1233"/>
        <source>Enable use of OSB background (needs new document)</source>
        <translation>Activer l&apos;utilisation de l&apos;arrière-plan OSB (nécessite un nouveau document)</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="427"/>
        <source>Highlight</source>
        <translation>Surligner</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="444"/>
        <source>GPX track</source>
        <translation>Trace GPX</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="482"/>
        <source>Use simple GPX track appearance</source>
        <translation>Utiliser l&apos;apparence simplifiée pour les traces GPX</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="897"/>
        <source>GPS input</source>
        <translation>Entrée GPS</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="903"/>
        <source>gpsd</source>
        <translation>gpsd</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="910"/>
        <source>Serial</source>
        <translation>Série</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="932"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="970"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="960"/>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="125"/>
        <source>Use Virtual nodes (new session required)</source>
        <translation>Utiliser les nœuds virtuels (nouvelle session nécessaire)</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="111"/>
        <source>Dutch</source>
        <translation>Néérlandais</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="115"/>
        <source>Japanase</source>
        <translation>Japonais</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="117"/>
        <source>Portuguese</source>
        <translation>Portuguais</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="120"/>
        <source>Slovak</source>
        <translation>Slovaque</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="121"/>
        <source>Spanish</source>
        <translation>Espagnol</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="122"/>
        <source>Swedish</source>
        <translation>Suédois</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="123"/>
        <source>Ukrainian</source>
        <translation>Ukrainien</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="132"/>
        <source>Relations selectable while hidden</source>
        <translation>Relations sélectionnables quand cachées</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="216"/>
        <source>Interface</source>
        <translation>Interface</translation>
    </message>
</context>
<context>
    <name>ProjPreferencesDialog</name>
    <message>
        <location filename="../src/Preferences/ProjPreferencesDialog.ui" line="14"/>
        <source>Projections setup</source>
        <translation>Configuration des projections</translation>
    </message>
    <message>
        <location filename="../src/Preferences/ProjPreferencesDialog.ui" line="20"/>
        <source>Projections</source>
        <translation>Projections</translation>
    </message>
    <message>
        <location filename="../src/Preferences/ProjPreferencesDialog.ui" line="47"/>
        <source>Projection list:</source>
        <translation>Liste des projections :</translation>
    </message>
    <message>
        <location filename="../src/Preferences/ProjPreferencesDialog.ui" line="76"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/ProjPreferencesDialog.ui" line="93"/>
        <source>PROJ4 string</source>
        <translation>Chaîne PROJ4</translation>
    </message>
    <message>
        <location filename="../src/Preferences/ProjPreferencesDialog.ui" line="116"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../src/Preferences/ProjPreferencesDialog.ui" line="123"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../src/Preferences/ProjPreferencesDialog.ui" line="130"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>PropertiesDock</name>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="490"/>
        <location filename="../src/Docks/PropertiesDock.cpp" line="1132"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="428"/>
        <source>Properties - Multiple elements</source>
        <translation>Propriétés - Eléments multiples</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="478"/>
        <source>Properties - Relation</source>
        <translation>Propriétés - Relation</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="458"/>
        <source>Properties - Road</source>
        <translation>Propriétés - Route</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="1133"/>
        <source>Center map</source>
        <translation>Centrer la carte</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="1134"/>
        <source>Center &amp;&amp; Zoom map</source>
        <translation>Centrer &amp;&amp; Zoomer la carte</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="333"/>
        <source>%1/%1 selected item(s)</source>
        <translation>%1/%1 élément(s) sélectionné(s)</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="356"/>
        <source>%1/%2 selected item(s)</source>
        <translation>%1/%2 élément(s) sélectionné(s)</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="419"/>
        <source>Selected items</source>
        <translation>Eléments sélectionnés</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="1135"/>
        <source>Select member</source>
        <translation>Selectionner le membre</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="444"/>
        <source>Properties - Node</source>
        <translation>Propriétés - Nœuds</translation>
    </message>
</context>
<context>
    <name>QGPS</name>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="93"/>
        <location filename="../src/GPS/qgps.cpp" line="165"/>
        <location filename="../src/GPS/qgps.cpp" line="222"/>
        <source>No Position Fix</source>
        <translation>Pas d&apos;acquisition de position</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="94"/>
        <location filename="../src/GPS/qgps.cpp" line="170"/>
        <location filename="../src/GPS/qgps.cpp" line="223"/>
        <source>No UTC Time</source>
        <translation>Pas d&apos;heure UTC</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="91"/>
        <location filename="../src/GPS/qgps.cpp" line="150"/>
        <source>Invalid</source>
        <translation>Non valide</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="135"/>
        <source>Meters</source>
        <translation>Mètres</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="140"/>
        <source>km/h</source>
        <translation>km/h</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="153"/>
        <source>2D</source>
        <translation>2D</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="156"/>
        <source>3D</source>
        <translation>3D</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="162"/>
        <source>Position Fix available</source>
        <translation>L&apos;acquisition de la position est disponible</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="221"/>
        <source>GPS</source>
        <translation>GPS</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="147"/>
        <source>Unavailable</source>
        <translation>Non disponible</translation>
    </message>
</context>
<context>
    <name>QGPSComDevice</name>
    <message>
        <location filename="../src/GPS/qgpsdevice.cpp" line="684"/>
        <source>GPS log error</source>
        <translation>Erreur de journalisation GPS</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsdevice.cpp" line="685"/>
        <source>Unable to create GPS log file: %1.</source>
        <translation>Impossible de créer le fichier de journalisation GPS: %1.</translation>
    </message>
</context>
<context>
    <name>QGPSDDevice</name>
    <message>
        <location filename="../src/GPS/qgpsdevice.cpp" line="962"/>
        <source>Unable to connect to %1:%2</source>
        <translation>Impossible de se connecter à %1:%2</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsdevice.cpp" line="1062"/>
        <source>GPS log error</source>
        <translation>Erreur de journalisation GPS</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsdevice.cpp" line="1063"/>
        <source>Unable to create GPS log file: %1.</source>
        <translation>Impossible de créer le fichier de journalisation GPS: %1.</translation>
    </message>
</context>
<context>
    <name>QGPSMainWindowUI</name>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="14"/>
        <source>GPS</source>
        <translation>GPS</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="22"/>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="29"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="63"/>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="80"/>
        <source>99 99&apos; 99&quot;</source>
        <translation>99 99&apos; 99&quot;</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="70"/>
        <source>Longitude</source>
        <translation>Longitude</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="87"/>
        <source>Altitude</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="97"/>
        <source>1000 meter</source>
        <translation>1000 mètres</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="104"/>
        <source># Satellites</source>
        <translation>Nombre de satellites</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="114"/>
        <source>00</source>
        <translation>00</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="121"/>
        <source>Fix Type</source>
        <translation>Type d&apos;acquisition</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="131"/>
        <source>Invalid</source>
        <translation>Non valide</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="138"/>
        <source>Speed</source>
        <translation>Vitesse</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="148"/>
        <source>000 km/h</source>
        <translation>000 km/h</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="155"/>
        <source>Latitude</source>
        <translation>Latitude</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/NameFinder/xmlstreamreader.cpp" line="41"/>
        <source>Not a proper results stream!</source>
        <translation>Flux de résultats impropre !</translation>
    </message>
</context>
<context>
    <name>QtToolBarDialog</name>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.cpp" line="1798"/>
        <source>&lt; S E P A R A T O R &gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="16"/>
        <source>Customize Toolbars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="31"/>
        <source>Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="46"/>
        <source>Toolbars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="53"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="60"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="67"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="84"/>
        <source>Restore All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="104"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="111"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="118"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="143"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="158"/>
        <source>&lt;-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="173"/>
        <source>-&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="188"/>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="213"/>
        <source>Current Toolbar Actions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtToolBarDialogPrivate</name>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.cpp" line="1247"/>
        <source>Custom Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RelationProperties</name>
    <message>
        <location filename="../src/Docks/MinimumRelationProperties.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumRelationProperties.ui" line="31"/>
        <source>Members</source>
        <translation>Membres</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumRelationProperties.ui" line="119"/>
        <source>Tags</source>
        <translation>Etiquettes</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumRelationProperties.ui" line="51"/>
        <location filename="../src/Docks/MinimumRelationProperties.ui" line="81"/>
        <location filename="../src/Docks/MinimumRelationProperties.ui" line="97"/>
        <location filename="../src/Docks/MinimumRelationProperties.ui" line="139"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>RoadProperties</name>
    <message>
        <location filename="../src/Docks/MinimumRoadProperties.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumRoadProperties.ui" line="34"/>
        <source>xxx</source>
        <translation>xxx</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumRoadProperties.ui" line="41"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumRoadProperties.ui" line="61"/>
        <source>Tags</source>
        <translation>Etiquettes</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumRoadProperties.ui" line="81"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>SatelliteStrengthView</name>
    <message>
        <location filename="../src/GPS/SatelliteStrengthView.cpp" line="50"/>
        <source>No satellites</source>
        <translation>Pas de satelittes</translation>
    </message>
</context>
<context>
    <name>SelectionDialog</name>
    <message>
        <location filename="../src/Utils/SelectionDialog.ui" line="14"/>
        <source>Selection</source>
        <translation>Sélection</translation>
    </message>
    <message>
        <location filename="../src/Utils/SelectionDialog.ui" line="25"/>
        <source>Key</source>
        <translation>Clef</translation>
    </message>
    <message>
        <location filename="../src/Utils/SelectionDialog.ui" line="32"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../src/Utils/SelectionDialog.ui" line="45"/>
        <source>=</source>
        <translation>=</translation>
    </message>
    <message>
        <location filename="../src/Utils/SelectionDialog.ui" line="55"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/Utils/SelectionDialog.ui" line="88"/>
        <source>Query</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Utils/SelectionDialog.ui" line="102"/>
        <source>Maximum returned results</source>
        <translation>Nombre maximum de résultats</translation>
    </message>
    <message>
        <location filename="../src/Utils/SelectionDialog.ui" line="81"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
</context>
<context>
    <name>SlippyMapWidget</name>
    <message>
        <location filename="../src/Utils/SlippyMapWidget.cpp" line="277"/>
        <source>Reset view</source>
        <translation>Réinitialiser la vue</translation>
    </message>
</context>
<context>
    <name>StyleDock</name>
    <message>
        <location filename="../src/Docks/StyleDock.cpp" line="102"/>
        <source>Styles</source>
        <translation>Styles</translation>
    </message>
</context>
<context>
    <name>StyleDockWidget</name>
    <message>
        <location filename="../src/Docks/StyleDock.ui" line="14"/>
        <source>Styles</source>
        <translation>Styles</translation>
    </message>
</context>
<context>
    <name>SyncListDialog</name>
    <message>
        <location filename="../src/Sync/SyncListDialog.ui" line="35"/>
        <source>Please review carefully the changes sent to OSM</source>
        <translation>Veuillez réviser avec précaution les changements envoyés vers OSM</translation>
    </message>
    <message>
        <location filename="../src/Sync/SyncListDialog.ui" line="66"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/Sync/SyncListDialog.ui" line="73"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/Sync/SyncListDialog.ui" line="13"/>
        <source>Upload to Openstreetmap</source>
        <translation>Envoi à Openstreetmap</translation>
    </message>
    <message>
        <location filename="../src/Sync/SyncListDialog.ui" line="25"/>
        <source>Please specify a comment for this changeset.</source>
        <translation>Veuillez spécifier un commentaire pour ce changeset</translation>
    </message>
</context>
<context>
    <name>TMSPreferencesDialog</name>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="14"/>
        <source>TMS servers setup</source>
        <translation>Configuration des serveurs TMS</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="20"/>
        <source>TMS Servers</source>
        <translation>Serveurs TMS</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="47"/>
        <source>Server list:</source>
        <translation>Liste des serveurs:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="76"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="93"/>
        <source>Server Url:</source>
        <translation>Url du serveur:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="103"/>
        <source>Get Services</source>
        <translation>Obtenir les Services</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="125"/>
        <source>Services :</source>
        <translation>Services :</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="135"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;This is a &amp;quot;standard&amp;quot; &lt;a href=&quot;http://wiki.osgeo.org/wiki/Tile_Map_Service_Specification&quot;&gt;TMS&lt;/a&gt; server&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Ceci est un TMS &amp;quot;standard&amp;quot; &lt;a href=&quot;http://wiki.osgeo.org/wiki/Tile_Map_Service_Specification&quot;&gt;&lt;/a&gt; server&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="164"/>
        <source>Projection</source>
        <translation>Projection</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="172"/>
        <source>Mercator (EPSG:900913)</source>
        <translation>Mercator (EPSG:900913)</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="177"/>
        <source>Lat/Lon (EPSG:4326)</source>
        <translation>Lat/Lon (EPSG:4326)</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="185"/>
        <source>Origin is bottom-left</source>
        <translation>L&apos;Origine est en bas à gauche</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="192"/>
        <source>Tile size</source>
        <translation>Taille de la tuile</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="209"/>
        <source>Minimum zoom</source>
        <translation>Zoom minimum</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="219"/>
        <source>Maximum zoom</source>
        <translation>Zoom maximum</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="245"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="252"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="259"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.cpp" line="212"/>
        <location filename="../src/Preferences/TMSPreferencesDialog.cpp" line="265"/>
        <location filename="../src/Preferences/TMSPreferencesDialog.cpp" line="275"/>
        <source>Merkaartor: GetServices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.cpp" line="212"/>
        <source>Address and Path cannot be blank.</source>
        <translation>L&apos;adresse et le chemin ne peuvent pas être vides.</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.cpp" line="266"/>
        <source>Download failed: %1.</source>
        <translation>Le téléchargement a échoué: %1.</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.cpp" line="275"/>
        <source>Error reading services.
</source>
        <translation>Erreur lors de la lecture des services.
</translation>
    </message>
</context>
<context>
    <name>TagModel</name>
    <message>
        <location filename="../src/TagModel.cpp" line="105"/>
        <source>Key</source>
        <translation>Clef</translation>
    </message>
    <message>
        <location filename="../src/TagModel.cpp" line="107"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../src/TagModel.h" line="22"/>
        <source>Edit this to add...</source>
        <translation>Editer ceci pour ajouter...</translation>
    </message>
</context>
<context>
    <name>TagSelectorWidget</name>
    <message>
        <location filename="../src/Utils/TagSelectorWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../src/Utils/TagSelectorWidget.ui" line="34"/>
        <source>OR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Utils/TagSelectorWidget.ui" line="44"/>
        <source>AND</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Utils/TagSelectorWidget.ui" line="54"/>
        <source>NOT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Utils/TagSelectorWidget.ui" line="64"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/Utils/TagSelectorWidget.ui" line="76"/>
        <source>is</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Utils/TagSelectorWidget.ui" line="81"/>
        <source>isoneof</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagTemplateWidgetCombo</name>
    <message>
        <location filename="../src/TagTemplate/TagTemplate.cpp" line="193"/>
        <source>Undefined</source>
        <translation>Non défini</translation>
    </message>
</context>
<context>
    <name>TagTemplates</name>
    <message>
        <location filename="../src/TagTemplate/TagTemplate.cpp" line="904"/>
        <source>Undefined</source>
        <translation>Non défini</translation>
    </message>
</context>
<context>
    <name>TerraceDialog</name>
    <message>
        <location filename="../src/TerraceDialog.ui" line="14"/>
        <source>Terrace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="20"/>
        <source>No House Numbering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="32"/>
        <source>Number of houses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="51"/>
        <source>Add House Numbering (Karlsruhe scheme)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="73"/>
        <source>Ranges</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="86"/>
        <source>For example &quot;1-9;15-17;19,19A&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="93"/>
        <source>Pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="104"/>
        <source>All numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="109"/>
        <source>Odd numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="114"/>
        <source>Even numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="122"/>
        <source>Select a node in the area as well to indicate the end with the first house number</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackLayer</name>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="867"/>
        <source># of track segments</source>
        <translation>Nombre de Segments de trace</translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="867"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="868"/>
        <source>Total distance</source>
        <translation>Distance totale</translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="868"/>
        <source>%1 km</source>
        <translation>%1 km</translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="869"/>
        <source>Total duration</source>
        <translation>Durée totale</translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="869"/>
        <source>%1h %2m</source>
        <translation>%1h %2m</translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="765"/>
        <source>Extract - %1</source>
        <translation>Extraction - %1</translation>
    </message>
</context>
<context>
    <name>TrackLayerWidget</name>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="458"/>
        <source>Extract Drawing layer</source>
        <translation>Extraire une couche de dessin</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="463"/>
        <source>Zoom</source>
        <translation>Faire un zoom</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="471"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>TrackPointProperties</name>
    <message>
        <location filename="../src/Docks/MinimumTrackPointProperties.ui" line="14"/>
        <source>Trackpoint</source>
        <translation>Point</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumTrackPointProperties.ui" line="34"/>
        <source>Latitude</source>
        <translation>Latitude</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumTrackPointProperties.ui" line="44"/>
        <source>Longitude</source>
        <translation>Longitude</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumTrackPointProperties.ui" line="51"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumTrackPointProperties.ui" line="58"/>
        <source>xxx</source>
        <translation>xxx</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumTrackPointProperties.ui" line="81"/>
        <source>Tags</source>
        <translation>Etiquettes</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumTrackPointProperties.ui" line="101"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>UploadMapDialog</name>
    <message>
        <location filename="../src/UploadMapDialog.ui" line="13"/>
        <source>Dialog</source>
        <translation>Boîte de dialogue</translation>
    </message>
    <message>
        <location filename="../src/UploadMapDialog.ui" line="36"/>
        <source>Website</source>
        <translation>Site Internet</translation>
    </message>
    <message>
        <location filename="../src/UploadMapDialog.ui" line="49"/>
        <source>Password</source>
        <translation>Mot-de-passe</translation>
    </message>
    <message>
        <location filename="../src/UploadMapDialog.ui" line="56"/>
        <source>Username</source>
        <translation>Nom d’utilisateur</translation>
    </message>
    <message>
        <location filename="../src/UploadMapDialog.ui" line="63"/>
        <source>Use proxy</source>
        <translation>Utiliser un serveur mandataire</translation>
    </message>
    <message>
        <location filename="../src/UploadMapDialog.ui" line="88"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../src/UploadMapDialog.ui" line="104"/>
        <source>99999; </source>
        <translation>99999; </translation>
    </message>
</context>
<context>
    <name>UploadedLayerWidget</name>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="524"/>
        <source>Zoom</source>
        <translation>Faire un zoom</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="529"/>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
</context>
<context>
    <name>WMSPreferencesDialog</name>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.cpp" line="257"/>
        <location filename="../src/Preferences/WMSPreferencesDialog.cpp" line="314"/>
        <location filename="../src/Preferences/WMSPreferencesDialog.cpp" line="339"/>
        <source>Merkaartor: GetCapabilities</source>
        <translation>Merkaartor: Obtenir les capacités</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.cpp" line="257"/>
        <source>Address and Path cannot be blank.</source>
        <translation>L&apos;adresse et le chemin ne peuvent pas être vides.</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.cpp" line="315"/>
        <source>Download failed: %1.</source>
        <translation>Le téléchargement a échoué: %1.</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.cpp" line="339"/>
        <source>Error reading capabilities.
</source>
        <translation>Erreur durant la lecture des capacités.
</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="14"/>
        <source>WMS servers setup</source>
        <translation>Configuration des serveurs WMS</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="20"/>
        <source>WMS Servers</source>
        <translation>Serveurs WMS</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="47"/>
        <source>Server list:</source>
        <translation>Liste des serveurs:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="76"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="110"/>
        <source>Layers:</source>
        <translation>Couches:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="155"/>
        <source>Projection:</source>
        <translation>Projection:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="236"/>
        <source>Styles:</source>
        <translation>Styles:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="222"/>
        <source>Image format</source>
        <translation>Format d&apos;image</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="103"/>
        <source>Get Capabilities</source>
        <translation>Obtenir les Capacités</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="132"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This is a caching WMS (&lt;/span&gt;&lt;a href=&quot;http://wiki.openstreetmap.org/wiki/Merkaartor/Documentation#WMS-C_Servers&quot;&gt;&lt;span style=&quot; font-weight:600; text-decoration: underline; color:#0000ff;&quot;&gt;WMS-C)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-weight:600;&quot;&gt; server&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="178"/>
        <source>Tile it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="198"/>
        <source>Zoom levels</source>
        <translation>Niveaux de zoom</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="262"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="269"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="276"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="93"/>
        <source>Server Url:</source>
        <translation>Url du serveur:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="124"/>
        <source>1</source>
        <translation>1</translation>
    </message>
</context>
<context>
    <name>WorldOsbManager</name>
    <message>
        <location filename="../src/Tools/WorldOsbManager.cpp" line="87"/>
        <source>Select OSB World directory</source>
        <translation>Sélectionnez le répertoire &quot;OSB Mondial&quot;</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.cpp" line="101"/>
        <source>Invalid OSB World directory name</source>
        <translation>Répertoire &quot;OSB Mondial&quot; non valable</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.cpp" line="102"/>
        <source>Please provide a valid directory name.</source>
        <translation>Veuillez spécifier un nom de répertoire valide</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.cpp" line="115"/>
        <source>Region generation error</source>
        <translation>Erreur de génération de région</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.cpp" line="116"/>
        <source>Error while generating region %1</source>
        <translation>Erreur lors de la génération de la région %1</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.ui" line="13"/>
        <source>Dialog</source>
        <translation>Boîte de dialogue</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.ui" line="24"/>
        <source>World URI</source>
        <translation>URI mondial</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.ui" line="47"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.ui" line="58"/>
        <source>Auto-load</source>
        <translation>Chargement-automatique</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.ui" line="65"/>
        <source>Auto-show</source>
        <translation>Affichage-automatique</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.ui" line="94"/>
        <source>Show Grid</source>
        <translation>Afficher la grille</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.ui" line="126"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
</context>
</TS>
