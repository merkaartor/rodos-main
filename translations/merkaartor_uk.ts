<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="uk">
<context>
    <name></name>
    <message>
        <location filename="../src/MainWindow.cpp" line="921"/>
        <location filename="../src/MainWindow.cpp" line="935"/>
        <location filename="../src/MainWindow.cpp" line="950"/>
        <source>Supported formats</source>
        <translation>Підтримувані формати</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="922"/>
        <location filename="../src/MainWindow.cpp" line="936"/>
        <source>Merkaartor document (*.mdc)
</source>
        <translation>Документи Merkaartor  (*.mdc)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="923"/>
        <location filename="../src/MainWindow.cpp" line="937"/>
        <location filename="../src/MainWindow.cpp" line="951"/>
        <source>GPS Exchange format (*.gpx)
</source>
        <translation>Формат обміну GPS даними (*.gpx)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="924"/>
        <location filename="../src/MainWindow.cpp" line="938"/>
        <location filename="../src/MainWindow.cpp" line="952"/>
        <source>OpenStreetMap format (*.osm)
</source>
        <translation>Формат OpenStreetMap (*.osm)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="925"/>
        <location filename="../src/MainWindow.cpp" line="939"/>
        <location filename="../src/MainWindow.cpp" line="953"/>
        <source>OpenStreetMap binary format (*.osb)
</source>
        <translation>Двійковий формат OpenStreetMap (*.osb)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="927"/>
        <location filename="../src/MainWindow.cpp" line="941"/>
        <location filename="../src/MainWindow.cpp" line="955"/>
        <source>Noni GPSPlot format (*.ngt)
</source>
        <translation>Формат Noni GPSPlot (*.ngt)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="928"/>
        <location filename="../src/MainWindow.cpp" line="942"/>
        <location filename="../src/MainWindow.cpp" line="956"/>
        <source>NMEA GPS log format (*.nmea *.nma)
</source>
        <translation>Формат GPS даних NMEA (*.nmea *.nma)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="929"/>
        <location filename="../src/MainWindow.cpp" line="943"/>
        <location filename="../src/MainWindow.cpp" line="957"/>
        <source>KML file (*.kml)
</source>
        <translation>Файли KML (*.kml)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="930"/>
        <location filename="../src/MainWindow.cpp" line="944"/>
        <location filename="../src/MainWindow.cpp" line="958"/>
        <source>ESRI Shapefile (*.shp)
</source>
        <translation>Шейпфайли ESRI (*.shp)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="932"/>
        <location filename="../src/MainWindow.cpp" line="947"/>
        <location filename="../src/MainWindow.cpp" line="960"/>
        <source>All Files (*)</source>
        <translation>Всі файли (*)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="946"/>
        <source>Geotagged images (*.jpg)
</source>
        <translation>Малюнки позначені геотеґами (*.jpg)
</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="926"/>
        <location filename="../src/MainWindow.cpp" line="940"/>
        <location filename="../src/MainWindow.cpp" line="954"/>
        <source>OpenStreetMap change format (*.osc)
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="931"/>
        <location filename="../src/MainWindow.cpp" line="945"/>
        <location filename="../src/MainWindow.cpp" line="959"/>
        <source>Comma delimited format (*.csv)
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/AboutDialog.ui" line="14"/>
        <source>About Merkaartor</source>
        <translation>Про програму Merkaartor</translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="30"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-style:italic;&quot;&gt;Merkaartor version&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;%1%2(%3)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-style:italic;&quot;&gt;Версія Merkaartor&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;%1%2(%3)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="42"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;QT version&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;%1&lt;/span&gt; (built with &lt;span style=&quot; font-weight:600;&quot;&gt;%2&lt;/span&gt;)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Версія QT&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;%1&lt;/span&gt; (зібрана з &lt;span style=&quot; font-weight:600;&quot;&gt;%2&lt;/span&gt;)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="54"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Boost version&lt;/span&gt;&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="92"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="101"/>
        <source>Copyright Bart Vanhauwaert, Chris Browet and others, 2006-2010</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="108"/>
        <source>This program is licensed under the GNU Public License v2</source>
        <translation>Ця програма ліцензована на умовах GNU Public License v2</translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="115"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://www.merkaartor.org&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.merkaartor.org&lt;/span&gt;&lt;/a&gt; or &lt;a href=&quot;http://merkaartor.yuio.de&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://merkaartor.yuio.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://www.merkaartor.org&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.merkaartor.org&lt;/span&gt;&lt;/a&gt; або &lt;a href=&quot;http://merkaartor.yuio.de&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://merkaartor.yuio.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="135"/>
        <source>Changelog</source>
        <translation>Перелік змін</translation>
    </message>
    <message>
        <location filename="../src/AboutDialog.ui" line="178"/>
        <source>OK</source>
        <translation>Так</translation>
    </message>
</context>
<context>
    <name>ActionsDialog</name>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="20"/>
        <source>Description</source>
        <translation>Опис</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="20"/>
        <source>Shortcut</source>
        <translation>Комбінації клавіш</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="41"/>
        <source>&amp;Import</source>
        <translation>&amp;Імпортувати</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="42"/>
        <source>&amp;Export</source>
        <translation>&amp;Експорт</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="43"/>
        <source>&amp;Default</source>
        <translation>За&amp;звичай</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="44"/>
        <source>&amp;OK</source>
        <translation>&amp;Так</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="45"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Відміна</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="72"/>
        <source>Shortcut Editor</source>
        <translation>Редактор комбінацій клавіш</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="116"/>
        <source>Load Shortcut scheme</source>
        <translation>Завантажити схеми комбінацій клавіш</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="116"/>
        <location filename="../src/Tools/ActionsDialog.cpp" line="143"/>
        <source>Merkaartor shortcut scheme (*.mss)</source>
        <translation>Схеми комбінацій клавіш Merkaartor (*.mss)</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="120"/>
        <source>Unable to open file</source>
        <translation>Неможливо відкрити файл</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="120"/>
        <source>%1 could not be opened.</source>
        <translation>%1 неможливо відкрити.</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="143"/>
        <source>Save Shortcut scheme</source>
        <translation>Зберегти схему комбінації клавіш</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="143"/>
        <source>untitled</source>
        <translation>без назви</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="148"/>
        <source>Unable to open save file</source>
        <translation>Неможливо відкрити збережений файл</translation>
    </message>
    <message>
        <location filename="../src/Tools/ActionsDialog.cpp" line="148"/>
        <source>%1 could not be opened for writing.</source>
        <translation>%1 неможливо відкрити для запису.</translation>
    </message>
</context>
<context>
    <name>Command</name>
    <message>
        <location filename="../src/Commands/Command.cpp" line="25"/>
        <source>No description</source>
        <translation>Немає опису</translation>
    </message>
</context>
<context>
    <name>CreateDoubleWayDock</name>
    <message>
        <location filename="../src/Interactions/CreateDoubleWayDock.ui" line="13"/>
        <source>Form</source>
        <translation>Подвійна дорога</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateDoubleWayDock.ui" line="25"/>
        <source>Driving at the right side of the road</source>
        <translation>Правосторонній рух</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateDoubleWayDock.ui" line="40"/>
        <source>Distance between two roads</source>
        <translation>Відстань між двома дорогами</translation>
    </message>
</context>
<context>
    <name>CreateRoundaboutDock</name>
    <message>
        <location filename="../src/Interactions/CreateRoundaboutDock.ui" line="13"/>
        <source>Form</source>
        <translation>Подвійна дорога</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateRoundaboutDock.ui" line="25"/>
        <source>Driving at the right side of the road</source>
        <translation>Правосторонній рух</translation>
    </message>
</context>
<context>
    <name>DirtyDock</name>
    <message>
        <location filename="../src/Docks/DirtyDock.cpp" line="69"/>
        <source>There is &lt;b&gt;no&lt;/b&gt; object in the dirty layer</source>
        <translation>&lt;b&gt;Немає об’єктів&lt;/b&gt; у шарі-чернетці</translation>
    </message>
    <message>
        <location filename="../src/Docks/DirtyDock.cpp" line="72"/>
        <source>There is &lt;b&gt;one&lt;/b&gt; object in the dirty layer</source>
        <translation>В шарі-чернетці &lt;b&gt;один&lt;/b&gt; об’єкт</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/Docks/DirtyDock.cpp" line="76"/>
        <source>There are &lt;b&gt;%n&lt;/b&gt; objects in the dirty layer</source>
        <translation type="unfinished">
            <numerusform>В шарі-чернетці &lt;b&gt;%n&lt;/b&gt; об’єкт</numerusform>
            <numerusform>В шарі-чернетці &lt;b&gt;%n&lt;/b&gt; об’єкти</numerusform>
            <numerusform>В шарі-чернетці &lt;b&gt;%n&lt;/b&gt; об’єктів</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/Docks/DirtyDock.cpp" line="196"/>
        <source>Undo</source>
        <translation>Відмінити</translation>
    </message>
    <message>
        <location filename="../src/Docks/DirtyDock.cpp" line="197"/>
        <source>Center map</source>
        <translation>Центрувати мапу</translation>
    </message>
    <message>
        <location filename="../src/Docks/DirtyDock.cpp" line="198"/>
        <source>Center &amp;&amp; Zoom map</source>
        <translation>Центрувати та масштабувати мапу</translation>
    </message>
</context>
<context>
    <name>DirtyDockWidget</name>
    <message>
        <location filename="../src/Docks/DirtyDock.ui" line="14"/>
        <source>Undo</source>
        <translation>Відмінити</translation>
    </message>
    <message>
        <location filename="../src/Docks/DirtyDock.ui" line="37"/>
        <source>There is &lt;b&gt;no&lt;/b&gt; object in the dirty layer</source>
        <translation>&lt;b&gt;Немає об’єктів&lt;/b&gt; у шарі-чернетці</translation>
    </message>
    <message>
        <location filename="../src/Docks/DirtyDock.ui" line="56"/>
        <source>Cleanup</source>
        <translation>Очистити</translation>
    </message>
</context>
<context>
    <name>DirtyLayerWidget</name>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="503"/>
        <source>Zoom</source>
        <translation>Масштаб</translation>
    </message>
</context>
<context>
    <name>DirtyListExecutor</name>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="334"/>
        <location filename="../src/Sync/DirtyList.cpp" line="575"/>
        <source>ADD road %1</source>
        <translation>ДОДАНО дорогу %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="342"/>
        <location filename="../src/Sync/DirtyList.cpp" line="605"/>
        <source>ADD trackpoint %1</source>
        <translation>ДОДАНО точку шляху %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="350"/>
        <location filename="../src/Sync/DirtyList.cpp" line="546"/>
        <source>ADD relation %1</source>
        <translation>ДОДАНО відношення %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="358"/>
        <location filename="../src/Sync/DirtyList.cpp" line="695"/>
        <source>UPDATE trackpoint %1</source>
        <translation>ОНОВЛЕНО точку шляху %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="366"/>
        <location filename="../src/Sync/DirtyList.cpp" line="636"/>
        <source>UPDATE relation %1</source>
        <translation>ОНОВЛЕНО відношення %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="374"/>
        <location filename="../src/Sync/DirtyList.cpp" line="666"/>
        <source>UPDATE road %1</source>
        <translation>ОНОВЛЕНО дорогу %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="382"/>
        <location filename="../src/Sync/DirtyList.cpp" line="726"/>
        <source>REMOVE trackpoint %1</source>
        <translation>ВИЛУЧЕНО точку шляху %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="390"/>
        <location filename="../src/Sync/DirtyList.cpp" line="751"/>
        <source>REMOVE road %1</source>
        <translation>ВИЛУЧЕНО дорогу %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="398"/>
        <location filename="../src/Sync/DirtyList.cpp" line="776"/>
        <source>REMOVE relation %1</source>
        <translation>ВИЛУЧЕНО відношення %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="430"/>
        <location filename="../src/Sync/DirtyList.cpp" line="437"/>
        <location filename="../src/Sync/DirtyList.cpp" line="453"/>
        <source>Error uploading request</source>
        <translation>Помилка запиту надсилання даних</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="431"/>
        <source>Please check your username and password in the Preferences menu</source>
        <translation>Будь ласка, перевірте ваш логін та пароль в меню Параметри</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="434"/>
        <location filename="../src/Sync/DirtyList.cpp" line="454"/>
        <source>There was an error uploading this request (%1)
Server message is &apos;%2&apos;</source>
        <translation>Помилка запиту на надсилання даних: (%1)
Повідомлення сервера: &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="436"/>
        <source>
API message is &apos;%1&apos;</source>
        <translation>
повідомлення API: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="471"/>
        <source>Uploading changes...</source>
        <translation>Надсилання змін…</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="498"/>
        <source>OPEN changeset</source>
        <translation>ВІДКРИВАЮ набір змін</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyList.cpp" line="526"/>
        <source>CLOSE changeset</source>
        <translation>ЗАКРИВАЮ набір змін</translation>
    </message>
</context>
<context>
    <name>DirtyListExecutorOSC</name>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="61"/>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="68"/>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="82"/>
        <source>Error uploading request</source>
        <translation>Помилка запиту надсилання даних</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="62"/>
        <source>Please check your username and password in the Preferences menu</source>
        <translation>Будь ласка, перевірте ваш логін та пароль в меню Параметри</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="65"/>
        <source>There was an error uploading this request (%1)
Server message is &apos;%2&apos;</source>
        <translation>Помилка запиту на надсилання даних: (%1)
Повідомлення сервера: &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="67"/>
        <source>
API message is &apos;%1&apos;</source>
        <translation>
повідомлення API: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="83"/>
        <source>There was an error uploading this request (%1)
&quot;%2&quot;
Please redownload the problematic feature to handle the conflict.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="99"/>
        <source>Checking changes...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="170"/>
        <source>OPEN changeset</source>
        <translation>ВІДКРИВАЮ набір змін</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="288"/>
        <source>CLOSE changeset</source>
        <translation>ЗАКРИВАЮ набір змін</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="342"/>
        <source>ADD relation %1</source>
        <translation>ДОДАНО відношення %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="356"/>
        <source>ADD road %1</source>
        <translation>ДОДАНО дорогу %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="371"/>
        <source>ADD trackpoint %1</source>
        <translation>ДОДАНО точку шляху %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="387"/>
        <source>UPDATE relation %1</source>
        <translation>ОНОВЛЕНО відношення %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="402"/>
        <source>UPDATE road %1</source>
        <translation>ОНОВЛЕНО дорогу %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="416"/>
        <source>UPDATE trackpoint %1</source>
        <translation>ОНОВЛЕНО точку шляху %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="430"/>
        <source>REMOVE trackpoint %1</source>
        <translation>ВИЛУЧЕНО точку шляху %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="444"/>
        <source>REMOVE road %1</source>
        <translation>ВИЛУЧЕНО дорогу %1</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="458"/>
        <source>REMOVE relation %1</source>
        <translation>ВИЛУЧЕНО відношення %1</translation>
    </message>
</context>
<context>
    <name>Document</name>
    <message>
        <location filename="../src/Document.cpp" line="108"/>
        <source>Dirty layer</source>
        <translation>Шар чернетки</translation>
    </message>
    <message>
        <location filename="../src/Document.cpp" line="111"/>
        <source>Uploaded layer</source>
        <translation>Надісланий шар</translation>
    </message>
    <message>
        <location filename="../src/Document.cpp" line="274"/>
        <source>Background imagery</source>
        <translation>Фонове зображення</translation>
    </message>
    <message>
        <location filename="../src/Document.cpp" line="508"/>
        <location filename="../src/Document.cpp" line="565"/>
        <source>OSM Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Document.cpp" line="515"/>
        <location filename="../src/Document.cpp" line="575"/>
        <source>Exporting OSM...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloadMapDialog</name>
    <message>
        <location filename="../src/DownloadMapDialog.ui" line="14"/>
        <source>Download</source>
        <translation>Завантажити</translation>
    </message>
    <message>
        <location filename="../src/DownloadMapDialog.ui" line="32"/>
        <source>Location</source>
        <translation>Місце знаходження</translation>
    </message>
    <message>
        <location filename="../src/DownloadMapDialog.ui" line="40"/>
        <source>Bookmark</source>
        <translation>Закладка</translation>
    </message>
    <message>
        <location filename="../src/DownloadMapDialog.ui" line="62"/>
        <source>Current view</source>
        <translation>Поточний вид</translation>
    </message>
    <message>
        <location filename="../src/DownloadMapDialog.ui" line="74"/>
        <source>WWW link (OSM/google maps)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DownloadMapDialog.ui" line="86"/>
        <source>From the map below (map provided by the OpenStreetMap project)</source>
        <translation>З мапи нижче (мапу надано проектом OpenStreetMap)</translation>
    </message>
    <message>
        <location filename="../src/DownloadMapDialog.ui" line="98"/>
        <source>Also download raw GPS tracks</source>
        <translation>Завантажити необроблені треки GPS</translation>
    </message>
    <message>
        <location filename="../src/DownloadMapDialog.ui" line="105"/>
        <source>Resolve all relations</source>
        <translation>Розв’язати усі відношення</translation>
    </message>
</context>
<context>
    <name>Downloader</name>
    <message>
        <location filename="../src/Docks/InfoDock.cpp" line="81"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="486"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="491"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="497"/>
        <source>Download failed</source>
        <translation>Невдача завантаження</translation>
    </message>
    <message>
        <location filename="../src/Docks/InfoDock.cpp" line="81"/>
        <source>Unexpected http status code (%1)</source>
        <translation>Несподіваний код статусу http (%1)</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="207"/>
        <source>error</source>
        <translation>помилка</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="211"/>
        <source>didn&apos;t download enough</source>
        <translation>недостатньо завантажено</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/Sync/DownloadOSM.cpp" line="320"/>
        <source>Downloading from OSM (%n bytes)</source>
        <translation type="unfinished">
            <numerusform>Завантаження з OSM (%n байт)</numerusform>
            <numerusform>Завантаження з OSM (%n байти)</numerusform>
            <numerusform>Завантаження з OSM (%n байтів)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/Sync/DownloadOSM.cpp" line="322"/>
        <source>Downloading from OSM (%n kBytes)</source>
        <translation type="unfinished">
            <numerusform>Завантаження з OSM (%n Кбайт)</numerusform>
            <numerusform>Завантаження з OSM (%n Кбайти)</numerusform>
            <numerusform>Завантаження з OSM (%n Кбайтів)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="442"/>
        <source>Downloading...</source>
        <translation>Завантаження…</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="452"/>
        <source>Downloading from OSM (connecting)</source>
        <translation>Завантаження з OSM (з’єднуємось з сервером)</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="483"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="494"/>
        <source>Unexpected http status code (%1)
Server message is &apos;%2&apos;</source>
        <translation>Неочікуваний код статусу http (%1)
Відповідь сервера &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="485"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="496"/>
        <source>
API message is &apos;%1&apos;</source>
        <translation>
повідомлення API: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="491"/>
        <source>Username/password invalid</source>
        <translation>Хибні логін/пароль</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="509"/>
        <source>Unresolved conflicts</source>
        <translation>Нерозв’язувані конфлікти</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="509"/>
        <source>Please resolve existing conflicts first</source>
        <translation>Будь ласка, розв’яжіть поточні конфлікти</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="688"/>
        <source>OpenStreetBugs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="713"/>
        <source>Downloading points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="433"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="555"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="700"/>
        <source>Parsing...</source>
        <translation>Обробка…</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="439"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="562"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="707"/>
        <source>Parsing XML</source>
        <translation>Обробка XML</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="570"/>
        <source>Downloading trackpoints %1-%2</source>
        <translation>Завантажуються точки треку %1-%2</translation>
    </message>
    <message>
        <location filename="../src/Sync/DownloadOSM.cpp" line="588"/>
        <source>Downloaded track - nodes %1-%2</source>
        <translation>Завантажений трек - точки %1-%2</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportExportOSC.cpp" line="73"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="628"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="742"/>
        <location filename="../src/Sync/DownloadOSM.cpp" line="846"/>
        <source>%1 download</source>
        <translation>%1 завантажено</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="313"/>
        <source>Downloading unresolved...</source>
        <translation>Завантаження нерозв’язуваних елементів…</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="320"/>
        <source>Downloading unresolved %1 of %2</source>
        <translation>Завантажується нерозв’язуваний елемент %1 з %2</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="329"/>
        <source>Parsing unresolved %1 of %2</source>
        <translation>Обробка: нерозв’язуваний елемент %1 з %2</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="447"/>
        <source>Conflicts from %1</source>
        <translation>Конфлікт з %1</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="538"/>
        <source>Conflicts have been detected</source>
        <translation>Виявлено конфлікти</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="539"/>
        <source>This means that some of the feature you modified since your last download have since been modified by someone else on the server.
The features have been duplicated as &quot;conflict_...&quot; on the &quot;Conflicts...&quot; layer.
Before being able to upload your changes, you will have to manually merge the two versions and remove the one from the &quot;Conflicts...&quot; layer.</source>
        <translation>Це значить , що для частини елементів, які ви змінили з моменту останнього завантаження хтось запровадив зміни на сервері.
Елементи продубльовані у шарі &quot;Конфлікти&quot; як &quot;conflict_…&quot;.
Для того, щоб отримати можливість надіслати ваші зміни до сервера, вам потрібно власноруч об’єднати обидві версії та вилучити одну з них з шару &quot;Конфлікти&quot;.</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="517"/>
        <source>Empty roads/relations detected</source>
        <translation>Знайдено порожні дороги/відношення</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="518"/>
        <source>Empty roads/relations are probably errors.
Do you want to mark them for deletion?</source>
        <translation>Порожні дороги/відношення, можливо помилка.
Бажаєте позначити їх для вилучення?</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportOSM.cpp" line="526"/>
        <source>Remove empty feature %1</source>
        <translation>Вилучення порожнього елементу %1</translation>
    </message>
    <message>
        <location filename="../src/Utils/SlippyMapWidget.cpp" line="69"/>
        <source>Downloading %1,%2 (zoom %3)...</source>
        <translation>Завантажується %1,%2 (масштаб %3)…</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="131"/>
        <source>Uploading...</source>
        <translation>Відправка…</translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="153"/>
        <source>Preparing changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Sync/DirtyListExecutorOSC.cpp" line="155"/>
        <source>Waiting for server response</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DrawingLayerWidget</name>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="272"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../src/ImportExport/ExportDialog.ui" line="13"/>
        <source>Export</source>
        <translation>Експорт</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ExportDialog.ui" line="25"/>
        <source>What do you want to export?</source>
        <translation>Що експортувати?</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ExportDialog.ui" line="38"/>
        <source>All visible (i.e. not hidden)</source>
        <translation>Все що видно (тобто не сховане)</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ExportDialog.ui" line="45"/>
        <source>Viewport</source>
        <translation>Вміст екрану</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ExportDialog.ui" line="52"/>
        <source>Selected</source>
        <translation>Виділене</translation>
    </message>
</context>
<context>
    <name>FeaturesDock</name>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="414"/>
        <source>Features</source>
        <translation>Елементи</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="415"/>
        <source>Center map</source>
        <translation>Центрувати мапу</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="416"/>
        <source>Center &amp;&amp; Zoom map</source>
        <translation>Центрувати та масштабувати мапу</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="417"/>
        <source>Download missing children</source>
        <translation>Завантажити відсутніх нащадків</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="418"/>
        <source>Add to selection</source>
        <translation>Додати до виділення</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="419"/>
        <source>Delete</source>
        <translation>Вилучити</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="426"/>
        <source>Relations</source>
        <translation>Відношення</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="427"/>
        <source>Roads</source>
        <translation>Дороги</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="428"/>
        <source>POI&apos;s</source>
        <translation>Об’єкти</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.cpp" line="429"/>
        <source>All</source>
        <translation>Всі</translation>
    </message>
</context>
<context>
    <name>FeaturesDockWidget</name>
    <message>
        <location filename="../src/Docks/FeaturesDock.ui" line="14"/>
        <source>Features</source>
        <translation>Елементи</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.ui" line="72"/>
        <source>Only features fully within the viewport</source>
        <translation>Тільки елементи з екрану</translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.ui" line="101"/>
        <source>Find...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/FeaturesDock.ui" line="108"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FilterPreferencesDialog</name>
    <message>
        <location filename="../src/Preferences/FilterPreferencesDialog.ui" line="14"/>
        <source>Filters setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/FilterPreferencesDialog.ui" line="20"/>
        <source>Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/FilterPreferencesDialog.ui" line="47"/>
        <source>Filter list:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/FilterPreferencesDialog.ui" line="76"/>
        <source>Name:</source>
        <translation>Назва:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/FilterPreferencesDialog.ui" line="93"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/FilterPreferencesDialog.ui" line="116"/>
        <source>Apply</source>
        <translation>Застосувати</translation>
    </message>
    <message>
        <location filename="../src/Preferences/FilterPreferencesDialog.ui" line="123"/>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location filename="../src/Preferences/FilterPreferencesDialog.ui" line="130"/>
        <source>Remove</source>
        <translation>Вилучити</translation>
    </message>
</context>
<context>
    <name>GeoImageDock</name>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="85"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="903"/>
        <source>Geo Images</source>
        <translation>Гео-зображення</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="93"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="905"/>
        <source>Remove Images</source>
        <translation>Вилучити зображення</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="94"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="906"/>
        <source>Copy filename to clipboard</source>
        <translation>Копіювати ім’я файлу до буферу обміну</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="95"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="907"/>
        <source>Select next image</source>
        <translation>Вибрати наступне зображення</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="96"/>
        <source>PgDown</source>
        <translation>PgDown</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="97"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="908"/>
        <source>Select previous image</source>
        <translation>Вибрати попереднє зображення</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="98"/>
        <source>PgUp</source>
        <translation>PgUp</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="342"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="447"/>
        <source>Load geotagged Images</source>
        <translation>Завантажити зображення з гео-позначками</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="343"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="448"/>
        <source>Select the layer to which the images belong:</source>
        <translation>Оберіть шар до якого належать зображення:</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="356"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="461"/>
        <source>Layer is readonly</source>
        <translation>Шар тільки для читання</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="357"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="462"/>
        <source>The used layer is not writeable. Should it be made writeable?
If not, you can&apos;t load images that belongs to it.</source>
        <translation>Цей шар тільки для читання. Можливо його треба зробити доступним для запису?
Якщо ні — вин не зможете завантажити зображення для нього.</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="469"/>
        <source>Loading Images ...</source>
        <translation>Завантаження зображень:…</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="469"/>
        <source>Abort loading</source>
        <translation>Скасування завантаження</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="481"/>
        <source>No such file</source>
        <translation>Немає такого файлу</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="481"/>
        <source>Can&apos;t find image &quot;%1&quot;.</source>
        <translation>Неможливо знайти зображення &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="489"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="491"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="847"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="851"/>
        <source>Exiv2</source>
        <translation>Exiv2</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="489"/>
        <source>Error while opening &quot;%2&quot;:
%1</source>
        <translation>Помилка відкриття &quot;%2&quot;:
%1</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="491"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="851"/>
        <source>Error while loading EXIF-data from &quot;%1&quot;.</source>
        <translation>Помилка завантаження EXIF-даних з &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="50"/>
        <source>Network timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="50"/>
        <source>Cannot read the photo&apos;s details from the Walking Papers server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="70"/>
        <source>Please specify Walking Papers URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="71"/>
        <source>URL:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="92"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="904"/>
        <source>Center map</source>
        <translation>Центрувати мапу</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="99"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="909"/>
        <source>Save geotagged image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="298"/>
        <location filename="../src/Docks/GeoImageDock.cpp" line="403"/>
        <source>Photo layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="642"/>
        <source>Specify offset</source>
        <translation>Вкажіть координати</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="644"/>
        <source>Position images more to the:</source>
        <translation>Координати зображення більші за:</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="645"/>
        <source>end of the track</source>
        <translation>кінець треку</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="646"/>
        <source>beginning of the track</source>
        <translation>початок треку</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="650"/>
        <source>hh:mm:ss</source>
        <translation>гг:хх:сс</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="708"/>
        <source>No TrackPoints</source>
        <translation>Точки треку відсутні</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="708"/>
        <source>No TrackPoints found for image &quot;%1&quot;</source>
        <translation>Точки треку для зображення &quot;%1&quot; відсутні</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="715"/>
        <source>ss &apos;seconds&apos;</source>
        <translation>сс &apos;секунд’</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="717"/>
        <source>mm &apos;minutes and&apos; ss &apos;seconds&apos;</source>
        <translation>хх &apos;хвилин’ та сс &apos;секунд’</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="719"/>
        <source>hh &apos;hours,&apos; mm &apos;minutes and&apos; ss &apos;seconds&apos;</source>
        <translation>гг &apos;годин’, хх &apos;хвилин’ та сс &apos;секунд’</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="720"/>
        <source>Wrong image?</source>
        <translation>Помилкове зображення?</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="721"/>
        <source>Image &quot;%1&quot; was taken %2 before the next trackpoint was recorded.
Do you still want to use it?</source>
        <translation>Зображення &quot;%1&quot; отримане %2 до того як була записана подальша точка шляху.
Ви бажаєте застосувати його?</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="722"/>
        <source>Image &quot;%1&quot; was taken %2 after the last trackpoint was recorded.
Do you still want to use it?</source>
        <translation>Зображення &quot;%1&quot; отримане %2 після того як була записана остання точка шляху.
Ви бажаєте застосувати його?</translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="774"/>
        <source>JPEG Images (*.jpg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/GeoImageDock.cpp" line="847"/>
        <source>Error while opening &quot;%1&quot;:
%2</source>
        <translation>Помилка відкриття &quot;%1&quot;:
%2</translation>
    </message>
</context>
<context>
    <name>GotoDialog</name>
    <message>
        <location filename="../src/GotoDialog.cpp" line="109"/>
        <source>Invalid OSM url</source>
        <translation>Хибний OSM url</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.cpp" line="110"/>
        <source>The specified url is invalid!</source>
        <translation>Вказаний url — хибний!</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.cpp" line="118"/>
        <location filename="../src/GotoDialog.cpp" line="127"/>
        <source>Invalid Coordinates format</source>
        <translation>Хибний формат координат</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.cpp" line="119"/>
        <source>Coordinates must be: &apos;&lt;left lon&gt;, &lt;bottom lat&gt;, &lt;right lon&gt;, &lt;top lat&gt;&apos;</source>
        <translation>Координати задаються рядком: &apos;&lt;left lon&gt;, &lt;bottom lat&gt;, &lt;right lon&gt;, &lt;top lat&gt;&apos;</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.cpp" line="128"/>
        <source>Coordinates must be: &apos;&lt;center lat&gt;, &lt;center lon&gt;, &lt;span lat&gt;, &lt;span lon&gt;&apos;</source>
        <translation>Координати задаються рядком: &apos;&lt;center lat&gt;, &lt;center lon&gt;, &lt;span lat&gt;, &lt;span lon&gt;&apos;</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="14"/>
        <source>Go To</source>
        <translation>Перейти до</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="28"/>
        <source>Bookmark</source>
        <translation>Закладка</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="61"/>
        <source>WWW link (OSM/google maps)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="88"/>
        <source>lonMin, latMin, lonMax, latMax</source>
        <translation>довМін, ширМін, довМакс, ширМакс</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="115"/>
        <source>latCent, lonCent, latSpan, lonSpan</source>
        <translation>ширЦ-р, довЦ-р, ширВідст, довВідст</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="134"/>
        <source>Info</source>
        <translation>Інформація</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="148"/>
        <source>OSM API Url</source>
        <translation>OSM API Url</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="175"/>
        <source>OSM XAPI url</source>
        <translation>OSM XAPI url</translation>
    </message>
    <message>
        <location filename="../src/GotoDialog.ui" line="197"/>
        <location filename="../src/GotoDialog.ui" line="210"/>
        <source>Search</source>
        <translation>Пошук</translation>
    </message>
</context>
<context>
    <name>ImageLayerWidget</name>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="287"/>
        <source>None</source>
        <translation>Немає</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="293"/>
        <source>Shape adapter</source>
        <translation>Адаптер форм</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="371"/>
        <source>WMS adapter</source>
        <translation>WMS адаптер</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="387"/>
        <source>TMS adapter</source>
        <translation>TMS адаптер</translation>
    </message>
</context>
<context>
    <name>ImageMapLayer</name>
    <message>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="66"/>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="156"/>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="193"/>
        <source>Map - None</source>
        <translation>Мапа - Немає</translation>
    </message>
    <message>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="167"/>
        <source>Map - WMS - %1</source>
        <translation>Мапа - WMS - %1</translation>
    </message>
    <message>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="172"/>
        <source>Map - WMS-C - %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="177"/>
        <source>Map - WMS-Tiled - %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="188"/>
        <source>Map - TMS - %1</source>
        <translation>Мапа - TMS - %1</translation>
    </message>
    <message>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="209"/>
        <source>Map - OSB Background</source>
        <translation>Мапа - фон з OSB</translation>
    </message>
    <message>
        <location filename="../src/Layers/ImageMapLayer.cpp" line="215"/>
        <source>Map - %1</source>
        <translation>Мапа - %1</translation>
    </message>
</context>
<context>
    <name>ImportCSVDialog</name>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="31"/>
        <source>String</source>
        <translation>Рядок</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="32"/>
        <source>Integer</source>
        <translation>Ціле число</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="33"/>
        <source>Float</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="34"/>
        <source>Longitude</source>
        <translation>Довгота</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="35"/>
        <source>Latitude</source>
        <translation>Широта</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="302"/>
        <source>Invalid projection</source>
        <translation>Помилкова проекція</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="302"/>
        <source>Unable to set projection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="321"/>
        <source>No coordinates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="322"/>
        <source>Latitude or Longitude field missing. It will be impossible to import the file.
Do you really want to exit?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="359"/>
        <source>Load CSV import settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="359"/>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="469"/>
        <source>Merkaartor import settings (*.mis)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="365"/>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="378"/>
        <source>Invalid file</source>
        <translation>Хибний файл</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="365"/>
        <source>%1 could not be opened.</source>
        <translation>%1 неможливо відкрити.</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="378"/>
        <source>%1 is not a CSV import settings file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="469"/>
        <source>Save CSV import settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="478"/>
        <source>Unable to open save import settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.cpp" line="478"/>
        <source>%1 could not be opened for writing.</source>
        <translation>%1 неможливо відкрити для запису.</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Надсилання змін</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="20"/>
        <source>Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="49"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Courier New&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;1&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;3&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;4&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="66"/>
        <source>OSM Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="84"/>
        <source>Specifications</source>
        <translation>Специфікації</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="96"/>
        <source>First line contains headers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="103"/>
        <source>Delimiter</source>
        <translation>Розділювач</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="115"/>
        <source>Colon (,)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="122"/>
        <source>Semicolon (;)</source>
        <translation>Крапка з комою (;)</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="129"/>
        <source>Tab</source>
        <translation>Табуляція</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="136"/>
        <source>Other:</source>
        <translation>Інше:</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="162"/>
        <source>Projection (in PROJ4 format; leave blank for latitude/longitude)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="175"/>
        <source>Fields</source>
        <translation>Поля</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="202"/>
        <source>Field list:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="231"/>
        <source>Field Name (= tag key) :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="248"/>
        <source>Field Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="258"/>
        <source>Import</source>
        <translation>Імпортувати</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="284"/>
        <source>Import range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="290"/>
        <source>From (0 from start):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="300"/>
        <source>To (0 to End):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="315"/>
        <source>Load settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportCSVDialog.ui" line="322"/>
        <source>Save settings...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InfoDock</name>
    <message>
        <location filename="../src/Docks/InfoDock.cpp" line="24"/>
        <location filename="../src/Docks/InfoDock.cpp" line="94"/>
        <source>Info</source>
        <translation>Інформація</translation>
    </message>
</context>
<context>
    <name>Layer</name>
    <message numerus="yes">
        <location filename="../src/Layers/Layer.cpp" line="566"/>
        <source>%n features</source>
        <translation type="unfinished">
            <numerusform>%n елемент</numerusform>
            <numerusform>%n елементи</numerusform>
            <numerusform>%n елементів</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="566"/>
        <source>Size</source>
        <translation>Розмір</translation>
    </message>
</context>
<context>
    <name>LayerDock</name>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="222"/>
        <location filename="../src/Docks/LayerDock.cpp" line="321"/>
        <source>Show All</source>
        <translation>Показати все</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="227"/>
        <location filename="../src/Docks/LayerDock.cpp" line="325"/>
        <source>Hide All</source>
        <translation>Сховати все</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="234"/>
        <source>Readonly All</source>
        <translation>Все тільки для читання</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="239"/>
        <source>Readonly None</source>
        <translation>Редагований</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="246"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="364"/>
        <source>Layers</source>
        <translation>Шари</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="370"/>
        <source>All</source>
        <translation>Всі</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="371"/>
        <source>Default</source>
        <translation>Типові</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="372"/>
        <source>OSM</source>
        <translation>OSM</translation>
    </message>
    <message>
        <location filename="../src/Docks/LayerDock.cpp" line="373"/>
        <source>Tracks</source>
        <translation>Треки</translation>
    </message>
</context>
<context>
    <name>LayerWidget</name>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="160"/>
        <source>Visible</source>
        <translation>Видимий</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="166"/>
        <source>Readonly</source>
        <translation>Тільки для читання</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="174"/>
        <source>Low</source>
        <translation>прозорий</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="174"/>
        <source>High</source>
        <translation>напівпрозорий</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="174"/>
        <source>Opaque</source>
        <translation>непрозорий</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="177"/>
        <source>Opacity</source>
        <translation>Непрозорість</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="192"/>
        <source>Zoom</source>
        <translation>Масштаб</translation>
    </message>
</context>
<context>
    <name>MDiscardableDialog</name>
    <message>
        <location filename="../src/Utils/MDiscardableDialog.cpp" line="24"/>
        <source>Don&apos;t ask me this again</source>
        <translation>Не питати в майбутньому</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../src/Main.cpp" line="178"/>
        <source>Merkaartor v%1%2(%3)
Loading plugins...</source>
        <translation>Merkaartor v%1%2(%3)
Завантаження втулків…</translation>
    </message>
    <message>
        <location filename="../src/Main.cpp" line="210"/>
        <source>Merkaartor v%1%2(%3)
Initializing...</source>
        <translation>Merkaartor v%1%2(%3)
Ініціалізація…</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/ImportExport/ImportExportOSC.cpp" line="69"/>
        <source>Import osmChange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Commands/DocumentCommands.cpp" line="124"/>
        <source>Cascaded cleanup</source>
        <translation>Каскадне очищення</translation>
    </message>
    <message>
        <location filename="../src/Commands/FeatureCommands.cpp" line="66"/>
        <location filename="../src/Commands/FeatureCommands.cpp" line="74"/>
        <location filename="../src/Commands/FeatureCommands.cpp" line="188"/>
        <source>Set Tag &apos;%1=%2&apos; on %3</source>
        <translation>Встановлено теґ &apos;%1=%2&apos; для %3</translation>
    </message>
    <message>
        <location filename="../src/Commands/FeatureCommands.cpp" line="307"/>
        <location filename="../src/Commands/FeatureCommands.cpp" line="396"/>
        <location filename="../src/Docks/PropertiesDock.cpp" line="703"/>
        <source>Clear Tag &apos;%1&apos; on %2</source>
        <translation>Стерто теґ &apos;%1&apos; для %2</translation>
    </message>
    <message>
        <location filename="../src/Commands/NodeCommands.cpp" line="17"/>
        <location filename="../src/Commands/NodeCommands.cpp" line="25"/>
        <location filename="../src/Commands/NodeCommands.cpp" line="110"/>
        <source>Move node %1</source>
        <translation>Пересунути точку %1</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="751"/>
        <source>Remove member &apos;%1&apos; on %2</source>
        <translation>Вилучити елемент%1 з відношення %2</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportNMEA.cpp" line="44"/>
        <source>Import NMEA</source>
        <translation>Імпорт NMEA</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateAreaInteraction.cpp" line="36"/>
        <source>Create Area Interaction</source>
        <translation>Створити взаємозв’язки ділянки</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateAreaInteraction.cpp" line="103"/>
        <source>Create Area %1</source>
        <translation>Створити Ділянку %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateAreaInteraction.cpp" line="138"/>
        <source>Area: Create Road %1</source>
        <translation>Ділянка: Створити дорогу %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateAreaInteraction.cpp" line="164"/>
        <source>Add a hole.</source>
        <translation>Додати отвір.</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateAreaInteraction.cpp" line="165"/>
        <source>Do you want to add a(nother) hole to this area?</source>
        <translation>Бажаєте додати ще один отвір до цієї ділянки?</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateAreaInteraction.cpp" line="169"/>
        <source>Area: Finish Road %1</source>
        <translation>Ділянка: Закінчити дорогу %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateAreaInteraction.cpp" line="186"/>
        <location filename="../src/Interactions/CreateAreaInteraction.cpp" line="199"/>
        <source>Area: Add node %1 to Road %2</source>
        <translation>Ділянка: Додати точку %1 до дороги %2</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateDoubleWayInteraction.cpp" line="44"/>
        <source>Create double way Interaction</source>
        <translation>Створити взаємозв’язки подвійної дороги</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateDoubleWayInteraction.cpp" line="165"/>
        <source>Add nodes to double-way Road %1</source>
        <translation>Додати точки до подвійної дороги %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateDoubleWayInteraction.cpp" line="220"/>
        <source>Create double-way Road %1</source>
        <translation>Створити Подвійну дорогу %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateNodeInteraction.cpp" line="31"/>
        <source>Create node Interaction</source>
        <translation>Створити взаємозв’язки точки</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateNodeInteraction.cpp" line="82"/>
        <location filename="../src/Interactions/MoveNodeInteraction.cpp" line="240"/>
        <source>Create node in Road: %1</source>
        <translation>Створити точку на Дорозі %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateNodeInteraction.cpp" line="98"/>
        <source>Create point %1</source>
        <translation>Створити точку %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreatePolygonInteraction.cpp" line="30"/>
        <source>LEFT-CLICK to start;DRAG to scale;SHIFT-DRAG to rotate;LEFT-CLICK to end</source>
        <translation>ЛІВИЙ-КЛАЦ — розпочати; ТЯГТИ — зміна масштабу; SHIFT-ТЯГТИ — обертання; ЛІВИЙ-КЛАЦ — закінчити</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreatePolygonInteraction.cpp" line="35"/>
        <source>Create Polygon Interaction</source>
        <translation>Створити взаємозв’язки багатокутника</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreatePolygonInteraction.cpp" line="86"/>
        <source>Create Polygon %1</source>
        <translation>Створити Багатокутник %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateRoundaboutInteraction.cpp" line="43"/>
        <source>Create roundabout Interaction</source>
        <translation>Створити взаємозв’язки кільця</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateRoundaboutInteraction.cpp" line="87"/>
        <source>Create Roundabout %1</source>
        <translation>Створити кільце %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateSingleWayInteraction.cpp" line="44"/>
        <source>Create way Interaction</source>
        <translation>Створити взаємозв’язки шляху</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateSingleWayInteraction.cpp" line="130"/>
        <location filename="../src/Interactions/CreateSingleWayInteraction.cpp" line="181"/>
        <location filename="../src/Interactions/CreateSingleWayInteraction.cpp" line="194"/>
        <source>Create Node %1 in Road %2</source>
        <translation>Створити точку %1 на Дорозі %2</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateSingleWayInteraction.cpp" line="163"/>
        <source>Create Road: %1</source>
        <translation>Створити Дорогу: %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateSingleWayInteraction.cpp" line="171"/>
        <source>Create Node: %1</source>
        <translation>Створити Точку: %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/CreateSingleWayInteraction.cpp" line="197"/>
        <source>Add Node %1 to Road %2</source>
        <translation>Додати Точку %1 до Дороги %2</translation>
    </message>
    <message>
        <location filename="../src/Interactions/EditInteraction.cpp" line="56"/>
        <source>LEFT-CLICK to select;RIGHT-CLICK to pan;CTRL-LEFT-CLICK to toggle selection;SHIFT-LEFT-CLICK to add to selection;LEFT-DRAG for area selection;CTRL-RIGHT-DRAG for zoom;</source>
        <translation>ЛІВИЙ-КЛАЦ — виділити; ПРАВИЙ-КЛАЦ — показати ділянку; CTRL-ЛІВИЙ-КЛАЦ — поступове виділення; SHIFT-ЛІВИЙ-КЛАЦ — додати до виділення; ЛІВИЙ-ТЯГНИ — виділити ділянку; CTRL-ПРАВИЙ-ТЯГНИ — для масштабування;</translation>
    </message>
    <message>
        <location filename="../src/Interactions/EditInteraction.cpp" line="58"/>
        <source>CLICK to select/move;CTRL-CLICK to toggle selection;SHIFT-CLICK to add to selection;SHIFT-DRAG for area selection;CTRL-DRAG for zoom;</source>
        <translation>КЛАЦ — виділити/пересунути; CTRL-КЛАЦ — поступове виділення; SHIFT-КЛАЦ — додати до виділення; SHIFT-ТЯГНИ — виділення ділянки; CTRL-ТЯГНИ — масштабування;</translation>
    </message>
    <message>
        <location filename="../src/Interactions/EditInteraction.cpp" line="63"/>
        <source>Edit Interaction</source>
        <translation>Редагування взаємозв’язків</translation>
    </message>
    <message>
        <location filename="../src/Interactions/EditInteraction.cpp" line="248"/>
        <source>Remove feature %1</source>
        <translation>Вилучення елементу %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/EditInteraction.cpp" line="278"/>
        <source>Reverse Road %1</source>
        <translation>Змінити напрямок дороги %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/MoveNodeInteraction.cpp" line="39"/>
        <source>LEFT-CLICK to select;LEFT-DRAG to move</source>
        <translation>ЛІВИЙ-КЛАЦ — виділити; ЛІВИЙ-ТЯГНИ — пересування</translation>
    </message>
    <message>
        <location filename="../src/Interactions/MoveNodeInteraction.cpp" line="44"/>
        <source>Move node Interaction</source>
        <translation>Пересування точки взаємозв’язку</translation>
    </message>
    <message>
        <location filename="../src/Interactions/MoveNodeInteraction.cpp" line="139"/>
        <source>Move Nodes</source>
        <translation>Пересунути точки</translation>
    </message>
    <message>
        <location filename="../src/Interactions/MoveNodeInteraction.cpp" line="143"/>
        <source>Move Node %1</source>
        <translation>Пересунути точку %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/MoveNodeInteraction.cpp" line="193"/>
        <source>Nodes at the same position found.</source>
        <translation>Знайдено точку у тому ж місці.</translation>
    </message>
    <message>
        <location filename="../src/Interactions/MoveNodeInteraction.cpp" line="194"/>
        <source>Do you want to merge all nodes at the drop position?</source>
        <translation>Чи бажаєте з’єднати точки у цьому місці?</translation>
    </message>
    <message>
        <location filename="../src/Interactions/MoveNodeInteraction.cpp" line="203"/>
        <location filename="../src/MainWindow.cpp" line="1872"/>
        <source>Merge Nodes into %1</source>
        <translation>Об’єднати точки в %1</translation>
    </message>
    <message>
        <location filename="../src/Interactions/RotateInteraction.cpp" line="38"/>
        <source>HOVER to select;LEFT-DRAG to rotate/scale</source>
        <translation>НАВЕСТИ МИШУ — вибрати; ЛІВИЙ КЛАЦ — обертати/масштабувати</translation>
    </message>
    <message>
        <location filename="../src/Interactions/RotateInteraction.cpp" line="43"/>
        <source>Rotate Interaction</source>
        <translation>Обертання взаємозв’язків</translation>
    </message>
    <message>
        <location filename="../src/Interactions/RotateInteraction.cpp" line="127"/>
        <source>Scale/Rotate Nodes</source>
        <translation>Масштабувати/Обертати точки</translation>
    </message>
    <message>
        <location filename="../src/Interactions/ZoomInteraction.cpp" line="23"/>
        <source>LEFT-CLICK to first corner -&gt; LEFT-DRAG to specify area -&gt; LEFT-CLICK to zoom</source>
        <translation>ЛІВИЙ-КЛАЦ — перший кут; ЛІВИЙ-ТЯГТИ — позначення ділянки; ЛІВИЙ-КЛАЦ — масштабувати</translation>
    </message>
    <message>
        <location filename="../src/Interactions/ZoomInteraction.cpp" line="26"/>
        <source>Zoom Interaction</source>
        <translation>Масштабування взаємозв&apos;язків</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="180"/>
        <source>tile %v / %m</source>
        <translation>плитка %v / %m</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="315"/>
        <source>Properties...</source>
        <translation>Властивості…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="338"/>
        <source>Cannot load Projections file</source>
        <translation>Не можу завантажити файл Проекцій</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="338"/>
        <source>&quot;Projections.xml&quot; could not be opened anywhere. Aborting.</source>
        <translation>Неможливо відкрити &quot;Projections.xml&quot;. Припинення обробки.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="345"/>
        <source>Low</source>
        <translation>прозорий</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="345"/>
        <source>High</source>
        <translation>напівпрозорий</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="345"/>
        <source>Opaque</source>
        <translation>непрозорий</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="457"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="461"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="465"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="469"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="473"/>
        <source>Directional Arrows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="477"/>
        <source>GPS</source>
        <translation>GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="481"/>
        <source>Layers</source>
        <translation>Шари</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="485"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="489"/>
        <source>Feature</source>
        <translation>Елемент</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="497"/>
        <source>Node</source>
        <translation>Точка</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="501"/>
        <source>Way</source>
        <translation>Шлях</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="505"/>
        <source>Relation</source>
        <translation>Відношення</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="509"/>
        <source>Tools</source>
        <translation>Інструменти</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="513"/>
        <source>Windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="517"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="694"/>
        <source>Clipboard invalid</source>
        <translation>Хибні дані буферу обміну</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="694"/>
        <source>Clipboard do not contain valid data.</source>
        <translation>Буфер обміну не містить відповідних даних.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="966"/>
        <source>Open track file</source>
        <translation>Відкрити трек файл</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="992"/>
        <location filename="../src/Sync/SyncOSM.cpp" line="43"/>
        <source>Unsaved changes</source>
        <translation>Не збережені зміни</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="993"/>
        <source>The current map contains unsaved changes that will be lost when starting a new one.
Do you want to cancel starting a new map or continue and discard the old changes?</source>
        <translation>Поточна мапа містить не збережені зміни, що будуть втрачені, якщо почати редагування нової мапи.
Чи бажаєте скасувати створення нової мапи або продовжити та втратити усі зміни?</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1021"/>
        <source>Waypoints</source>
        <translation>Точки дороги</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1041"/>
        <source>Track %1</source>
        <translation>Трек %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1084"/>
        <source>Big Fat Copyright Warning</source>
        <translation>Велике жирне попередження про Авторські права</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1085"/>
        <source>You are trying to import a KML file. Please be aware that:

 - You cannot import to OSM a KML file created from Google Earth. While you might
   think that nodes you created from GE are yours, they are not!
   They are still a derivative work from GE, and, as such, cannot be used in OSM.

 - If you downloaded it from the Internet, chances are that there is a copyright on it.
   Please be absolutely sure that using those data in OSM is permitted by the author, or
   that the data is public domain.

If unsure, please seek advice on the &quot;legal&quot; or &quot;talk&quot; openstreetmap mailing lists.

Are you absolutely sure this KML can legally be imported in OSM?</source>
        <translation>Ви намагаєтесь імпортувати KLM-файл. Зважте наступне:

 - Ви не можете імпортувати KLM-файли створені у Google Earth. Ви вважаєте,
   що точки створені з GE ваші, насправді це на так!
   Вони все одно лишаються похідними від GE і тому, чорт забирай, не можуть
   бути використані в OSM.

 - Якщо ви завантажили їх з Інтернет, є шанс, що вони захищені авторським
   правом. Будь ласка, переконайтесь, що у вас є дозвіл від автора на
   їх використання в OSM або, що вони є суспільним надбанням.

У разі невпевненості звертайтесь до оголошень у списках розсилки &quot;legal&quot; чи
   &quot;talk&quot; від openstreetmap.

Ви впевнені, що цей KLM-файл може буде імпортований до OSM на законних підставах?</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1147"/>
        <source>No valid file</source>
        <translation>Відповідні файли відсутні</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1147"/>
        <location filename="../src/MainWindow.cpp" line="2294"/>
        <source>%1 could not be opened.</source>
        <translation>%1 неможливо відкрити.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1290"/>
        <source>Open track files</source>
        <translation>Відкрити трек файли</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1302"/>
        <source>Old Qt version detected</source>
        <translation>Виявлено стару версію Qt</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1303"/>
        <source>Your setup uses Qt %1, which contains various known errors in uploading data to OpenStreetMap leading to 401 server response codes. Are you sure you want to continue (which is not recommended).
For more information see http://wiki.openstreetmap.org/index.php/Problem_uploading_with_Merkaartor</source>
        <translation>Ви використовуєте Qt версії %1, яка містить деякі відомі помилки надсилання даних до OpenStreetMap, починаючи з коду 401. Ви впевнені, що бажаєте продовжити (але ми не радимо вам це робити)?
За докладною інформацією звертайтесь до http://wiki.openstreetmap.org/index.php/Problem_uploading_with_Merkaartor</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1311"/>
        <source>Upload OSM</source>
        <translation>Надсилання даних OSM</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1311"/>
        <source>You don&apos;t seem to have specified your
OpenStreetMap username and password.
Do you want to do this now?</source>
        <translation>Ви не зазначили в налаштуваннях ваш
логін та пароль до OpenStreetMap.
Бажаєте зробити це зараз?</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1332"/>
        <location filename="../src/MainWindow.cpp" line="1346"/>
        <location filename="../src/MainWindow.cpp" line="1370"/>
        <source>Error downloading</source>
        <translation>Помилка завантаження</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1332"/>
        <location filename="../src/MainWindow.cpp" line="1346"/>
        <location filename="../src/MainWindow.cpp" line="1370"/>
        <source>The map could not be downloaded</source>
        <translation>Неможливо завантажити мапу</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1359"/>
        <source>Error downloading OpenStreetBugs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1359"/>
        <source>The OpenStreetBugs could not be downloaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1626"/>
        <source>Create Polygon</source>
        <translation>Створити багатокутник</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1626"/>
        <source>Specify the number of sides</source>
        <translation>Вкажіть кількість кутів</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1671"/>
        <source>Join Roads</source>
        <translation>Об’єднати дороги</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1685"/>
        <source>Split Roads</source>
        <translation>Розділити дороги</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1699"/>
        <source>Break Roads</source>
        <translation>Відокремити дороги</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1713"/>
        <source>Simplify Roads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1744"/>
        <source>Force Feature upload</source>
        <translation>Швидке надсилання елементів</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1769"/>
        <source>Network timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1769"/>
        <source>Cannot contact OpenStreetBugs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1779"/>
        <source>Error closing bug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1779"/>
        <source>Cannot delete bug. Server message is:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1786"/>
        <source>Create Junction</source>
        <translation>Створити перехрестя</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1790"/>
        <source>Multiple intersection.</source>
        <translation>Багатократне перетинання.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1791"/>
        <source>Those roads have multiple intersections.
Do you still want to create a junction for each one (Unwanted junctions can still be deleted afterhand)?</source>
        <translation>Ці дороги мають багатократне перетинання.
Ви все ще бажаєте створити перехрестя для кожного перетинання (небажані перехрестя після цього можна вилучити)?</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1808"/>
        <source>Add Street Numbers</source>
        <translation>Додати номер вулиці</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1826"/>
        <source>Number of segments to divide into</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1829"/>
        <source>Subdivide road into %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1844"/>
        <source>Align Nodes</source>
        <translation>Вирівняти точки</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1858"/>
        <source>Spread Nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1887"/>
        <source>Detach Node %1</source>
        <translation>Від’єднати точку %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1901"/>
        <source>Add member to relation</source>
        <translation>Додати елемент до відношення</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1913"/>
        <source>Remove member from relation</source>
        <translation>Вилучити елемент з відношення</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1925"/>
        <source>Split area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1940"/>
        <source>Terrace area into %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1970"/>
        <source>Create Relation %1</source>
        <translation>Створити Відношення %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2020"/>
        <source>Save map style</source>
        <translation>Зберегти стиль мапи</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2020"/>
        <location filename="../src/MainWindow.cpp" line="2031"/>
        <source>Merkaartor map style (*.mas)</source>
        <translation>Стиль мапи Merkaartor (*.mas)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2031"/>
        <source>Load map style</source>
        <translation>Завантажити стиль мапи</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2230"/>
        <source>Save Merkaartor document</source>
        <translation>Зберегти документ Merkaartor</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2230"/>
        <source>untitled</source>
        <translation>без назви</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2230"/>
        <source>Merkaartor documents Files (*.mdc)</source>
        <translation>Файли документів Merkaartor (*.mdc)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2273"/>
        <source>Unable to open save file</source>
        <translation>Неможливо відкрити збережений файл</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2273"/>
        <source>%1 could not be opened for writing.</source>
        <translation>%1 неможливо відкрити для запису.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2294"/>
        <location filename="../src/MainWindow.cpp" line="2300"/>
        <location filename="../src/MainWindow.cpp" line="2310"/>
        <source>Invalid file</source>
        <translation>Хибний файл</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2300"/>
        <source>%1 is not a valid XML file.</source>
        <translation>%1 — не є вірним XML файлом.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2310"/>
        <source>%1 is not a valid Merkaartor document.</source>
        <translation>%1 — не є вірним документом Merkaartor’а.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2375"/>
        <source>Export OSM</source>
        <translation>Експорт даних OSM</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2375"/>
        <source>OSM Files (*.osm)</source>
        <translation>Файли OSM (*.osm)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2398"/>
        <source>Export Binary OSM</source>
        <translation>Експортувати двійкові OSM-дані</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2398"/>
        <source>OSM Binary Files (*.osb)</source>
        <translation>Бінарні файли  OSM (*.osb)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2419"/>
        <source>Export osmChange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2419"/>
        <source>osmChange Files (*.osc)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2445"/>
        <source>Export GPX</source>
        <translation>Експорт GPX</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2445"/>
        <source>GPX Files (*.gpx)</source>
        <translation>Файли GPX (*.gpx)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2470"/>
        <source>Export KML</source>
        <translation>Експорт KML</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2470"/>
        <source>KML Files (*.kml)</source>
        <translation>Файли KML (*.kml)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2719"/>
        <source>%1 (int)</source>
        <translation>%1 (int)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2771"/>
        <source>Add Bookmark</source>
        <translation>Додати закладку</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2772"/>
        <source>Specify the name of the bookmark.</source>
        <translation>Вкажіть ім’я закладки.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2775"/>
        <source>Invalid bookmark name</source>
        <translation>Помилкове ім’я закладки</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2776"/>
        <source>Bookmark cannot be blank.</source>
        <translation>Закладка не може бути порожньою.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2780"/>
        <source>Warning: Bookmark name already exists</source>
        <translation>Попередження: закладка з таким ім’ям вже існує</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2781"/>
        <source>Enter a new one, keep the same to overwrite or cancel.</source>
        <translation>Введіть нове значення, лишить теж саме, щоб перезаписати або натисніть скасувати.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2819"/>
        <source>Remove Bookmark</source>
        <translation>Вилучити закладку</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2820"/>
        <source>Select the bookmark to remove.</source>
        <translation>Оберіть закладку для вилучення.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2882"/>
        <source>Invalid projection</source>
        <translation>Помилкова проекція</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2882"/>
        <source>Unable to set projection &quot;%1&quot;.</source>
        <translation>Неможливо встановити проекцію. &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2893"/>
        <source>Invalid Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="2893"/>
        <source>Unable to set filter &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="3028"/>
        <source>GPS error</source>
        <translation>помилка GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="3029"/>
        <source>Unable to open GPS port.</source>
        <translation>Неможливо відкрити порт GPS.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="3038"/>
        <source>Open NMEA log file</source>
        <translation>Відкрити файл журналу NMEA</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="3154"/>
        <source>Save Tag Templates</source>
        <translation>Зберегти шаблони теґів</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="3154"/>
        <source>Merkaartor tag templates (*.mat)</source>
        <translation>Шаблони теґів Merkaartor (*.mat)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="3166"/>
        <location filename="../src/MainWindow.cpp" line="3180"/>
        <source>Open Tag Templates</source>
        <translation>Відкрити шаблони теґів</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="3254"/>
        <source>Warning! Could not load the Merkaartor translations for the &quot;%1&quot; language. Switching to default english.</source>
        <translation>УВАГА! Неможливо завантажити переклад Merkaartor’а на мову — &quot;%1&quot;. Використовуємо типово англійську.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="14"/>
        <source>Merkaartor</source>
        <translation>Merkaartor</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="32"/>
        <source>&amp;Help</source>
        <translation>&amp;Довідка</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="38"/>
        <source>&amp;Create</source>
        <translation>&amp;Створити</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="51"/>
        <location filename="../src/MainWindow.ui" line="608"/>
        <source>&amp;Road</source>
        <translation>&amp;Дорога</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="66"/>
        <source>&amp;Edit</source>
        <translation>П&amp;равка</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="86"/>
        <source>&amp;View</source>
        <translation>&amp;Вид</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="90"/>
        <source>&amp;Bookmarks</source>
        <translation>&amp;Закладки</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="98"/>
        <source>Set &amp;projection</source>
        <translation>Встановити &amp;проекцію</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="287"/>
        <source>Show directional &amp;Arrows</source>
        <translation>Показувати &amp;вказівники напрямку</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="103"/>
        <source>Set Areas &amp;opacity</source>
        <translation>Встановити &amp;прозорість ділянок</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="127"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="131"/>
        <source>&amp;Export</source>
        <translation>&amp;Експорт</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="142"/>
        <source>Re&amp;cent open</source>
        <translation>Не&amp;щодавно відкриті</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="147"/>
        <source>Recen&amp;t import</source>
        <translation>Не&amp;щодавно імпортовані</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="171"/>
        <source>T&amp;ools</source>
        <translation>Інстру&amp;менти</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="175"/>
        <source>&amp;Style</source>
        <translation>&amp;Стиль</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="209"/>
        <location filename="../src/MainWindow.ui" line="626"/>
        <source>&amp;Node</source>
        <translation>&amp;Точка</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="218"/>
        <source>&amp;Window</source>
        <translation>В&amp;ікно</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="222"/>
        <location filename="../src/MainWindow.ui" line="904"/>
        <source>&amp;Docks</source>
        <translation>&amp;Панелі</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="241"/>
        <source>Fea&amp;ture</source>
        <translation>&amp;Елементи</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="493"/>
        <location filename="../src/MainWindow.ui" line="245"/>
        <source>OpenStreetBugs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="256"/>
        <location filename="../src/MainWindow.ui" line="929"/>
        <source>&amp;Layers</source>
        <translation>&amp;Шари</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="263"/>
        <source>&amp;Gps</source>
        <translation>&amp;GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="276"/>
        <source>Rel&amp;ation</source>
        <translation>Від&amp;ношення</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="283"/>
        <source>S&amp;how</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="378"/>
        <source>&amp;Quit</source>
        <translation>Ви&amp;хід</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="383"/>
        <source>&amp;About</source>
        <translation>Про &amp;Merkaartor</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="392"/>
        <source>&amp;Open</source>
        <translation>&amp;Відкрити</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="395"/>
        <source>Create a new document and import a file</source>
        <translation>Створити новий документ та імпортувати файл</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="398"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="407"/>
        <source>Zoom &amp;all</source>
        <translation>Показати &amp;все</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="410"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="415"/>
        <source>Zoom &amp;window</source>
        <translation>Змінити &amp;масштаб</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="418"/>
        <location filename="../src/MainWindow.ui" line="421"/>
        <source>Zoom window</source>
        <translation>Змінити масштаб</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="424"/>
        <source>F3</source>
        <translation>F3</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="433"/>
        <source>Zoom &amp;out</source>
        <translation>Від&amp;далити</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="436"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="445"/>
        <source>Zoom &amp;in</source>
        <translation>Набл&amp;изити</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="448"/>
        <location filename="../src/MainWindow.ui" line="451"/>
        <source>Zoom in</source>
        <translation>Наблизити</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="454"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="459"/>
        <location filename="../src/MainWindow.ui" line="462"/>
        <location filename="../src/MainWindow.ui" line="465"/>
        <source>Curved link</source>
        <translation>Звивисте з’єднання</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="477"/>
        <location filename="../src/MainWindow.ui" line="996"/>
        <source>&amp;Undo</source>
        <translation>&amp;Відміна</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="480"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="489"/>
        <source>&amp;Redo</source>
        <translation>&amp;Повернути</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="492"/>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="504"/>
        <source>&amp;Move</source>
        <translation>&amp;Пересунути</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="507"/>
        <source>Ctrl+M</source>
        <translation>Ctrl+M</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="516"/>
        <source>&amp;Import</source>
        <translation>&amp;Імпортувати</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="519"/>
        <source>Import a file into the current document</source>
        <translation>Імпортувати файл до поточного документу</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="528"/>
        <source>&amp;Download</source>
        <translation>&amp;Завантажити</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="531"/>
        <source>Download map data for a new area</source>
        <translation>Завантажити дані мапи для нової ділянки</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="534"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="539"/>
        <source>Link</source>
        <translation>Зв’язки</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="542"/>
        <location filename="../src/MainWindow.ui" line="545"/>
        <source>Create link</source>
        <translation>Створити зв’язок</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="548"/>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="560"/>
        <source>&amp;Select</source>
        <translation>&amp;Виділити</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="563"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="572"/>
        <source>&amp;Upload</source>
        <translation>&amp;Відправити</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="575"/>
        <source>Upload changes to the server</source>
        <translation>Відправити зміни до сервера</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="578"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="590"/>
        <source>R&amp;emove</source>
        <translation>Ви&amp;лучити</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="593"/>
        <source>Remove selected features</source>
        <translation>Вилучити виділені елементи</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="596"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="611"/>
        <source>Create new road</source>
        <translation>Створити нову дорогу</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="614"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="629"/>
        <source>Create new node</source>
        <translation>Створити нову точку</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="632"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="644"/>
        <source>&amp;Reverse</source>
        <translation>&amp;Змінити напрямок</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="647"/>
        <source>Reverse road direction</source>
        <translation>Змінити напрямок дороги</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="656"/>
        <source>&amp;Go To...</source>
        <translation>&amp;Перейти до…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="659"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="664"/>
        <source>&amp;Double carriage way</source>
        <translation>П&amp;одвійна дорога</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="667"/>
        <source>Create Double carriage way</source>
        <translation>Створити Подвійну дорогу</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="672"/>
        <source>&amp;Roundabout</source>
        <translation>&amp;Кільце</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="675"/>
        <source>Create Roundabout</source>
        <translation>Створити кільце</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="684"/>
        <source>&amp;New</source>
        <translation>&amp;Новий</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="687"/>
        <source>Create a new document</source>
        <translation>Створити новий документ</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="696"/>
        <source>&amp;Split</source>
        <translation>&amp;Розділити</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="699"/>
        <source>Split road into separate (connected) roads</source>
        <translation>Розділити дорогу на окремі (з’єднані) дороги</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="702"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="711"/>
        <source>&amp;Join</source>
        <translation>&amp;Приєднати</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="714"/>
        <source>Join connected roads to a single road</source>
        <translation>Об’єднати з’єднанні дороги у одну дорогу</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="717"/>
        <source>Alt+J</source>
        <translation>Alt+J</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="726"/>
        <source>&amp;Break apart</source>
        <translation>Від&amp;окремити</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="729"/>
        <source>Break</source>
        <translation>Відокремити</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="732"/>
        <source>Break apart connected roads</source>
        <translation>Відокремити з’єднанні дороги</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="735"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="740"/>
        <source>Re&amp;lation</source>
        <translation>&amp;Відношення</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="743"/>
        <source>Create Relation</source>
        <translation>Створити Відношення</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="755"/>
        <source>&amp;Area</source>
        <translation>Ді&amp;лянка</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="758"/>
        <source>Create new area</source>
        <translation>Створити нову ділянку</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="763"/>
        <source>&amp;Edit...</source>
        <translation>П&amp;равка…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="768"/>
        <location filename="../src/MainWindow.ui" line="1381"/>
        <source>&amp;Save...</source>
        <translation>&amp;Зберегти…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="773"/>
        <location filename="../src/MainWindow.ui" line="1371"/>
        <source>&amp;Load...</source>
        <translation>&amp;Завантажити…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="778"/>
        <source>&amp;Curved road</source>
        <translation>&amp;Звивиста дорога</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="787"/>
        <source>&amp;Preferences...</source>
        <translation>&amp;Параметри…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="792"/>
        <location filename="../src/MainWindow.ui" line="800"/>
        <source>&amp;All...</source>
        <translation>&amp;Все…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="795"/>
        <location filename="../src/MainWindow.ui" line="803"/>
        <source>Export all visible layers to a file</source>
        <translation>Експортувати усі видимі шари до файлу</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="812"/>
        <source>&amp;Find...</source>
        <translation>З&amp;найти…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="815"/>
        <source>Find</source>
        <translation>Пошук</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="818"/>
        <source>Find and select items</source>
        <translation>Знайти та вибрати об’єкти</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="823"/>
        <location filename="../src/MainWindow.ui" line="831"/>
        <source>&amp;Viewport...</source>
        <translation>&amp;Область перегляду…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="826"/>
        <location filename="../src/MainWindow.ui" line="834"/>
        <source>Export the features in the viewport to a file</source>
        <translation>Експортувати вміст області перегляду до файлу</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="839"/>
        <source>&amp;Add...</source>
        <translation>&amp;Додати…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="844"/>
        <source>&amp;Remove...</source>
        <translation>&amp;Вилучити…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="849"/>
        <source>&amp;Merge</source>
        <translation>З’&amp;єднати</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="852"/>
        <source>Node Merge</source>
        <translation>Об’єднання точок</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="855"/>
        <source>Merge the selected nodes (first selected will remain)</source>
        <translation>З’єднати вибрані точки (залишається перша вибрана точка)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="858"/>
        <source>Alt+M</source>
        <translation>Alt+M</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="863"/>
        <source>Save &amp;As...</source>
        <translation>Зберегти &amp;як…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="872"/>
        <source>&amp;Save</source>
        <translation>&amp;Зберегти</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="875"/>
        <source>Save to file</source>
        <translation>Зберегти у файл</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="878"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="887"/>
        <source>Download more</source>
        <translation>Завантажити ще</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="890"/>
        <source>Download more map data for the current area</source>
        <translation>Завантажити більше даних мапи для поточної ділянки</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="893"/>
        <location filename="../src/MainWindow.ui" line="896"/>
        <source>Download the current view to the previous download layer</source>
        <translation>Завантажити поточний вигляд до попередньо завантаженого шару</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="899"/>
        <source>Ctrl+Shift+D</source>
        <translation>Ctrl+Shift+D</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="912"/>
        <source>&amp;Properties</source>
        <translation>&amp;Властивості</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="915"/>
        <location filename="../src/MainWindow.ui" line="918"/>
        <source>Hide/Show the Properties dock</source>
        <translation>Сховати/Показати панель Властивостей</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="921"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="932"/>
        <location filename="../src/MainWindow.ui" line="935"/>
        <source>Hide/Show the Layers dock</source>
        <translation>Сховати/Показати панель Шарів</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="938"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="946"/>
        <source>&amp;Info</source>
        <translation>&amp;Інформація</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="949"/>
        <location filename="../src/MainWindow.ui" line="952"/>
        <source>Hide/Show the Info dock</source>
        <translation>Сховати/Показати панель Інформації</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="955"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="964"/>
        <source>&amp;Align</source>
        <translation>Ви&amp;рівняти</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="967"/>
        <source>Align nodes</source>
        <translation>Вирівняти точки</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="970"/>
        <source>Align selected nodes. First two selected give the line.</source>
        <translation>Вирівняти обрані точки. Перші дві обрані точки створюють лінію.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="973"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="982"/>
        <source>&amp;Spread</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="985"/>
        <source>Spread nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="988"/>
        <source>Align and spread selected nodes equally.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="999"/>
        <location filename="../src/MainWindow.ui" line="1002"/>
        <source>Hide/Show the Undo dock</source>
        <translation>Совати/Показати панель скасування змін</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1005"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1013"/>
        <source>Show &amp;downloaded areas</source>
        <translation>Показати &amp;завантажені ділянки</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1016"/>
        <source>Ctrl+Alt+A</source>
        <translation>Ctrl+Alt+A</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1025"/>
        <source>&amp;Copy</source>
        <translation>&amp;Копіювати</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1028"/>
        <source>Copy selected features and tags to the clipboard</source>
        <translation>Копіювати виділені елементи та теґи до буферу обміну</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1031"/>
        <source>Copy the selected feature&apos;s tags to the clipboard; if the feature is a trackpoint, copy the coordinates, too.</source>
        <translation>Копіювати теґи обраних елементів до буферу обміну, якщо елементом є точка треку — скопіювати також координати.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1034"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1039"/>
        <source>Paste Tags (&amp;Overwrite)</source>
        <translation>Вставити теґи (&amp;перезаписати)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1042"/>
        <source>Paste (and overwrite) the tags in the clipboard to the selected feature.</source>
        <translation>Вставити (та замінити) теґи з буферу обміну у виділені елементи..</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1045"/>
        <source>Ctrl+V, O</source>
        <translation>Ctrl+V, O</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1054"/>
        <source>Paste Tags (&amp;Merge)</source>
        <translation>Вставити теґи (&amp;об’єднати)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1057"/>
        <source>Paste tags</source>
        <translation>Вставити теґи</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1060"/>
        <source>Paste tags from the clipboard (Merge with existing tags)</source>
        <translation>Вставити теґи з буферу обміну (Об’єднати з існуючими теґами)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1063"/>
        <source>Merge the tags in the clipboard with the ones of the selected feature.</source>
        <translation>Об’єднати теґи з буферу обміну з теґами виділених елементів..</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1066"/>
        <source>Ctrl+V, M</source>
        <translation>Ctrl+V, M</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1071"/>
        <location filename="../src/MainWindow.ui" line="1076"/>
        <source>Selected...</source>
        <translation>Виділені…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1085"/>
        <source>Paste Features</source>
        <translation>Вставити Елемент</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1088"/>
        <source>Paste</source>
        <translation>Вставити</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1091"/>
        <source>Paste features from the clipboard</source>
        <translation>Вставити елементи з буферу обміну</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1094"/>
        <source>Paste the features in the clipboard; If the features&apos;id are already in the document, overwrite them.</source>
        <translation>Вставити елемент з буферу обміну; якщо ідентифікатор елементу вже присутній у документі — переписати його.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1097"/>
        <location filename="../src/MainWindow.ui" line="1540"/>
        <source>Ctrl+V, F</source>
        <translation>Ctrl+V, F</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1105"/>
        <source>OSM (XML)</source>
        <translation>OSM (XML)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1110"/>
        <source>OSM (Binary)</source>
        <translation>OSM (двійковий)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1115"/>
        <source>&amp;Force Upload</source>
        <translation>&amp;Швидке надсилання</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1118"/>
        <source>Commit feature to the dirty layer</source>
        <translation>Перенести елементи на шар-чернетку</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1121"/>
        <location filename="../src/MainWindow.ui" line="1124"/>
        <source>Commit the selected feature from a non-uploadable layer (e.g.Track or Extract) to the dirty layer, ready for upload</source>
        <translation>Перенести виділені елементи з шару, що не надсилається (наприклад, Треки) до шару-чернетки, готового до надсилання</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1129"/>
        <source>GPX</source>
        <translation>GPX</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1134"/>
        <source>KML</source>
        <translation>KML</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1139"/>
        <source>Toggle Toolbar</source>
        <translation>Панель інструментів</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1142"/>
        <location filename="../src/MainWindow.ui" line="1145"/>
        <source>Hide/Show the Toolbar</source>
        <translation>Сховати/Показати панель Інструментів</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1150"/>
        <source>Hide All</source>
        <translation>Сховати все</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1153"/>
        <location filename="../src/MainWindow.ui" line="1161"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1158"/>
        <source>Show All</source>
        <translation>Показати все</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1169"/>
        <source>&amp;Image layer</source>
        <translation>Шар &amp;зображень</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1182"/>
        <source>Show &amp;nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1193"/>
        <source>Show na&amp;mes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1469"/>
        <source>&amp;WMS Servers Editor...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1474"/>
        <source>&amp;TMS Servers Editor...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1562"/>
        <source>&amp;Subdivide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1565"/>
        <source>Subdivide segment equally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1568"/>
        <source>Subdivide a selected way segment (the way and two adjacent nodes) into segments of equal length.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1630"/>
        <source>S&amp;implify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1633"/>
        <source>Simplify road(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1636"/>
        <source>Simplify way by removing unnecessary child nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1641"/>
        <source>&amp;Filters Editor...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1649"/>
        <source>&amp;None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1658"/>
        <source>&amp;Split Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1661"/>
        <source>Split area between two nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1664"/>
        <source>Split a selected area between two selected nodes into two separates areas.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1673"/>
        <source>&amp;Terrace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1676"/>
        <source>Terrace area into residences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1679"/>
        <source>Split a selected area into terraced residences.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1684"/>
        <source>Toolbar Editor...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1174"/>
        <source>&amp;Raster/SVG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="108"/>
        <source>&amp;Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="185"/>
        <source>Ta&amp;g templates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="331"/>
        <source>Main toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1185"/>
        <source>Ctrl+Alt+P</source>
        <translation>Ctrl+Alt+P</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1196"/>
        <source>Ctrl+Alt+N</source>
        <translation>Ctrl+Alt+N</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1201"/>
        <source>&amp;Start</source>
        <translation>&amp;Старт</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1204"/>
        <source>Start GPS</source>
        <translation>Розпочати роботу з GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1209"/>
        <source>&amp;Replay...</source>
        <translation>По&amp;втор…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1212"/>
        <source>Replay GPS</source>
        <translation>Відтворити інформацію GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1220"/>
        <source>&amp;GPS</source>
        <translation>&amp;GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1223"/>
        <location filename="../src/MainWindow.ui" line="1226"/>
        <source>Hide/Show the GPS dock</source>
        <translation>Сховати/Показати панель GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1229"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1237"/>
        <source>S&amp;top</source>
        <translation>С&amp;топ</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1240"/>
        <source>Stop GPS</source>
        <translation>Зупинити роботу із GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1248"/>
        <source>&amp;Center on GPS</source>
        <translation>&amp;Центрувати за GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1256"/>
        <source>Show track &amp;segments</source>
        <translation>Показати &amp;сегменти треку</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1259"/>
        <source>Ctrl+Alt+T</source>
        <translation>Ctrl+Alt+T</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1267"/>
        <source>Show &amp;scale</source>
        <translation>Показати &amp;масштаб</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1270"/>
        <source>Ctrl+Alt+S</source>
        <translation>Ctrl+Alt+S</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1278"/>
        <source>Show &amp;relations</source>
        <translation>Показати &amp;відношення</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1281"/>
        <source>Ctrl+Alt+R</source>
        <translation>Ctrl+Alt+R</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1289"/>
        <source>Show roads background</source>
        <translation>Показати фон доріг</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1297"/>
        <source>Show roads boundary</source>
        <translation>Показати межі доріг</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1305"/>
        <source>Show touchup</source>
        <translation>Показати підказки</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1316"/>
        <location filename="../src/MainWindow.ui" line="1319"/>
        <source>Record</source>
        <translation>Запис</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1322"/>
        <source>Record GPS</source>
        <translation>Записати інформацію з GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1333"/>
        <source>Pause</source>
        <translation>Пауза</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1336"/>
        <source>Pause GPS</source>
        <translation>Призупинити запис інформації з GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1344"/>
        <source>G&amp;eoImage</source>
        <translation>G&amp;eoImage</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1347"/>
        <location filename="../src/MainWindow.ui" line="1350"/>
        <source>Hide/Show the GeoImage dock</source>
        <translation>Сховати/Показати панель GeoImage</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1353"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1358"/>
        <source>World OSB manager...</source>
        <translation>Менеджер OSB World…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1366"/>
        <source>&amp;Shortcut Editor...</source>
        <translation>&amp;Комбінації клавіш…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1376"/>
        <source>&amp;Merge...</source>
        <translation>З’&amp;єднати…</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1386"/>
        <source>&amp;Add member</source>
        <translation>&amp;Додати елемент</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1391"/>
        <source>&amp;Remove member</source>
        <translation>&amp;Вилучити елемент</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1399"/>
        <source>&amp;Never</source>
        <translation>&amp;ніколи</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1407"/>
        <source>for &amp;Oneway roads</source>
        <translation>для &amp;односторонніх доріг</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1415"/>
        <source>&amp;Always</source>
        <translation>&amp;завжди</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1424"/>
        <source>&amp;Detach</source>
        <translation>&amp;Від’єднати</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1427"/>
        <source>Detach node from a road</source>
        <translation>Від’єднати точку від дороги</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1430"/>
        <source>Detach a node from a Road</source>
        <translation>Від’єднати точку від дороги</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1442"/>
        <source>&amp;Work Offline</source>
        <translation>&amp;Працювати оффлайн</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1447"/>
        <source>SVG</source>
        <translation>Векторний малюнок (SVG)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1455"/>
        <source>&amp;Styles</source>
        <translation>&amp;Стилі</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1458"/>
        <location filename="../src/MainWindow.ui" line="1461"/>
        <source>Hide/Show the Styles dock</source>
        <translation>Сховати/Показати панель Стилів</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1464"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1479"/>
        <source>&amp;Reset Discardable dialogs status</source>
        <translation>&amp;Перевстановити стан діалогу Discardable</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1488"/>
        <source>GPS Menu</source>
        <translation>Меню GPS</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1497"/>
        <source>Camera</source>
        <translation>Камера</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1502"/>
        <source>Create &amp;Junction</source>
        <translation>Створити &amp;перехрестя</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1514"/>
        <source>Rotate</source>
        <translation>Обертання</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1517"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1522"/>
        <source>Polygon</source>
        <translation>Багатокутник</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1527"/>
        <source>Rectangle</source>
        <translation>Прямокутник</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1532"/>
        <source>Add new Image layer</source>
        <translation>Додати новий шар зображення</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1537"/>
        <source>Paste Feature(s)</source>
        <translation>Вставити елемент(и)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1548"/>
        <source>Features</source>
        <translation>Елементи</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1553"/>
        <source>Add street &amp;numbers (Karlsruhe scheme)</source>
        <translation>&amp;Додати номер вулиці (схема Карлсрує)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1576"/>
        <source>Show &amp;virtual nodes</source>
        <translation>Показувати в&amp;ірутальні точки</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1584"/>
        <source>Show lat/lon &amp;grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1592"/>
        <source>&amp;Lock zoom to tiled background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1597"/>
        <source>&amp;Projections Editor...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1605"/>
        <source>Show &amp;Photos on map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1610"/>
        <source>OsmChange (OSC)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1615"/>
        <source>Force Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1620"/>
        <source>Download OpenStreet&amp;Bugs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.ui" line="1625"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportGPX.cpp" line="290"/>
        <source>Import GPX</source>
        <translation>Імпорт GPX</translation>
    </message>
    <message>
        <location filename="../src/ImportExport/ImportNGT.cpp" line="19"/>
        <source>Import NGT</source>
        <translation>Імпорт NGT</translation>
    </message>
    <message>
        <location filename="../src/Features/Relation.cpp" line="753"/>
        <source>Relation Modified %1</source>
        <translation>Відношення %1 змінено</translation>
    </message>
    <message>
        <location filename="../src/Features/Way.cpp" line="766"/>
        <source>Delete Children.</source>
        <translation>Вилучити нащадків.</translation>
    </message>
    <message>
        <location filename="../src/Features/Way.cpp" line="767"/>
        <source>Do you want to delete the children nodes also?</source>
        <translation>Бажаєте також вилучити точки-нащадки?</translation>
    </message>
    <message>
        <location filename="../src/Sync/SyncOSM.cpp" line="17"/>
        <source>Unresolved conflicts</source>
        <translation>Нерозв’язувані конфлікти</translation>
    </message>
    <message>
        <location filename="../src/Sync/SyncOSM.cpp" line="17"/>
        <source>Please resolve existing conflicts first</source>
        <translation>Будь ласка, розв’яжіть поточні конфлікти</translation>
    </message>
    <message>
        <location filename="../src/Sync/SyncOSM.cpp" line="44"/>
        <source>It is strongly recommended to save the changes to your document after an upload.
Do you want to do this now?</source>
        <translation>Ми радимо зберегти зміни у вашому документі після його відправи.
Бажаєте зробити це зараз?</translation>
    </message>
    <message>
        <location filename="../src/TagModel.cpp" line="131"/>
        <location filename="../src/TagModel.cpp" line="162"/>
        <source>Set Tags on multiple features</source>
        <translation>Встановити теґи для кількох елементів</translation>
    </message>
    <message>
        <location filename="../src/TagModel.cpp" line="133"/>
        <location filename="../src/TagModel.cpp" line="164"/>
        <source>Set Tags on %1</source>
        <translation>Встановити теґ для %1</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="711"/>
        <source>Clear %1 tags on %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="828"/>
        <location filename="../src/Docks/PropertiesDock.cpp" line="873"/>
        <source>Reorder members in relation %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MapFeature</name>
    <message>
        <location filename="../src/Features/Feature.cpp" line="913"/>
        <source>&lt;i&gt;V: &lt;/i&gt;&lt;b&gt;%1&lt;/b&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Features/Feature.cpp" line="915"/>
        <source>&lt;i&gt;last: &lt;/i&gt;&lt;b&gt;%1&lt;/b&gt; by &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>&lt;i&gt;ост.: &lt;/i&gt;&lt;b&gt;%1&lt;/b&gt; от &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/Features/Feature.cpp" line="917"/>
        <source>&lt;i&gt;last: &lt;/i&gt;&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>&lt;i&gt;ост.: &lt;/i&gt;&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/Features/Feature.cpp" line="920"/>
        <source>&lt;br/&gt;&lt;i&gt;layer: &lt;/i&gt;&lt;b&gt;%1&lt;/b&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Features/Feature.cpp" line="927"/>
        <source>History</source>
        <translation>Історія</translation>
    </message>
    <message>
        <location filename="../src/Features/Feature.cpp" line="930"/>
        <source>Referenced by ways</source>
        <translation>Належить до лінії</translation>
    </message>
    <message>
        <location filename="../src/Features/Feature.cpp" line="933"/>
        <source>Referenced by relation</source>
        <translation>Належить до відношення</translation>
    </message>
    <message>
        <location filename="../src/Features/Relation.cpp" line="570"/>
        <source>size</source>
        <translation>розмір</translation>
    </message>
    <message>
        <location filename="../src/Features/Relation.cpp" line="570"/>
        <source>members</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Features/Relation.cpp" line="573"/>
        <location filename="../src/Features/Way.cpp" line="1060"/>
        <source>Topleft</source>
        <translation>Верхній лівий кут</translation>
    </message>
    <message>
        <location filename="../src/Features/Relation.cpp" line="575"/>
        <location filename="../src/Features/Way.cpp" line="1062"/>
        <source>Botright</source>
        <translation>Правий нижній кут</translation>
    </message>
    <message>
        <location filename="../src/Features/Relation.cpp" line="577"/>
        <source>Relation</source>
        <translation>Відношення</translation>
    </message>
    <message>
        <location filename="../src/Features/Relation.cpp" line="732"/>
        <source>Role</source>
        <translation>Роль</translation>
    </message>
    <message>
        <location filename="../src/Features/Relation.cpp" line="734"/>
        <source>Member</source>
        <translation>Член</translation>
    </message>
    <message>
        <location filename="../src/Features/Way.cpp" line="1055"/>
        <source>Length</source>
        <translation>Довжина</translation>
    </message>
    <message>
        <location filename="../src/Features/Way.cpp" line="1057"/>
        <source>Size</source>
        <translation>Розмір</translation>
    </message>
    <message>
        <location filename="../src/Features/Way.cpp" line="1057"/>
        <source>%1 nodes</source>
        <translation>точок - %1</translation>
    </message>
    <message>
        <location filename="../src/Features/Way.cpp" line="1064"/>
        <source>Area</source>
        <translation>Ділянка</translation>
    </message>
    <message>
        <location filename="../src/Features/Way.cpp" line="1064"/>
        <source>Way</source>
        <translation>Шлях</translation>
    </message>
    <message>
        <location filename="../src/Features/Node.cpp" line="608"/>
        <source>coord</source>
        <translation>коорд</translation>
    </message>
    <message>
        <location filename="../src/Features/Node.cpp" line="611"/>
        <source>elevation</source>
        <translation>висота</translation>
    </message>
    <message>
        <location filename="../src/Features/Node.cpp" line="613"/>
        <source>speed</source>
        <translation>швидкість</translation>
    </message>
    <message>
        <location filename="../src/Features/Node.cpp" line="615"/>
        <location filename="../src/Features/Node.cpp" line="623"/>
        <source>description</source>
        <translation>опис</translation>
    </message>
    <message>
        <location filename="../src/Features/Node.cpp" line="617"/>
        <location filename="../src/Features/Node.cpp" line="626"/>
        <source>comment</source>
        <translation>коментар</translation>
    </message>
    <message>
        <location filename="../src/Features/Node.cpp" line="620"/>
        <source>Waypoint</source>
        <translation>Точки дороги</translation>
    </message>
    <message>
        <location filename="../src/Features/Node.cpp" line="629"/>
        <source>Node</source>
        <translation>Точка</translation>
    </message>
</context>
<context>
    <name>MapView</name>
    <message>
        <location filename="../src/MapView.cpp" line="250"/>
        <source>%1ms;ppm:%2</source>
        <translation>%1мс;ppm:%2</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="279"/>
        <source>%1 m</source>
        <translation>%1 м</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="281"/>
        <source>%1 km</source>
        <translation>%1 км</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="678"/>
        <source>Feature</source>
        <translation>Елемент</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="687"/>
        <source>Node</source>
        <translation>Точка</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="695"/>
        <source>Road</source>
        <translation>Дорога</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="703"/>
        <source>Relation</source>
        <translation>Відношення</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="812"/>
        <source>Load image</source>
        <translation>Завантажити зображення</translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="814"/>
        <source>Add node position to image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="816"/>
        <source>Geotag image with this position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MapView.cpp" line="818"/>
        <source>Cancel</source>
        <translation>Відміна</translation>
    </message>
</context>
<context>
    <name>MerkaartorPreferences</name>
    <message>
        <location filename="../src/Preferences/BookmarksList.cpp" line="20"/>
        <location filename="../src/Preferences/BookmarksList.cpp" line="27"/>
        <source>New Bookmark</source>
        <translation>Нова Закладка</translation>
    </message>
    <message>
        <location filename="../src/Preferences/MerkaartorPreferences.cpp" line="403"/>
        <location filename="../src/Preferences/MerkaartorPreferences.cpp" line="406"/>
        <source>Preferences upload failed</source>
        <translation>Невдача завантаження параметрів</translation>
    </message>
    <message>
        <location filename="../src/Preferences/MerkaartorPreferences.cpp" line="403"/>
        <source>Duplicate key</source>
        <translation>Дублювати ключ</translation>
    </message>
    <message>
        <location filename="../src/Preferences/MerkaartorPreferences.cpp" line="406"/>
        <source>More than 150 preferences</source>
        <translation>Більше ніж 150 параметрів</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TmsServersList.cpp" line="20"/>
        <location filename="../src/Preferences/TmsServersList.cpp" line="36"/>
        <location filename="../src/Preferences/WmsServersList.cpp" line="20"/>
        <location filename="../src/Preferences/WmsServersList.cpp" line="43"/>
        <source>New Server</source>
        <translation>Новий сервер</translation>
    </message>
</context>
<context>
    <name>MultiProperties</name>
    <message>
        <location filename="../src/MultiProperties.ui" line="13"/>
        <source>Form</source>
        <translation>Подвійна дорога</translation>
    </message>
    <message>
        <location filename="../src/MultiProperties.ui" line="33"/>
        <source>Tags</source>
        <translation>Теґи</translation>
    </message>
    <message>
        <location filename="../src/MultiProperties.ui" line="53"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/MultiProperties.ui" line="73"/>
        <source>Selected items</source>
        <translation>Обрані елементи</translation>
    </message>
</context>
<context>
    <name>NameFinder::NameFinderTableModel</name>
    <message>
        <location filename="../src/NameFinder/namefindertablemodel.cpp" line="68"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/NameFinder/namefindertablemodel.cpp" line="71"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../src/NameFinder/namefindertablemodel.cpp" line="74"/>
        <source>Near</source>
        <translation>Біля</translation>
    </message>
    <message>
        <location filename="../src/NameFinder/namefindertablemodel.cpp" line="77"/>
        <source>Unknown field</source>
        <translation>Невідоме поле</translation>
    </message>
</context>
<context>
    <name>NameFinder::NameFinderWidget</name>
    <message>
        <location filename="../src/NameFinder/namefinderwidget.cpp" line="89"/>
        <source>Error!</source>
        <translation>Помилка!</translation>
    </message>
    <message>
        <location filename="../src/NameFinder/namefinderwidget.cpp" line="93"/>
        <source>Name finder service host not found.</source>
        <translation>Сервер сервісу пошуку назв не знайдено.</translation>
    </message>
    <message>
        <location filename="../src/NameFinder/namefinderwidget.cpp" line="96"/>
        <source>Name finder service host refused connection.</source>
        <translation>Сервер сервісу пошуку назв відмовив у з’єднанні.</translation>
    </message>
    <message>
        <location filename="../src/NameFinder/namefinderwidget.cpp" line="99"/>
        <source>Name finder service requires authentication.</source>
        <translation>Сервер сервісу пошуку назв потребує автентифікації.</translation>
    </message>
    <message>
        <location filename="../src/NameFinder/namefinderwidget.cpp" line="101"/>
        <source>Unknown error.</source>
        <translation>Невідома помилка.</translation>
    </message>
</context>
<context>
    <name>NameFinderWidgetUi</name>
    <message>
        <location filename="../src/NameFinder/namefinderwidget.ui" line="13"/>
        <source>Form</source>
        <translation>Подвійна дорога</translation>
    </message>
</context>
<context>
    <name>NativeRenderDialog</name>
    <message>
        <location filename="../src/Render/NativeRenderDialog.cpp" line="36"/>
        <source>Proceed...</source>
        <translation>Приступаю…</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.cpp" line="133"/>
        <source>Working. Please Wait...</source>
        <translation>Працюю. Зачекайте…</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.cpp" line="133"/>
        <source>Cancel</source>
        <translation>Відміна</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.cpp" line="159"/>
        <source>SVG rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.cpp" line="162"/>
        <source>Raster rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="26"/>
        <source>SVG</source>
        <translation>Векторний малюнок (SVG)</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="14"/>
        <source>Raster/SVG export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="20"/>
        <source>Export type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="33"/>
        <source>Raster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="43"/>
        <source>Export options</source>
        <translation>Опції експорту</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="67"/>
        <source>min lat/Lon</source>
        <translation>мін дов/шир</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="134"/>
        <source>max lat/Lon</source>
        <translation>макс дов/шир</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="201"/>
        <source>Show Scale</source>
        <translation>Показати Масштаб</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="214"/>
        <source>Show Grid</source>
        <translation>Показувати Сітку</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="227"/>
        <source>Show Borders</source>
        <translation>Показувати Межі</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="240"/>
        <source>Show License</source>
        <translation>Показувати Ліцензію</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="256"/>
        <source>Width x Height (px)</source>
        <translation>Ширина х Висота (пікс)</translation>
    </message>
    <message>
        <location filename="../src/Render/NativeRenderDialog.ui" line="279"/>
        <source>X</source>
        <translation>Х</translation>
    </message>
</context>
<context>
    <name>OsbLayer</name>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="1496"/>
        <source># of loaded Regions</source>
        <translation>Завантажено ділянок — #</translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="1496"/>
        <location filename="../src/Layers/Layer.cpp" line="1497"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="1497"/>
        <source># of loaded Tiles</source>
        <translation>завантажено квадратів — #</translation>
    </message>
</context>
<context>
    <name>OsbLayerWidget</name>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="552"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
</context>
<context>
    <name>PaintStyleEditor</name>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="45"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="46"/>
        <source>Always</source>
        <translation>Завжди</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="235"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="262"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="306"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="374"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="442"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="532"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.cpp" line="573"/>
        <source>Select Color</source>
        <translation>Виберіть колір</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="13"/>
        <source>Map style editor</source>
        <translation>Редактор стилів мапи</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="19"/>
        <source>Global</source>
        <translation>Загальні параметри</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="33"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="296"/>
        <source>Background</source>
        <translation>Фон</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="49"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="129"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="145"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="326"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="521"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="595"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="748"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1074"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1319"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="86"/>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="93"/>
        <source>Remove</source>
        <translation>Вилучити</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="100"/>
        <source>Duplicate</source>
        <translation>Дублювати</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="177"/>
        <source>Tag selection</source>
        <translation>Вибір теґу</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="197"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="253"/>
        <source>meter/pixel</source>
        <translation>метрів на піксель</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="223"/>
        <source>Visible up to</source>
        <translation>Показувати при</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="230"/>
        <source>Visible from scale</source>
        <translation>Показувати при масштабі</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="310"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="579"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1058"/>
        <source>Draw with color</source>
        <translation>Креслити кольором</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="372"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="641"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="794"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="967"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1168"/>
        <source>Proportional thickness</source>
        <translation>Пропорційна товщина</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="392"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="661"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="814"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="987"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1188"/>
        <source>Fixed thickness</source>
        <translation>Фіксована товщина</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="454"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="876"/>
        <source>Dashed</source>
        <translation>Пунктир</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="468"/>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="890"/>
        <source>off</source>
        <translation>проміжок</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="505"/>
        <source>Fill area</source>
        <translation>Залити ділянку кольором</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="565"/>
        <source>Boundary</source>
        <translation>Межі</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="718"/>
        <source>Touchup</source>
        <translation>Підказки</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="732"/>
        <source>Draw steps</source>
        <translation>Креслити кроки</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="927"/>
        <source>Draw icon</source>
        <translation>Показати піктограму</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1044"/>
        <source>Label</source>
        <translation>Мітки</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1081"/>
        <source>Font</source>
        <translation>Шрифт</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1128"/>
        <source>Label tag</source>
        <translation>Теґ мітки</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1244"/>
        <source>Halo</source>
        <translation>Ореол</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1254"/>
        <source>Area</source>
        <translation>Ділянка</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1303"/>
        <source>Draw with background color</source>
        <translation>Залити фон кольором</translation>
    </message>
    <message>
        <location filename="../src/PaintStyle/PaintStyleEditor.ui" line="1359"/>
        <source>Label with background tag</source>
        <translation>Мітка з позначкою фону</translation>
    </message>
</context>
<context>
    <name>PhotoLoadErrorDialog</name>
    <message>
        <location filename="../src/Docks/PhotoLoadErrorDialog.ui" line="14"/>
        <source>No Valid geotag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/PhotoLoadErrorDialog.ui" line="20"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../src/Docks/PhotoLoadErrorDialog.ui" line="27"/>
        <source>This photo do not contain geo-tagging EXIF data.
What do you want to do next?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/PhotoLoadErrorDialog.ui" line="44"/>
        <source>Try to match with a track node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/PhotoLoadErrorDialog.ui" line="63"/>
        <source>Load without associated node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/PhotoLoadErrorDialog.ui" line="73"/>
        <source>Extract info from barcode (Walking Papers)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/PhotoLoadErrorDialog.ui" line="89"/>
        <source>Ignore it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Docks/PhotoLoadErrorDialog.ui" line="99"/>
        <source>Do this for all current photos.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PictureViewerDialog</name>
    <message>
        <location filename="../src/Utils/PictureViewerDialog.cpp" line="39"/>
        <location filename="../src/Utils/PictureViewerDialog.cpp" line="45"/>
        <source>Output filename</source>
        <translation>Ім’я вихідного файлу</translation>
    </message>
    <message>
        <location filename="../src/Utils/PictureViewerDialog.cpp" line="39"/>
        <source>SVG files (*.svg)</source>
        <translation>Файли SVG (*.svg)</translation>
    </message>
    <message>
        <location filename="../src/Utils/PictureViewerDialog.cpp" line="45"/>
        <source>Image files (*.png *.jpg)</source>
        <translation>Файли зображень (*.png *.jpg)</translation>
    </message>
    <message>
        <location filename="../src/Utils/PictureViewerDialog.ui" line="13"/>
        <source>Dialog</source>
        <translation>Надсилання змін</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="108"/>
        <source>English</source>
        <translation>English — Англійська</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="109"/>
        <source>Arabic</source>
        <translation>Arabic — Арабська</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="110"/>
        <source>Czech</source>
        <translation>Czech — Чеська</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="112"/>
        <source>German</source>
        <translation>German — Німецька</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="113"/>
        <source>French</source>
        <translation>French — Французька</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="114"/>
        <source>Italian</source>
        <translation>Italian — Італійська</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="116"/>
        <source>Polish</source>
        <translation>Polish — Польська</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="118"/>
        <source>Brazilian Portuguese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="119"/>
        <source>Russian</source>
        <translation>Russian — Російська</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="378"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="590"/>
        <source>Custom styles directory</source>
        <translation>Тека власних стилів</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="387"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="676"/>
        <source>Tag Template</source>
        <translation>Шаблон теґів</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="387"/>
        <source>Merkaartor tag template (*.mat)</source>
        <translation>Шаблон теґів Merkaartor (*.mat)</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="396"/>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="409"/>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="422"/>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="435"/>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="448"/>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="460"/>
        <source>Select Color</source>
        <translation>Виберіть колір</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="475"/>
        <source>Tool already exists</source>
        <translation>Інструмент вже існує</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="476"/>
        <source>A tool of this name already exists.
Please select another name or click the &lt;Apply&gt; button if you want to modify the existing one</source>
        <translation>Інструмент з таким іменем вже Існує.
Оберіть інше ім’я, або натисніть &lt;Apply&gt;, якщо бажаєте змінити існуючий</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="495"/>
        <source>Cannot delete preset tool</source>
        <translation>Неможливо вилучити предналаштований інструмент</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="496"/>
        <source>Cannot delete preset tool &quot;%1&quot;</source>
        <translation>Неможливо вилучити предналаштований інструмент &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="514"/>
        <source>Cannot modify preset tool name</source>
        <translation>Неможливо змінити предналаштований інструмент</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="515"/>
        <source>Cannot modify preset tool &quot;%1&quot;&apos;s name</source>
        <translation>Неможливо змінити предналаштований інструмент &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="540"/>
        <source>Select tool executable</source>
        <translation>Вкажіть виконавчий файл інструмента</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="548"/>
        <source>Select Log directory</source>
        <translation>Вкажіть теку для журналів</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="15"/>
        <source>Preferences</source>
        <translation>Параметри</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="505"/>
        <source>Locale</source>
        <translation>Мова</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="511"/>
        <source>You may need to restart the program for these changes to take effect</source>
        <translation>Потрібно перезапустити програму, щоб зміни набули сили</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="520"/>
        <source>Use language</source>
        <translation>Використовувати мову</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="555"/>
        <source>Translate standard tags</source>
        <translation>Перекладати стандартні теґи</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="28"/>
        <source>Visual</source>
        <translation>Вигляд</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="34"/>
        <source>General</source>
        <translation>Загальний</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="42"/>
        <source>Zoom Out/in (%)</source>
        <translation>Масштаб -/+ (%)</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="66"/>
        <source>Opacity low/high</source>
        <translation>Непрозорість мін/макс</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="97"/>
        <source>Separate Move mode</source>
        <translation>Окремий режим для переміщення</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="104"/>
        <source>Single mouse button interaction</source>
        <translation>Миша з однією кнопкою</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="111"/>
        <source>Use custom Qt style</source>
        <translation>Власний стиль Qt</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="125"/>
        <source>Use Virtual nodes (new session required)</source>
        <translation>Використовувати віртуальні точки (потрібно створити новий сеанс)</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="158"/>
        <source>Colors</source>
        <translation>Кольори</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="437"/>
        <source>Background</source>
        <translation>Фон</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="175"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="230"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="276"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="322"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="375"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="459"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="613"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="731"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1021"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1317"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="188"/>
        <source>Overwrite style</source>
        <translation>Замінити стиль</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="360"/>
        <source>Hover</source>
        <translation>Під вказівником</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="246"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="292"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="338"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="391"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="475"/>
        <source>Pixels</source>
        <translation>Пікселів</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="427"/>
        <source>Highlight</source>
        <translation>Підсвічування</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="420"/>
        <source>Focus</source>
        <translation>Обране</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="413"/>
        <source>Relations</source>
        <translation>Відношення</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="444"/>
        <source>GPX track</source>
        <translation>Треки GPX</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="482"/>
        <source>Use simple GPX track appearance</source>
        <translation>Простий вигляд треків GPX</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="576"/>
        <source>Style</source>
        <translation>Стиль</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="582"/>
        <source>Map style</source>
        <translation>Стиль мапи</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="624"/>
        <source>Current style</source>
        <translation>Поточний стиль</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="646"/>
        <source>Disable styles for track layers</source>
        <translation>Не використовувати стилі для шарів з треками</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="670"/>
        <source>Template</source>
        <translation>Шаблони</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="684"/>
        <source>Built-in</source>
        <translation>Вбудований</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="708"/>
        <source>Custom</source>
        <translation>Власний</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="757"/>
        <source>Data</source>
        <translation>Дані</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="769"/>
        <source>OSM</source>
        <translation>OSM</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="777"/>
        <source>Website:</source>
        <translation>Сайт:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="787"/>
        <source>Username:</source>
        <translation>Логін:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="797"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1079"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="816"/>
        <source>Documents</source>
        <translation>Документи</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="822"/>
        <source>Autosave documents after upload</source>
        <translation>Автоматично зберігати документ після надсилання</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="832"/>
        <source>Tracks</source>
        <translation>Треки</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="840"/>
        <source>Automatically extract tracks on open</source>
        <translation>Видобувати треки під час відкриття</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="847"/>
        <source>Track layers readonly by default</source>
        <translation>Шар треків тільки для читання</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="858"/>
        <source>Don&apos;t connect GPX nodes separated by more than (in km; 0 to disable)</source>
        <translation>Не з’єднувати точки GPX на відстані більше (в км, 0 - з’єднувати)</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="891"/>
        <source>GPS</source>
        <translation>GPS</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="897"/>
        <source>GPS input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="903"/>
        <source>gpsd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="910"/>
        <source>Serial</source>
        <translation>Послідовний</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="932"/>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="970"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="960"/>
        <source>Host</source>
        <translation>Хост</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="998"/>
        <source>Save NMEA log</source>
        <translation>Зберегти журнал NMEA</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1039"/>
        <source>Set system time to GPS</source>
        <translation>Встановити системний час по GPS</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1060"/>
        <source>Network</source>
        <translation>Мережа</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1066"/>
        <source>Proxy settings</source>
        <translation>Параметри проксі сервера</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1072"/>
        <source>Use Proxy</source>
        <translation>Використовувати проксі</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1102"/>
        <source>User:</source>
        <translation>Логін:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1109"/>
        <source>Port:</source>
        <translation>Порт:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1116"/>
        <source>Host:</source>
        <translation>Адреса:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1165"/>
        <source>Enable JOSM-compatible local server on port 8111</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1186"/>
        <source>Background Image</source>
        <translation>Фонове зображення</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1192"/>
        <source>Tiles Caching (not active for Yahoo! due to legal restrictions)</source>
        <translation>Кеш плиток (не застосовується для !Yahoo)</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1198"/>
        <source>Cache directory</source>
        <translation>Тека кешу</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1208"/>
        <source>Cache size (in Mb; 0 to disable)</source>
        <translation>Розмір кешу (в МБ; 0 не використовувати)</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1227"/>
        <source>Map Adapter</source>
        <translation>Адаптер мапи</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1233"/>
        <source>Enable use of OSB background (needs new document)</source>
        <translation>Використовувати OSB-фон (потрібно для нових документів)</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1262"/>
        <source>Tools</source>
        <translation>Інструменти</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1280"/>
        <source>Name:</source>
        <translation>Назва:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1290"/>
        <source>Path:</source>
        <translation>Путь:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1339"/>
        <source>Apply</source>
        <translation>Застосувати</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1346"/>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="1353"/>
        <source>Remove</source>
        <translation>Вилучити</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="111"/>
        <source>Dutch</source>
        <translation>Голландська</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="115"/>
        <source>Japanase</source>
        <translation>Японська</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="117"/>
        <source>Portuguese</source>
        <translation>Португальська</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="120"/>
        <source>Slovak</source>
        <translation>Словацька</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="121"/>
        <source>Spanish</source>
        <translation>Іспанська</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="122"/>
        <source>Swedish</source>
        <translation>Шведська</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.cpp" line="123"/>
        <source>Ukrainian</source>
        <translation>Українська</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="132"/>
        <source>Relations selectable while hidden</source>
        <translation>Зв’язки можна виділяти, коли вони приховані</translation>
    </message>
    <message>
        <location filename="../src/Preferences/PreferencesDialog.ui" line="216"/>
        <source>Interface</source>
        <translation>Інтерфейс</translation>
    </message>
</context>
<context>
    <name>ProjPreferencesDialog</name>
    <message>
        <location filename="../src/Preferences/ProjPreferencesDialog.ui" line="14"/>
        <source>Projections setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/ProjPreferencesDialog.ui" line="20"/>
        <source>Projections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/ProjPreferencesDialog.ui" line="47"/>
        <source>Projection list:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/ProjPreferencesDialog.ui" line="76"/>
        <source>Name:</source>
        <translation>Назва:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/ProjPreferencesDialog.ui" line="93"/>
        <source>PROJ4 string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/ProjPreferencesDialog.ui" line="116"/>
        <source>Apply</source>
        <translation>Застосувати</translation>
    </message>
    <message>
        <location filename="../src/Preferences/ProjPreferencesDialog.ui" line="123"/>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location filename="../src/Preferences/ProjPreferencesDialog.ui" line="130"/>
        <source>Remove</source>
        <translation>Вилучити</translation>
    </message>
</context>
<context>
    <name>PropertiesDock</name>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="333"/>
        <source>%1/%1 selected item(s)</source>
        <translation>обрано елементів %1/%1</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="356"/>
        <source>%1/%2 selected item(s)</source>
        <translation>обрано елементів %1/%2</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="419"/>
        <source>Selected items</source>
        <translation>Обрані елементи</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="428"/>
        <source>Properties - Multiple elements</source>
        <translation>Властивості - Кілка елементів</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="458"/>
        <source>Properties - Road</source>
        <translation>Властивості — Дорога</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="478"/>
        <source>Properties - Relation</source>
        <translation>Властивості - Відношення</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="490"/>
        <location filename="../src/Docks/PropertiesDock.cpp" line="1132"/>
        <source>Properties</source>
        <translation>Властивості</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="1133"/>
        <source>Center map</source>
        <translation>Центрувати мапу</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="1134"/>
        <source>Center &amp;&amp; Zoom map</source>
        <translation>Центрувати та масштабувати мапу</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="1135"/>
        <source>Select member</source>
        <translation>Вкажіть членів</translation>
    </message>
    <message>
        <location filename="../src/Docks/PropertiesDock.cpp" line="444"/>
        <source>Properties - Node</source>
        <translation>Властивості — Точка</translation>
    </message>
</context>
<context>
    <name>QGPS</name>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="91"/>
        <location filename="../src/GPS/qgps.cpp" line="150"/>
        <source>Invalid</source>
        <translation>Хибне</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="93"/>
        <location filename="../src/GPS/qgps.cpp" line="165"/>
        <location filename="../src/GPS/qgps.cpp" line="222"/>
        <source>No Position Fix</source>
        <translation>Місце-знаходження не визначено</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="94"/>
        <location filename="../src/GPS/qgps.cpp" line="170"/>
        <location filename="../src/GPS/qgps.cpp" line="223"/>
        <source>No UTC Time</source>
        <translation>Час з супутника відсутній</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="135"/>
        <source>Meters</source>
        <translation>Метри</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="140"/>
        <source>km/h</source>
        <translation>км/г</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="147"/>
        <source>Unavailable</source>
        <translation>Недоступно</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="153"/>
        <source>2D</source>
        <translation>2D</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="156"/>
        <source>3D</source>
        <translation>3D</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="162"/>
        <source>Position Fix available</source>
        <translation>Можливе визначення місце-знаходження</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgps.cpp" line="221"/>
        <source>GPS</source>
        <translation>GPS</translation>
    </message>
</context>
<context>
    <name>QGPSComDevice</name>
    <message>
        <location filename="../src/GPS/qgpsdevice.cpp" line="684"/>
        <source>GPS log error</source>
        <translation>Помилка запису GPS</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsdevice.cpp" line="685"/>
        <source>Unable to create GPS log file: %1.</source>
        <translation>Неможливо створити файл журналу GPS:%1.</translation>
    </message>
</context>
<context>
    <name>QGPSDDevice</name>
    <message>
        <location filename="../src/GPS/qgpsdevice.cpp" line="962"/>
        <source>Unable to connect to %1:%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsdevice.cpp" line="1062"/>
        <source>GPS log error</source>
        <translation>Помилка запису GPS</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsdevice.cpp" line="1063"/>
        <source>Unable to create GPS log file: %1.</source>
        <translation>Неможливо створити файл журналу GPS:%1.</translation>
    </message>
</context>
<context>
    <name>QGPSMainWindowUI</name>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="14"/>
        <source>GPS</source>
        <translation>GPS</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="22"/>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="29"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="63"/>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="80"/>
        <source>99 99&apos; 99&quot;</source>
        <translation>99 99&apos; 99&quot;</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="70"/>
        <source>Longitude</source>
        <translation>Довгота</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="87"/>
        <source>Altitude</source>
        <translation>Висота</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="97"/>
        <source>1000 meter</source>
        <translation>1000 метрів</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="104"/>
        <source># Satellites</source>
        <translation>Супутники — #</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="114"/>
        <source>00</source>
        <translation>00</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="121"/>
        <source>Fix Type</source>
        <translation>Позиціювання</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="131"/>
        <source>Invalid</source>
        <translation>Хибне</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="138"/>
        <source>Speed</source>
        <translation>Швидкість</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="148"/>
        <source>000 km/h</source>
        <translation>000 км/г</translation>
    </message>
    <message>
        <location filename="../src/GPS/qgpsmainwindowui.ui" line="155"/>
        <source>Latitude</source>
        <translation>Широта</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/NameFinder/xmlstreamreader.cpp" line="41"/>
        <source>Not a proper results stream!</source>
        <translation>Не відповідний потік результатів!</translation>
    </message>
</context>
<context>
    <name>QtToolBarDialog</name>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.cpp" line="1798"/>
        <source>&lt; S E P A R A T O R &gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="16"/>
        <source>Customize Toolbars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="31"/>
        <source>Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="46"/>
        <source>Toolbars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="53"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="60"/>
        <source>Remove</source>
        <translation>Вилучити</translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="67"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="84"/>
        <source>Restore All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="104"/>
        <source>OK</source>
        <translation>Так</translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="111"/>
        <source>Apply</source>
        <translation>Застосувати</translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="118"/>
        <source>Cancel</source>
        <translation>Відміна</translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="143"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="158"/>
        <source>&lt;-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="173"/>
        <source>-&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="188"/>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.ui" line="213"/>
        <source>Current Toolbar Actions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtToolBarDialogPrivate</name>
    <message>
        <location filename="../3rdparty/qttoolbardialog-2.2_1-opensource/src/qttoolbardialog.cpp" line="1247"/>
        <source>Custom Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RelationProperties</name>
    <message>
        <location filename="../src/Docks/MinimumRelationProperties.ui" line="14"/>
        <source>Form</source>
        <translation>Подвійна дорога</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumRelationProperties.ui" line="31"/>
        <source>Members</source>
        <translation>Члени</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumRelationProperties.ui" line="51"/>
        <location filename="../src/Docks/MinimumRelationProperties.ui" line="81"/>
        <location filename="../src/Docks/MinimumRelationProperties.ui" line="97"/>
        <location filename="../src/Docks/MinimumRelationProperties.ui" line="139"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumRelationProperties.ui" line="119"/>
        <source>Tags</source>
        <translation>Теґи</translation>
    </message>
</context>
<context>
    <name>RoadProperties</name>
    <message>
        <location filename="../src/Docks/MinimumRoadProperties.ui" line="14"/>
        <source>Form</source>
        <translation>Подвійна дорога</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumRoadProperties.ui" line="34"/>
        <source>xxx</source>
        <translation>xxx</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumRoadProperties.ui" line="41"/>
        <source>Id</source>
        <translation>Ідентифікатор</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumRoadProperties.ui" line="61"/>
        <source>Tags</source>
        <translation>Теґи</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumRoadProperties.ui" line="81"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>SatelliteStrengthView</name>
    <message>
        <location filename="../src/GPS/SatelliteStrengthView.cpp" line="50"/>
        <source>No satellites</source>
        <translation>Супутники відсутні</translation>
    </message>
</context>
<context>
    <name>SelectionDialog</name>
    <message>
        <location filename="../src/Utils/SelectionDialog.ui" line="14"/>
        <source>Selection</source>
        <translation>Пошук елементів</translation>
    </message>
    <message>
        <location filename="../src/Utils/SelectionDialog.ui" line="25"/>
        <source>Key</source>
        <translation>Ключ</translation>
    </message>
    <message>
        <location filename="../src/Utils/SelectionDialog.ui" line="32"/>
        <source>Value</source>
        <translation>Значення</translation>
    </message>
    <message>
        <location filename="../src/Utils/SelectionDialog.ui" line="45"/>
        <source>=</source>
        <translation>=</translation>
    </message>
    <message>
        <location filename="../src/Utils/SelectionDialog.ui" line="55"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/Utils/SelectionDialog.ui" line="81"/>
        <source>Id</source>
        <translation>Ідентифікатор</translation>
    </message>
    <message>
        <location filename="../src/Utils/SelectionDialog.ui" line="88"/>
        <source>Query</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Utils/SelectionDialog.ui" line="102"/>
        <source>Maximum returned results</source>
        <translation>Максимальна кількість результатів</translation>
    </message>
</context>
<context>
    <name>SlippyMapWidget</name>
    <message>
        <location filename="../src/Utils/SlippyMapWidget.cpp" line="277"/>
        <source>Reset view</source>
        <translation>Оновити вид</translation>
    </message>
</context>
<context>
    <name>StyleDock</name>
    <message>
        <location filename="../src/Docks/StyleDock.cpp" line="102"/>
        <source>Styles</source>
        <translation>Стилі</translation>
    </message>
</context>
<context>
    <name>StyleDockWidget</name>
    <message>
        <location filename="../src/Docks/StyleDock.ui" line="14"/>
        <source>Styles</source>
        <translation>Стилі</translation>
    </message>
</context>
<context>
    <name>SyncListDialog</name>
    <message>
        <location filename="../src/Sync/SyncListDialog.ui" line="13"/>
        <source>Upload to Openstreetmap</source>
        <translation>Надіслати до Openstreetmap</translation>
    </message>
    <message>
        <location filename="../src/Sync/SyncListDialog.ui" line="25"/>
        <source>Please specify a comment for this changeset.</source>
        <translation>Надайте коментарі до цих змін.</translation>
    </message>
    <message>
        <location filename="../src/Sync/SyncListDialog.ui" line="35"/>
        <source>Please review carefully the changes sent to OSM</source>
        <translation>Будь ласка, уважно перегляньте зміни, що надсилаються до OSM</translation>
    </message>
    <message>
        <location filename="../src/Sync/SyncListDialog.ui" line="66"/>
        <source>OK</source>
        <translation>Так</translation>
    </message>
    <message>
        <location filename="../src/Sync/SyncListDialog.ui" line="73"/>
        <source>Cancel</source>
        <translation>Відміна</translation>
    </message>
</context>
<context>
    <name>TMSPreferencesDialog</name>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="14"/>
        <source>TMS servers setup</source>
        <translation>Налаштування серверів TMS</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="20"/>
        <source>TMS Servers</source>
        <translation>Сервери TMS</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="47"/>
        <source>Server list:</source>
        <translation>Перелік серверів:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="76"/>
        <source>Name:</source>
        <translation>Назва:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="93"/>
        <source>Server Url:</source>
        <translation>Адреса сервера (URL):</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="103"/>
        <source>Get Services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="125"/>
        <source>Services :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="135"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;This is a &amp;quot;standard&amp;quot; &lt;a href=&quot;http://wiki.osgeo.org/wiki/Tile_Map_Service_Specification&quot;&gt;TMS&lt;/a&gt; server&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="164"/>
        <source>Projection</source>
        <translation>Проекція</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="172"/>
        <source>Mercator (EPSG:900913)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="177"/>
        <source>Lat/Lon (EPSG:4326)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="185"/>
        <source>Origin is bottom-left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="192"/>
        <source>Tile size</source>
        <translation>Розмір частин</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="209"/>
        <source>Minimum zoom</source>
        <translation>Мінімальний масштаб</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="219"/>
        <source>Maximum zoom</source>
        <translation>Максимальний масштаб</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="245"/>
        <source>Apply</source>
        <translation>Застосувати</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="252"/>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.ui" line="259"/>
        <source>Remove</source>
        <translation>Вилучити</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.cpp" line="212"/>
        <location filename="../src/Preferences/TMSPreferencesDialog.cpp" line="265"/>
        <location filename="../src/Preferences/TMSPreferencesDialog.cpp" line="275"/>
        <source>Merkaartor: GetServices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.cpp" line="212"/>
        <source>Address and Path cannot be blank.</source>
        <translation>Адреса та путь не можуть бути порожніми.</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.cpp" line="266"/>
        <source>Download failed: %1.</source>
        <translation>Збій під час завантаження: %1.</translation>
    </message>
    <message>
        <location filename="../src/Preferences/TMSPreferencesDialog.cpp" line="275"/>
        <source>Error reading services.
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagModel</name>
    <message>
        <location filename="../src/TagModel.cpp" line="105"/>
        <source>Key</source>
        <translation>Ключ</translation>
    </message>
    <message>
        <location filename="../src/TagModel.cpp" line="107"/>
        <source>Value</source>
        <translation>Значення</translation>
    </message>
    <message>
        <location filename="../src/TagModel.h" line="22"/>
        <source>Edit this to add...</source>
        <translation>Змініть, щоб додати…</translation>
    </message>
</context>
<context>
    <name>TagSelectorWidget</name>
    <message>
        <location filename="../src/Utils/TagSelectorWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Подвійна дорога</translation>
    </message>
    <message>
        <location filename="../src/Utils/TagSelectorWidget.ui" line="34"/>
        <source>OR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Utils/TagSelectorWidget.ui" line="44"/>
        <source>AND</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Utils/TagSelectorWidget.ui" line="54"/>
        <source>NOT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Utils/TagSelectorWidget.ui" line="64"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/Utils/TagSelectorWidget.ui" line="76"/>
        <source>is</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Utils/TagSelectorWidget.ui" line="81"/>
        <source>isoneof</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagTemplateWidgetCombo</name>
    <message>
        <location filename="../src/TagTemplate/TagTemplate.cpp" line="193"/>
        <source>Undefined</source>
        <translation>Невизначений</translation>
    </message>
</context>
<context>
    <name>TagTemplates</name>
    <message>
        <location filename="../src/TagTemplate/TagTemplate.cpp" line="904"/>
        <source>Undefined</source>
        <translation>Невизначений</translation>
    </message>
</context>
<context>
    <name>TerraceDialog</name>
    <message>
        <location filename="../src/TerraceDialog.ui" line="14"/>
        <source>Terrace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="20"/>
        <source>No House Numbering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="32"/>
        <source>Number of houses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="51"/>
        <source>Add House Numbering (Karlsruhe scheme)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="73"/>
        <source>Ranges</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="86"/>
        <source>For example &quot;1-9;15-17;19,19A&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="93"/>
        <source>Pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="104"/>
        <source>All numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="109"/>
        <source>Odd numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="114"/>
        <source>Even numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/TerraceDialog.ui" line="122"/>
        <source>Select a node in the area as well to indicate the end with the first house number</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackLayer</name>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="867"/>
        <source># of track segments</source>
        <translation>сегментів треку — #</translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="867"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="868"/>
        <source>Total distance</source>
        <translation>Загальна відстань</translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="868"/>
        <source>%1 km</source>
        <translation>%1 км</translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="869"/>
        <source>Total duration</source>
        <translation>Загальна тривалість</translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="869"/>
        <source>%1h %2m</source>
        <translation>%1г %2хв</translation>
    </message>
    <message>
        <location filename="../src/Layers/Layer.cpp" line="765"/>
        <source>Extract - %1</source>
        <translation>Видобування - %1</translation>
    </message>
</context>
<context>
    <name>TrackLayerWidget</name>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="458"/>
        <source>Extract Drawing layer</source>
        <translation>Видобути шар,що редагується</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="463"/>
        <source>Zoom</source>
        <translation>Масштаб</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="471"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
</context>
<context>
    <name>TrackPointProperties</name>
    <message>
        <location filename="../src/Docks/MinimumTrackPointProperties.ui" line="14"/>
        <source>Trackpoint</source>
        <translation>Точка треку</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumTrackPointProperties.ui" line="34"/>
        <source>Latitude</source>
        <translation>Широта</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumTrackPointProperties.ui" line="44"/>
        <source>Longitude</source>
        <translation>Довгота</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumTrackPointProperties.ui" line="51"/>
        <source>Id</source>
        <translation>Ідентифікатор</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumTrackPointProperties.ui" line="58"/>
        <source>xxx</source>
        <translation>xxx</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumTrackPointProperties.ui" line="81"/>
        <source>Tags</source>
        <translation>Теґи</translation>
    </message>
    <message>
        <location filename="../src/Docks/MinimumTrackPointProperties.ui" line="101"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>UploadMapDialog</name>
    <message>
        <location filename="../src/UploadMapDialog.ui" line="13"/>
        <source>Dialog</source>
        <translation>Надсилання змін</translation>
    </message>
    <message>
        <location filename="../src/UploadMapDialog.ui" line="36"/>
        <source>Website</source>
        <translation>Сайт</translation>
    </message>
    <message>
        <location filename="../src/UploadMapDialog.ui" line="49"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../src/UploadMapDialog.ui" line="56"/>
        <source>Username</source>
        <translation>Логін</translation>
    </message>
    <message>
        <location filename="../src/UploadMapDialog.ui" line="63"/>
        <source>Use proxy</source>
        <translation>Використовувати проксі</translation>
    </message>
    <message>
        <location filename="../src/UploadMapDialog.ui" line="88"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../src/UploadMapDialog.ui" line="104"/>
        <source>99999; </source>
        <translation>99999; </translation>
    </message>
</context>
<context>
    <name>UploadedLayerWidget</name>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="524"/>
        <source>Zoom</source>
        <translation>Масштаб</translation>
    </message>
    <message>
        <location filename="../src/Layers/LayerWidget.cpp" line="529"/>
        <source>Clear</source>
        <translation>Очистити</translation>
    </message>
</context>
<context>
    <name>WMSPreferencesDialog</name>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.cpp" line="257"/>
        <location filename="../src/Preferences/WMSPreferencesDialog.cpp" line="314"/>
        <location filename="../src/Preferences/WMSPreferencesDialog.cpp" line="339"/>
        <source>Merkaartor: GetCapabilities</source>
        <translation>Merkaartor: Отримання властивостей</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.cpp" line="257"/>
        <source>Address and Path cannot be blank.</source>
        <translation>Адреса та путь не можуть бути порожніми.</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.cpp" line="315"/>
        <source>Download failed: %1.</source>
        <translation>Збій під час завантаження: %1.</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.cpp" line="339"/>
        <source>Error reading capabilities.
</source>
        <translation>Помилка читання властивостей.
</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="14"/>
        <source>WMS servers setup</source>
        <translation>Налаштування серверів WMS</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="20"/>
        <source>WMS Servers</source>
        <translation>Сервери WMS</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="47"/>
        <source>Server list:</source>
        <translation>Перелік серверів:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="76"/>
        <source>Name:</source>
        <translation>Назва:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="93"/>
        <source>Server Url:</source>
        <translation>Адреса сервера (URL):</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="103"/>
        <source>Get Capabilities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="110"/>
        <source>Layers:</source>
        <translation>Шари:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="124"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="132"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This is a caching WMS (&lt;/span&gt;&lt;a href=&quot;http://wiki.openstreetmap.org/wiki/Merkaartor/Documentation#WMS-C_Servers&quot;&gt;&lt;span style=&quot; font-weight:600; text-decoration: underline; color:#0000ff;&quot;&gt;WMS-C)&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-weight:600;&quot;&gt; server&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="155"/>
        <source>Projection:</source>
        <translation>Проекція:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="178"/>
        <source>Tile it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="198"/>
        <source>Zoom levels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="222"/>
        <source>Image format</source>
        <translation>Формат зображення</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="236"/>
        <source>Styles:</source>
        <translation>Стилі:</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="262"/>
        <source>Apply</source>
        <translation>Застосувати</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="269"/>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location filename="../src/Preferences/WMSPreferencesDialog.ui" line="276"/>
        <source>Remove</source>
        <translation>Вилучити</translation>
    </message>
</context>
<context>
    <name>WorldOsbManager</name>
    <message>
        <location filename="../src/Tools/WorldOsbManager.cpp" line="87"/>
        <source>Select OSB World directory</source>
        <translation>Оберіть теку для OSB World</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.cpp" line="101"/>
        <source>Invalid OSB World directory name</source>
        <translation>Хибна назва теки OSB World</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.cpp" line="102"/>
        <source>Please provide a valid directory name.</source>
        <translation>Вкажіть правильну назву теки.</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.cpp" line="115"/>
        <source>Region generation error</source>
        <translation>Помилка створення ділянки</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.cpp" line="116"/>
        <source>Error while generating region %1</source>
        <translation>Помилка створення ділянки %1</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.ui" line="13"/>
        <source>Dialog</source>
        <translation>Надсилання змін</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.ui" line="24"/>
        <source>World URI</source>
        <translation>World URI</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.ui" line="47"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.ui" line="58"/>
        <source>Auto-load</source>
        <translation>Автозавантаження</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.ui" line="65"/>
        <source>Auto-show</source>
        <translation>Авто-показ</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.ui" line="94"/>
        <source>Show Grid</source>
        <translation>Показувати Сітку</translation>
    </message>
    <message>
        <location filename="../src/Tools/WorldOsbManager.ui" line="126"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
</context>
</TS>
